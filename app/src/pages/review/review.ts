import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage } from 'ionic-angular';
import { ReviewServiceProvider } from '../../providers/review-service';
import { ClassServiceProvider } from '../../providers/class-service';
import { AuthServiceProvider } from '../../providers/auth-service';
import { Helper } from '../../providers/helper';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ReviewDetailPage } from '../review-detail/review-detail';
import { CONSTANTS } from '../../config/constants';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: 'page-review',
	templateUrl: 'review.html',
})
export class ReviewPage {
	REVISIT_CLASS: boolean = false;
	reviewlist: any;
	loading: any;
	result: any;
	title: string = "Review";
	bg_image: string = '';
	course_id:any = '';
	//constructor(public navCtrl: NavController, public navParams: NavParams) {
	constructor(
		private reviewService: ReviewServiceProvider,
		public loadCtrl: LoadingController,
		public navCtrl: NavController,
		public menu: MenuController,
		public helper: Helper,
		private storage: Storage,
		private authService: AuthServiceProvider,
		private classService: ClassServiceProvider
	) {
		this.menu.enable(true);
		this.REVISIT_CLASS = false;
		this.course_id = '';
		this.authService.get_course_id().then(id => {
			this.course_id = id;
		});

	}

	getclassReview() {
		this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
		this.loading.present();
		this.reviewService.reviews().then(
			response => {
				this.loading.dismiss();
				this.result = response;
				if (this.result.status == 'success') {
					this.reviewlist = this.result.data;
				}
			},
			err => {
				var error = err.json();
				console.log(error);
			}
		);
	}

	reviewDeatil(id) {
		this.navCtrl.push(ReviewDetailPage, { review_id: id });
	}


	ionViewWillEnter() {
		this.classService.get_background_images().then(res=>{
			let data:any=res;
			if(data.hasOwnProperty('inner_page')){
				this.bg_image = data.inner_page;
			}
		});
		this.initClassPage();
	}


	// Check to show class list or not
	initClassPage() {
		this.storage.get('course_settings').then((settings) => {
			if(this.course_id){
				let is_revisit = (settings && settings[this.course_id]) ? settings[this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
				this.REVISIT_CLASS = is_revisit;
				setTimeout(() => {
					this.getclassReview();
				}, 100);
			}else{
				let is_revisit = (settings && settings[CONSTANTS.CURRENT_COURSE]) ? settings[CONSTANTS.CURRENT_COURSE]['CLASSES_RE-ENTERABLE'] == 1 : !1;
				this.REVISIT_CLASS = is_revisit;
				setTimeout(() => {
					this.getclassReview();
				}, 100);
			}
		})
	}
}
