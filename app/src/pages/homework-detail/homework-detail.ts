import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonicPage, LoadingController, ModalController, NavController, NavParams } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { VgAPI } from 'videogular2/core';
import { ModalComponent } from '../../components/modal/modal';
import { Helper } from '../../providers/helper';
import { HomeworkServiceProvider } from '../../providers/homework-service';
import { HomeworkReadingDetailPage } from '../homework-reading-detail/homework-reading-detail';
import { MeditationTimerPage } from '../meditation-timer/meditation-timer';

/**
 * Generated class for the HomeworkDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-homework-detail',
	templateUrl: 'homework-detail.html',
})
export class HomeworkDetailPage{
	review_id: any;
	result: any;
	loading: any;
	homework_detail: any = [];
	classList : any;
	api: VgAPI;
	players = [];
	exercise_id: false;
	type: false;
	class_id: false;
	exercise_detail = [];
	breadcrumb = [];
	indexValue: any;
	homework_exercises_has_files_id;
	podcast_id;
	classes_id: any;
	
	constructor(
		public loadCtrl: LoadingController,
		public navCtrl: NavController,
		public navParams: NavParams,
		public menu: MenuController,
		public helper: Helper,
		public modalCtrl: ModalController,
		private homeworkService: HomeworkServiceProvider,
		public sanitizer: DomSanitizer,
	) {
		this.menu.enable(true);
	}

	// Middleware to check user is valid or not
	ionViewCanEnter() {
		this.helper.authenticated().then(
			response => {
			},
			err => {
			}
		);
	}

	// Call when user enter on the view
	ionViewWillEnter() {
		this.exercise_detail = this.navParams.get('exercise_detail');
		this.exercise_detail && this.exercise_detail.hasOwnProperty('type') && (this.type = this.exercise_detail['type']);
		this.exercise_detail && this.exercise_detail.hasOwnProperty('id') && (this.exercise_id = this.exercise_detail['id']);
		this.exercise_detail && this.exercise_detail.hasOwnProperty('classes_id') && (this.class_id = this.exercise_detail['classes_id']);
		this.breadcrumb = ['Homework', this.exercise_detail && this.exercise_detail.hasOwnProperty('title') ? this.exercise_detail['title'] : ''];
		var classes_id = this.navParams.get('classes_id');
		this.getclassList(classes_id);
	}

	getReadingDetail(detail){
		return String(detail).replace(/<.*?>/g, '').trim();
	}

	// Event called when the player is ready to play
	onPlayerReady(api: VgAPI, index, homework) {
		if (!this.players[index]) {
			this.players[index] = {};
		}
		this.indexValue = index;
		this.players[index].api = api;
		this.homework_exercises_has_files_id = homework.homework_exercises_has_files_id;
		this.podcast_id = homework.podcast_id;
		if (homework.file_status.hasOwnProperty('current_time')) {
			this.players[index].api.currentTime = homework.file_status.current_time;
		} else {
			this.players[index].api.currentTime = 0;
		}
		this.players[index].api.getDefaultMedia().subscriptions.ended.subscribe(() => {
			// Set the video to the beginning
			let obj = {
				time: {
					'current': this.players[index].api.currentTime * 1000,
					'total': this.players[index].api.currentTime * 1000,
					'status': 'COMPLETED'
				},
				'exercises_files_id': homework.exercise_id,
				'homework_exercises_has_files_id': homework.homework_exercises_has_files_id,
				'homework_podcast_recordings_id': homework.podcast_id,
			};
			this.onPlay(obj);
			this.players[index].api.getDefaultMedia().currentTime = 0;
		});
		this.api = api;
	}

	ionViewDidLeave() {
		const index = this.indexValue;
		this.players[index].api.pause();
	}

	getclassList(classes_id) {
		this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
		this.loading.present();
		var data = {
			'class_id': classes_id
		}
		this.homeworkService.classList(data).then(
			response => {
				this.loading.dismiss();
				this.result = response;
				if (this.result.status == 'success') {
					this.classList = this.result.data;					
				}
			},
			err => {
				this.loading.dismiss();

			}
		);
	}

	// Update audio/video track detail
	onPlay(obj, index = null) {
		if(index != null){
			this.players[index].api.play();
		}
		let current_time = obj.time.hasOwnProperty('current') ? obj.time.current : 0;
		let left_time = obj.time.hasOwnProperty('left') ? obj.time.left : 0;
		let total = obj.time.hasOwnProperty('total') ? obj.time.total : 0;
		let status = obj.time.hasOwnProperty('status') ? obj.time.status : 'STARTED';
		let exercises_files_id = obj.hasOwnProperty('exercises_files_id') ? obj.exercises_files_id : false;
		let exercises_id = obj.hasOwnProperty('exercises_id') ? obj.exercises_id : false;
		let files_id = obj.hasOwnProperty('files_id') ? obj.files_id : false;
		let classes_id = obj.hasOwnProperty('classes_id') ? obj.classes_id : false;
		let homework_exercises_has_files_id = obj.hasOwnProperty('homework_exercises_has_files_id') ? obj.homework_exercises_has_files_id : false;
		let homework_podcast_recordings_id = obj.hasOwnProperty('homework_podcast_recordings_id') ? obj.homework_podcast_recordings_id : false;
		
		let file_info = {
			current_time: current_time,
			left_time: left_time,
			total_time: total,
			file_status: status,
			exercises_files_id: exercises_files_id,
			exercises_id: exercises_id,
			files_id: files_id,
			classes_id: classes_id,
			homework_exercises_has_files_id:homework_exercises_has_files_id,
			homework_podcast_recordings_id:homework_podcast_recordings_id
		};
		this.homeworkService.exerciseTracking(file_info).then(
			result => {
			},
			err => {
				console.log(err);
			}
		);
	}



	// Show script content in modal
	showAlert(title, script) {
		let alert = this.modalCtrl.create(ModalComponent, { 'body': script, 'title': title });
		alert.present();
	}

	readingDetail(reading_detail, type) {
		this.navCtrl.push(HomeworkReadingDetailPage, { 'detail': reading_detail, 'exercise_detail': this.exercise_detail, 'type': type });
	}

	homeWorkPlayerReady(api: VgAPI) {
		this.api = api;
	}
	back(){
		this.navCtrl.setRoot(MeditationTimerPage);
	}
}

