webpackJsonp([0],{

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__forgot_password_forgot_password__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__signup_signup__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__tabs_tabs__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular4-social-login';







var SigninPage = (function () {
    function SigninPage(loadCtrl, navCtrl, formBuilder, authService, navParams, helper, storage) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.navParams = navParams;
        this.helper = helper;
        this.storage = storage;
        this.signupPage = __WEBPACK_IMPORTED_MODULE_8__signup_signup__["a" /* SignupPage */];
        this.forgotPasswordPage = __WEBPACK_IMPORTED_MODULE_7__forgot_password_forgot_password__["a" /* ForgotPasswordPage */];
        this.course = this.navParams.get('course');
        this.loginForm = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            remember_me: ['false'],
            course: [(this.navParams.get('course')) ? ((this.navParams.get('course') !== 'course') ? this.navParams.get('course') : __WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE) : __WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]
        });
    }
    SigninPage.prototype.signIn = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        var form = JSON.parse(JSON.stringify(this.loginForm.value));
        this.loading.present();
        if (this.loginForm.value.remember_me) {
            this.setCookie("username", "password", 365);
        }
        else {
            this.setCookie("username", "password", -1);
        }
        form['password'] = encodeURIComponent(this.loginForm.value['password']);
        this.authService.login(form).then(function (result) {
            _this.data = result;
            _this.loading.dismiss();
            if (_this.data.status != 'error') {
                _this.storage.set('token', _this.data.token);
                _this.storage.set('course_id', _this.data.course_id);
                _this.storage.set('study_id', _this.data.study_id);
                _this.storage.set('username', _this.data.username);
                _this.storage.set('profile_picture', _this.data.profile_picture);
                var msg = (_this.data.login_days < 7) ? _this.data.msg : 'Welcome back! We know you left off at an earlier class, and if you like, you can review the content you missed in the “Review” icon below. Right now what is most important is that you jump back in here with us right now.';
                _this.helper.presentToast(msg, _this.data.status);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                _this.helper.presentToast(_this.data.msg, 'error');
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.helper.presentToast('Error while connecting to server.', 'error');
        });
    };
    SigninPage.prototype.ionViewDidLoad = function () {
        this.checkCookie();
    };
    SigninPage.prototype.setCookie = function (username, password, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
        var expires = "expires=" + d.toUTCString();
        document.cookie =
            username +
                "=" +
                this.loginForm.value.username +
                ";" +
                expires +
                ";path=/";
        document.cookie =
            password +
                "=" +
                this.loginForm.value.password +
                ";" +
                expires +
                ";path=/";
    };
    SigninPage.prototype.getCookie = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };
    SigninPage.prototype.checkCookie = function () {
        var username = this.getCookie("username");
        var password = this.getCookie("password");
        if (username != "") {
            this.loginForm.controls["username"].setValue(username);
            this.loginForm.controls["password"].setValue(password);
            this.loginForm.controls["remember_me"].setValue(true);
        }
        else {
            if (username != "" && username != null) {
                this.setCookie("username", "password", 365);
            }
        }
    };
    return SigninPage;
}());
SigninPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\signin\signin.html"*/'<ion-header>\n	<ion-navbar></ion-navbar>\n</ion-header>\n<ion-content padding class="container">\n	<section class="w-1024">\n		<div col-lg-5 col-12 class="m-0-auto">\n			<form [formGroup]="loginForm" (ngSubmit)="signIn()" autocomplete="off">\n				<ion-row>\n					<ion-label stacked> USERNAME OR EMAIL ADDRESS</ion-label>\n					<ion-item>\n						<ion-input formControlName="username" id="username" type="text"\n							[class.invalid]="!loginForm.controls.username.valid && (loginForm.controls.username.dirty)">\n						</ion-input>\n\n						<ion-icon class="m-b-0" name="checkmark" item-right color="green"\n							*ngIf="loginForm.controls[\'username\'].valid"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger"\n						*ngIf="loginForm.controls[\'username\'].hasError(\'required\') && loginForm.controls[\'username\'].touched">\n						Email field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger"\n						*ngIf="loginForm.controls[\'username\'].hasError(\'pattern\') && loginForm.controls[\'username\'].touched">\n						Please enter a valid email.</p>\n					<ion-label stacked>PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="password" id="password" type="password"\n							[class.invalid]="!loginForm.controls.password.valid && (loginForm.controls.password.dirty)">\n						</ion-input>\n\n						<ion-icon class="m-b-0" name="checkmark" item-right color="green"\n							*ngIf="loginForm.controls[\'password\'].valid"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger"\n						*ngIf="loginForm.controls[\'password\'].hasError(\'required\') && loginForm.controls[\'password\'].touched">\n						Password field is required.</p>\n\n					<ion-checkbox color="dark" slot="start" formControlName="remember_me" id="remember_me">\n					</ion-checkbox>\n					<ion-label>Remember Me</ion-label>\n				</ion-row>\n				<div class="m-t-10">\n					<div padding-top text-center>\n						<button col-12 ion-button round class="submit-btn" color="primary"\n							[disabled]="!loginForm.valid">SIGN IN</button>\n					</div>\n					<div text-center color="primary" margin-top>\n						<a [navPush]="forgotPasswordPage">I forgot my password</a>\n					</div>\n				</div>\n			</form>\n		</div>\n	</section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\signin\signin.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], SigninPage);

//# sourceMappingURL=signin.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signin_signin__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_modal_modal__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignupPage = (function () {
    // Constructor
    function SignupPage(loadCtrl, navCtrl, formBuilder, authService, modalCtrl, helper, navParams) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.modalCtrl = modalCtrl;
        this.helper = helper;
        this.navParams = navParams;
        this.is_unique_email = false;
        this.is_unique_username = false;
        this.is_unique_email_msg = '';
        this.is_unique_username_msg = '';
        this.submitAttempt = false;
        this.signInPage = __WEBPACK_IMPORTED_MODULE_5__signin_signin__["a" /* SigninPage */];
        this.allowed_symbol = "$@!%*?&";
        this.isNotAllowedSymbol = false;
        this.description = {
            script: 'Caring for Yourself',
            description: '<p>The guided mindfulness training information and practices offered through Wakeful include mindfulness meditation and gentle mindful movement exercises. Please consult a qualified health care professional if you are new to mindful movement or suffer from any physical condition(s) that may limit your movement.  Should you experience any pain or discomfort, please listen to your body and discontinue the activity.</p><p> Mindfulness skills are not intended to serve as a substitute for consultation and/or treatment from a qualified heath care professional. As such, it is always the primary responsibility of the individual engaging in these practices to seek appropriate assessment, advice, support, and treatment from a qualified health professional if they have concerns about their mental or physical health. If you are experiencing problems such as anxiety, depression, insomnia, or some other type of illness while using the Wakeful tool, please consult a qualified health care professional.</p>'
        };
        this.code = this.navParams.get('accesscode');
        // form validations
        this.signupForm = this.formBuilder.group({
            token: [''],
            email: [
                '',
            ],
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^\s*[a-z0-9\@\.\$\#\_]+\s*$/i), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required])],
            id: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            password: [
                '',
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!%*?&])[A-Za-z\d$@!%*?&]{8,}/),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                ]),
            ],
            confirm_password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            term_condition: [1, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        }, {
            validator: this.matchingPasswords('password', 'confirm_password'),
        });
    }
    SignupPage.prototype.ionViewWillEnter = function () {
        this.checkCode();
    };
    // check code is authorized
    SignupPage.prototype.checkCode = function () {
        var _this = this;
        if (this.code != undefined) {
            var data = { code: this.code };
            this.authService.signup_user_data(data).then(function (result) {
                if (result['status'] == 'success') {
                    if (result['result']['is_authorized'] == 1) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signin_signin__["a" /* SigninPage */]);
                    }
                    else {
                        _this.email = result['result']['email'];
                        _this.register_token = result['result']['register_token'];
                        _this.id = result['result']['id'];
                    }
                }
                else {
                    _this.helper.presentToast('This link is no longer valid', 'error');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signin_signin__["a" /* SigninPage */]);
                }
            });
        }
    };
    // Login page navigation
    SignupPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__signin_signin__["a" /* SigninPage */]);
    };
    // Password match validation
    SignupPage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirm_password = group.controls[confirmPasswordKey];
            if (password.value !== confirm_password.value) {
                return {
                    mismatchedPasswords: true,
                };
            }
        };
    };
    // input is focused or not
    SignupPage.prototype.isTouched = function (ctrl, flag) {
        //this.signupForm.controls[ctrl]['value']=this.signupForm.controls[ctrl]['value'].trim();
        this.signupForm.controls[ctrl]['hasFocus'] = flag;
    };
    // Check Email is exits or not
    SignupPage.prototype.isEmailUnique = function (email) {
        var _this = this;
        if (this.signupForm.controls['email'].valid) {
            var email_info = { 'current_email': email.trim() };
            this.helper.isEmailUnique(email_info).then(function (response) {
                _this.is_unique_email_msg = 'Email address already exits.';
                _this.is_unique_email = false;
            }, function (err) {
                _this.is_unique_email_msg = '';
                _this.is_unique_email = true;
            });
        }
    };
    // Check Username is exits or not
    SignupPage.prototype.isUsernameUnique = function (username) {
        var _this = this;
        if (this.signupForm.controls['username'].valid) {
            var username_info = { 'current_username': username.trim() };
            this.helper.isUsernameUnique(username_info).then(function (response) {
                _this.is_unique_username_msg = 'Username already exits.';
                _this.is_unique_username = false;
            }, function (err) {
                _this.is_unique_username_msg = '';
                _this.is_unique_username = true;
            });
        }
    };
    // Save User Data
    SignupPage.prototype.signup = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        if (this.signupForm.valid && !this.notAllowedSymbol(this.signupForm.value['password'])) {
            var form = JSON.parse(JSON.stringify(this.signupForm.value));
            form['password'] = encodeURIComponent(this.signupForm.value['password']);
            form['confirm_password'] = encodeURIComponent(this.signupForm.value['confirm_password']);
            this.authService.signup(form).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status == 'success') {
                    localStorage.setItem('token', _this.data.token);
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signin_signin__["a" /* SigninPage */]);
                }
                else {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                }
            }, function (err) {
                var error = err.json();
                _this.loading.dismiss();
                if (error.status == 'error') {
                    _this.error_message = error.data;
                }
                _this.helper.presentToast(error.msg, error.status);
            });
        }
        else {
            this.loading.dismiss();
        }
    };
    SignupPage.prototype.notAllowedSymbol = function (password) {
        var flag = (/[\\" "#'()+,-./:;<=>[\]^_`{|}~]/g.test(password));
        if (flag) {
            var error = "Allowed special characters are " + this.allowed_symbol + " only.";
            this.helper.presentToast(error, 'error');
        }
        this.isNotAllowedSymbol = flag;
        return flag;
    };
    // Show script content in modal
    SignupPage.prototype.showAlert = function (script, description) {
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__components_modal_modal__["a" /* ModalComponent */], { 'body': description, 'title': script, 'bgColor': "#604a70", });
        alert.present();
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-signup',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\signup\signup.html"*/'<ion-header>\n	<ion-navbar></ion-navbar>\n</ion-header>\n<ion-content padding class="container">\n	<section class="w-1024">\n		<div col-lg-5 col-12 class="m-0-auto">\n			\n			<form [formGroup]="signupForm" (ngSubmit)="signup()" autocomplete="off">\n				<ion-row>\n\n					<ion-label stacked>ACCESS TOKEN</ion-label>\n					\n					<ion-item>\n						<ion-input formControlName="token" trim id="token" type="text" [disabled]="register_token ? true : false" value="{{register_token}}" [class.invalid]="!signupForm.controls.token.valid && (signupForm.controls.token.dirty || submitAttempt)"\n							(focus)="isTouched(\'token\',true)" (focusout)="isTouched(\'token\',false)"></ion-input>\n						\n					</ion-item>\n					<ion-label stacked>EMAIL ADDRESS</ion-label>\n					<ion-item>\n						\n						<ion-input formControlName="email" id="email" type="text" [disabled]="email ? true :false" value="{{email}}" ></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="signupForm.controls[\'email\'].valid && is_unique_email"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'email\'].hasError(\'required\') && signupForm.controls[\'email\'].touched && !signupForm.controls[\'email\'].hasFocus">Email field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'email\'].hasError(\'pattern\') && signupForm.controls[\'email\'].touched && !signupForm.controls[\'email\'].hasFocus">Please enter a valid email.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_unique_email && !signupForm.controls[\'email\'].hasError(\'pattern\') && !signupForm.controls[\'email\'].hasFocus">{{is_unique_email_msg}}</p>\n\n					<ion-label stacked>CHOOSE A USERNAME</ion-label>\n					<ion-item>\n						<ion-input formControlName="username" trim="blur" id="username" type="text" (blur)="isUsernameUnique($event.target.value)"\n						 [class.invalid]="!signupForm.controls.username.valid && (signupForm.controls.username.dirty || submitAttempt)" (focus)="isTouched(\'username\',true)"\n						 (focusout)="isTouched(\'username\',false)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="signupForm.controls[\'username\'].valid && is_unique_username"></ion-icon>\n					</ion-item>\n\n					\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'username\'].hasError(\'required\') && signupForm.controls[\'username\'].touched && !signupForm.controls[\'username\'].hasFocus">Username field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'username\'].hasError(\'pattern\') && signupForm.controls[\'username\'].touched && !signupForm.controls[\'username\'].hasFocus">Please enter a valid username.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_unique_username && !signupForm.controls[\'username\'].hasFocus">{{is_unique_username_msg}}</p>\n\n					<ion-label stacked>CHOOSE A PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="password" id="password" type="password" [class.invalid]="!signupForm.controls.password.valid && (signupForm.controls.password.dirty || submitAttempt)"\n						 (focus)="isTouched(\'password\',true)" (focusout)="isTouched(\'password\',false)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="signupForm.controls[\'password\'].valid"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'password\'].hasError(\'required\') && signupForm.controls[\'password\'].touched && !signupForm.controls[\'password\'].hasFocus">Password field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'password\'].hasError(\'pattern\') && signupForm.controls[\'password\'].touched && !signupForm.controls[\'password\'].hasFocus">The password must be alphanumeric, 1 uppercase letter and 1 special character ({{allowed_symbol}}).</p>\n\n					<ion-label stacked>CONFIRM PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="confirm_password" id="confirm_password" type="password" [class.invalid]="!signupForm.controls.confirm_password.valid && (signupForm.controls.confirm_password.dirty || submitAttempt)"\n						 (focus)="isTouched(\'confirm_password\',true)" (focusout)="isTouched(\'confirm_password\',false)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="(!signupForm.hasError(\'mismatchedPasswords\') && signupForm.controls.confirm_password.valid)"></ion-icon>\n					</ion-item>\n					<ion-input formControlName="id" type="hidden" value="{{id}}" ></ion-input>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.hasError(\'mismatchedPasswords\') && signupForm.controls.confirm_password.valid && (!signupForm.controls[\'confirm_password\'].hasFocus && !signupForm.controls[\'password\'].hasFocus)">\n						Please enter a valid confirm password.\n					</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'confirm_password\'].hasError(\'required\') && signupForm.controls[\'confirm_password\'].touched && (!signupForm.controls[\'confirm_password\'].hasFocus && !signupForm.controls[\'password\'].hasFocus)">Confirm password field is required.</p>\n				</ion-row>\n				\n						<ion-row>\n							<div col-1>\n								<ion-checkbox color="dark" slot="start" formControlName="term_condition" id="term_condition"></ion-checkbox>\n							</div>\n							<div col-11>\n								<ion-label for="term_condition" class="check-label">I agree to Wakeful’s <a href="javascript:void(0)" (click)="showAlert(description.script,description.description)">self-care terms and <br> conditions.</a></ion-label>\n							</div>\n							\n							<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="signupForm.controls[\'term_condition\'].hasError(\'required\') && signupForm.controls[\'term_condition\'].touched && !signupForm.controls[\'term_condition\'].hasFocus">Please agree the term and condition first.</p>\n						</ion-row>\n			\n				<div padding-top>\n					<div padding-top padding-bottom text-center>\n						<button ion-button col-12 round class="submit-btn" full color="primary" [disabled]="!(signupForm.valid && is_unique_username)">SIGN UP</button>\n					</div>\n					<div text-center>\n						<strong>\n							<span class="gray-text">Already have an account?</span>\n							<a [navPush]="signInPage">Sign in</a>\n						</strong>\n					</div>\n				</div>\n			</form>\n		</div>\n	</section>\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\signup\signup.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__class_schedule_class_schedule__ = __webpack_require__(383);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    function DashboardPage(menu, classService, authService, loadCtrl, navCtrl, helper, events) {
        var _this = this;
        this.menu = menu;
        this.classService = classService;
        this.authService = authService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.events = events;
        this.title = 'Dashboard';
        this.bg_image = '';
        this.course_id = '';
        this.tabshow = true;
        this.dashboardData = {
            completed_class: 0,
            consecutive_days: 0,
            meditation_minutes: 0,
            username: 'N/A',
            current_class_id: 0,
            current_class: 'N/A',
            class_status: '',
            current_page: 'N/A',
            current_page_id: 0,
            current_class_status: '',
            is_new_user: false,
            week_number: 0,
            nextClassDays: 0
        };
        this.events.subscribe('course:updateSettings', function (settings) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.initClassPage(settings);
        });
    }
    DashboardPage.prototype.getDashboardData = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.classService.dashboard(this.course_id).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success' && _this.result.data) {
                var data = _this.result.data;
                _this.dashboardData['completed_class'] = data.completed_class;
                _this.dashboardData['consecutive_days'] = data.user_profile.consecutive_days;
                _this.dashboardData['meditation_minutes'] = data.meditation_minutes;
                _this.dashboardData['username'] = data.user_profile.username;
                _this.dashboardData['is_new_user'] = data.is_new_user;
                if (data.current_class && data.current_class.hasOwnProperty('class_id')) {
                    _this.dashboardData['current_class'] = data.current_class.title;
                    _this.dashboardData['current_class_id'] = data.current_class.class_id;
                    _this.dashboardData['class_status'] = data.current_class.status;
                    _this.dashboardData['current_class_status'] = data.current_class_status;
                    _this.dashboardData['week_number'] = data.current_class.week_number;
                    _this.dashboardData['nextClassDays'] = data.next_class_day;
                }
                if (data.current_page && data.current_page.hasOwnProperty('id')) {
                    _this.dashboardData['current_page'] = data.current_page.title;
                    _this.dashboardData['current_page_id'] = data.current_page.id;
                }
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    DashboardPage.prototype.checkTabEnabled = function () {
        var _this = this;
        this.authService.check_user_class_count({ 'course_id': this.course_id }).then(function (res) {
            if (res['status'] == 'success') {
                _this.tabshow = false;
            }
            else {
                _this.tabshow = true;
            }
        });
    };
    DashboardPage.prototype.startClass = function () {
        //this.helper.get_course_setting().then(settings => {
        // if(this.course_id){
        // 	let journey_enabled = (settings && settings[this.course_id]) ? settings[this.course_id]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
        // 		this.JOURNEY_ENABLED = journey_enabled;
        // }else{
        // 	let journey_enabled = (settings && settings[CONSTANTS.CURRENT_COURSE]) ? settings[CONSTANTS.CURRENT_COURSE]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
        // 		this.JOURNEY_ENABLED = journey_enabled;
        // }
        //if(this.JOURNEY_ENABLED){
        this.navCtrl.parent.select(1);
        // set the root page so navigation history is cleared.
        // this.navCtrl.setRoot('ClassPage').then(res => {
        // 	// if success push the first page in line
        // 	//this.navCtrl.push('FirstPage');
        // 	this.navCtrl.parent.select(1);
        // });
        //}else{
        //this.helper.presentToast("Journey has not enabled in this course.", 'error');
        //}
        ///});
    };
    DashboardPage.prototype.classSchedule = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__class_schedule_class_schedule__["a" /* ClassSchedulePage */]);
    };
    DashboardPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
            _this.getDashboardData();
            _this.checkTabEnabled();
        });
    };
    DashboardPage.prototype.initClassPage = function (settings) {
        var _this = this;
        this.authService.get_course_id().then(function (id) {
            var course_id = id;
            var journey_enabled;
            if (course_id) {
                journey_enabled = (settings && settings[course_id]) ? settings[course_id]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
            }
            else {
                journey_enabled = (settings && settings[__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
            }
            _this.JOURNEY_ENABLED = journey_enabled;
        });
    };
    return DashboardPage;
}());
DashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-dashboard',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\dashboard\dashboard.html"*/'<!--\n  Generated template for the DashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <app-header [title]="title"></app-header>\n</ion-header>\n<ion-content text-center no-padding>\n    <section padding-top class="w-1024">\n        <section padding margin-top>\n            <h1 class="gray-text f-32">\n                <strong>Welcome{{ (!dashboardData.is_new_user) ? \' back\' : \'\'}}, {{dashboardData.username}}.</strong>\n            </h1>\n            <p class="gray-text">Here is your progress so far.</p>\n            <ion-row justify-content-center>\n                <ion-col col-4 col-sm-3 col-md-2>\n                    <ion-icon color="dashbg-blue" name="im-brandbubble" class="f-7em">\n                        <span>{{dashboardData.meditation_minutes}}</span>\n                    </ion-icon>\n                    <p class="progress-title">practice Minutes</p>\n                </ion-col>\n                <ion-col col-4 col-sm-3 col-md-2>\n                    <ion-icon color="dashbg-yellow" name="im-brandbubble" class="f-7em">\n                        <span>{{dashboardData.consecutive_days}}</span>\n                    </ion-icon>\n                    <p class="progress-title">consecutive days of practice</p>\n                </ion-col>\n                <ion-col col-4 col-sm-3 col-md-2>\n                    <ion-icon color="dashbg-pink" name="im-brandbubble" class="f-7em">\n                        <span>{{dashboardData.completed_class}}</span>\n                    </ion-icon>\n                    <p class="progress-title">completed classes</p>\n                </ion-col>\n            </ion-row>\n        </section>\n    </section>\n    <section style="background: #FFF">\n        <div class="w-1024" justify-content-center padding text-center>\n            <div *ngIf="!dashboardData.current_class_status">\n                <p class="gray-text">Where you are in the course</p>\n                <h3 class="gray-text f-26">{{dashboardData.current_class}} <span *ngIf="dashboardData.current_page">:</span>\n                    <strong>{{dashboardData.current_page}} </strong>\n                </h3>\n                <div col-md-5 col-sm-6 col-12 class="m-0-auto">\n                    <button *ngIf="dashboardData.class_status && (dashboardData.class_status ==\'STARTED\')" ion-button col-12 full round class="submit-btn" color="primary" [disabled]="!tabshow" (click)="startClass()">Start</button>\n                    <button *ngIf="dashboardData.class_status && dashboardData.class_status ==\'INPROGRESS\'" ion-button col-12 full round class="submit-btn" color="primary" [disabled]="!tabshow" (click)="startClass()">Continue</button>\n                    <span *ngIf="dashboardData.class_status && dashboardData.class_status ==\'COMPLETED\'" class="gray-text">Please complete the required daily home practice exercises until the next class becomes available.</span>\n                </div>\n                <div col-md-5 col-sm-6 col-12 class="m-0-auto">\n                   <p *ngIf="dashboardData.week_number > 0" class="next">Next Class: Starts in {{dashboardData.nextClassDays}} Days</p>\n                   <p><a href="javascript:void(0)" (click)="classSchedule()" class="view">View full class Schedule <span class="arrow arrow-bar is-right"></span></a></p>\n                </div>\n            </div>\n            <div *ngIf="dashboardData.current_class_status">\n                <p class="gray-text">{{ dashboardData.current_class_status }}</p>\n            </div>\n        </div>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\dashboard\dashboard.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], DashboardPage);

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_review_service__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__review_detail_review_detail__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the ReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReviewPage = (function () {
    //constructor(public navCtrl: NavController, public navParams: NavParams) {
    function ReviewPage(reviewService, loadCtrl, navCtrl, menu, helper, storage, authService, classService) {
        var _this = this;
        this.reviewService = reviewService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.helper = helper;
        this.storage = storage;
        this.authService = authService;
        this.classService = classService;
        this.REVISIT_CLASS = false;
        this.title = "Review";
        this.bg_image = '';
        this.course_id = '';
        this.menu.enable(true);
        this.REVISIT_CLASS = false;
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
    }
    ReviewPage.prototype.getclassReview = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.reviewService.reviews().then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.reviewlist = _this.result.data;
            }
        }, function (err) {
            var error = err.json();
            console.log(error);
        });
    };
    ReviewPage.prototype.reviewDeatil = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__review_detail_review_detail__["a" /* ReviewDetailPage */], { review_id: id });
    };
    ReviewPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.initClassPage();
    };
    // Check to show class list or not
    ReviewPage.prototype.initClassPage = function () {
        var _this = this;
        this.storage.get('course_settings').then(function (settings) {
            if (_this.course_id) {
                var is_revisit = (settings && settings[_this.course_id]) ? settings[_this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassReview();
                }, 100);
            }
            else {
                var is_revisit = (settings && settings[__WEBPACK_IMPORTED_MODULE_8__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_8__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassReview();
                }, 100);
            }
        });
    };
    return ReviewPage;
}());
ReviewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-review',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\review\review.html"*/'<ion-header>\n    <app-header [title]="title"></app-header>\n</ion-header>\n<ion-content [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 p-0-mob m-t-10">\n        <ion-list inset class="btn-list">\n            <p class="text-light-gray mob-p-l-r" text-center padding-bottom>Here you can view some of the course content form your previous class. Select the class below you\'d like to review.</p>\n            <div *ngIf="!REVISIT_CLASS">\n                <ion-item detail-push ion-item *ngFor="let review of reviewlist" (click)="review.status && reviewDeatil(review.id)" [class.active]="review.status==1">\n                    <h2>{{ review.class_title }}</h2>\n                    <ion-icon color="primary" name="ios-arrow-forward" item-right *ngIf="review.status==1"></ion-icon>\n                    <ion-icon color="gray" name="ios-lock-outline" item-right *ngIf="review.status==0"></ion-icon>\n                </ion-item>\n            </div>\n            <div *ngIf="REVISIT_CLASS">\n                <ion-item detail-push ion-item *ngFor="let review of reviewlist" (click)="reviewDeatil(review.id)" [class.active]="review.status==1">\n                    <h2>{{ review.class_title }}</h2>\n                    <ion-icon color="primary" name="ios-arrow-forward" item-right></ion-icon>\n                </ion-item>\n            </div>\n        </ion-list>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\review\review.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_review_service__["a" /* ReviewServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */]])
], ReviewPage);

//# sourceMappingURL=review.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReviewServiceProvider = (function () {
    function ReviewServiceProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    /**
     * @desc Used to add headers for each API call, if a user is logged in then add token header also
     * @param isLoggedIn
     */
    ReviewServiceProvider.prototype.getHeaders = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].AUTH
        });
    };
    /**
     * @desc Common Success Callback function used from all API calls
     * @param res
     * @param resolve
     * @param reject
     * @param status
     */
    ReviewServiceProvider.prototype.successCallback = function (res, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (res.headers.get('Content-type').indexOf('application/json') !== -1) {
            resolve(res.json());
        }
        else {
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /**
     * @desc Common Error Callback function used from all API calls
     * @param err
     * @param resolve
     * @param reject
     * @param status
     */
    ReviewServiceProvider.prototype.errorCallback = function (err, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (err.headers.get('Content-type') === 'application/json') {
            reject(err.json().join());
        }
        else {
            console.log(err);
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /** Calling api for get review list of classes*/
    ReviewServiceProvider.prototype.reviews = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'review/reviews?course_id=' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'review/reviews?course_id=' + __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for get review detail of review*/
    ReviewServiceProvider.prototype.review_detail = function (review_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'review/review_detail?id=' + review_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add status of review audio/video track */
    ReviewServiceProvider.prototype.reviewTracking = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'review/review_tracking', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    return ReviewServiceProvider;
}());
ReviewServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], ReviewServiceProvider);

//# sourceMappingURL=review-service.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeworkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_data_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_homework_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__homework_detail_homework_detail__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the HomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomeworkPage = (function () {
    function HomeworkPage(homeworkService, loadCtrl, navCtrl, menu, helper, storage, authService, classService, dataService) {
        var _this = this;
        this.homeworkService = homeworkService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.helper = helper;
        this.storage = storage;
        this.authService = authService;
        this.classService = classService;
        this.dataService = dataService;
        this.REVISIT_CLASS = false;
        this.shownGroup = null;
        this.title = "Homework";
        this.bg_image = '';
        this.course_id = '';
        this.classId = 0;
        this.menu.enable(true);
        this.REVISIT_CLASS = false;
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
        this.dataService.currentClass.subscribe(function (classId) {
            _this.classId = classId;
        });
    }
    HomeworkPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.initClassPage();
    };
    HomeworkPage.prototype.getclassHomework = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.homeworkService.homeworks().then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.homeworkList = _this.result.data;
            }
        }, function (err) {
            var error = err.json();
            console.log(error);
        });
    };
    HomeworkPage.prototype.homeworkDetail = function (exercise_detail) {
        exercise_detail.intro_text = exercise_detail.intro_text.replace(/\n/g, '<br>');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__homework_detail_homework_detail__["a" /* HomeworkDetailPage */], { 'exercise_detail': exercise_detail });
    };
    HomeworkPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    HomeworkPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    HomeworkPage.prototype.checkClass = function (group, homework) {
        var flagFound = homework.classes_id == this.classId;
        flagFound && (this.shownGroup = group);
        return flagFound;
    };
    // Check to show class list or not
    HomeworkPage.prototype.initClassPage = function () {
        var _this = this;
        this.storage.get('course_settings').then(function (settings) {
            if (_this.course_id) {
                var is_revisit = (settings && settings[_this.course_id]) ? settings[_this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassHomework();
                }, 100);
            }
            else {
                var is_revisit = (settings && settings[__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassHomework();
                }, 100);
            }
        });
    };
    return HomeworkPage;
}());
HomeworkPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-homework',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\homework\homework.html"*/'<!--\n  Generated template for the HomeworkPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <app-header [title]="title"></app-header>\n</ion-header>\n\n<ion-content [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 p-0-mob m-t-10">\n        <ion-list inset class="btn-list">\n            <p class="text-light-gray mob-p-l-r t-c-mob" text-center padding-bottom>Select the class below to see daily required exercises and supportive resources.</p>\n            <div class="div-opacity" *ngIf="!REVISIT_CLASS">\n                <ion-item *ngFor="let homework of homeworkList; let i=index" text-wrap (click)="homework.status && toggleGroup(i)" [class.open]="homework.classes_id == classId || isGroupShown(i)" [class.active]="homework.status" class="list">\n                    <div>\n                        <h2>\n                            <strong>{{homework.class_title}}</strong>\n                        </h2>\n                    </div>\n                    <!-- <ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon> -->\n                    <span class="accordian-plus-minus" *ngIf="homework.status==1">\n						<ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'remove\' : \'add\'" class="icon-add-min"></ion-icon>\n					</span>\n                    <ion-icon padding-right color="gray" name="ios-lock-outline" item-right *ngIf="homework.status==0"></ion-icon>\n                    <!-- <ion-icon class="icon-gray" [name]="isGroupShown(i) ? \'remove\' : \'add\'" name="add" item-right *ngIf="homework.status==1"></ion-icon> -->\n\n                    <div *ngIf="isGroupShown(i)" class="accordian-oppen-bg">\n                        <ion-list inset>\n                            <ion-item style="padding:0" detail-push ion-item *ngFor="let exercise of homework.list" (click)="homeworkDetail(exercise)">\n                                <div class="accordian-text">\n                                    <h2>\n                                        <strong>{{ exercise.title }}</strong>\n                                    </h2>\n                                </div>\n                                <ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n                            </ion-item>\n                        </ion-list>\n                    </div>\n                </ion-item>\n            </div>\n            <div class="div-opacity" *ngIf="REVISIT_CLASS">\n                <ion-item *ngFor="let homework of homeworkList; let i=index" text-wrap (click)="toggleGroup(i)" [class.open]="isGroupShown(i) || checkClass(i,homework)" [class.active]="homework.status" class="list">\n                    <div>\n                        <h2>\n                            <strong class="strong">{{homework.class_title}}</strong>\n                        </h2>\n                    </div>\n                    <span class="accordian-plus-minus">\n						<ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'remove\' : \'add\'" class="icon-add-min"></ion-icon>\n					</span>\n                    <div *ngIf="isGroupShown(i)" class="accordian-oppen-bg">\n                        <ion-list inset>\n                            <ion-item style="padding:0" detail-push ion-item *ngFor="let exercise of homework.list" (click)="homeworkDetail(exercise)">\n                                <div class="accordian-text">\n                                    <h2>\n                                        <strong class="strong">{{ exercise.title }}</strong>\n                                    </h2>\n                                </div>\n                                <ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n                            </ion-item>\n                        </ion-list>\n                    </div>\n                </ion-item>\n            </div>\n        </ion-list>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\homework\homework.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__providers_homework_service__["a" /* HomeworkServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */], __WEBPACK_IMPORTED_MODULE_8__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_7__providers_data_service__["a" /* DataServiceProvider */]])
], HomeworkPage);

//# sourceMappingURL=homework.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeworkDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_homework_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__homework_reading_detail_homework_reading_detail__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__meditation_timer_meditation_timer__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the HomeworkDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomeworkDetailPage = (function () {
    function HomeworkDetailPage(loadCtrl, navCtrl, navParams, menu, helper, modalCtrl, homeworkService, sanitizer) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.helper = helper;
        this.modalCtrl = modalCtrl;
        this.homeworkService = homeworkService;
        this.sanitizer = sanitizer;
        this.homework_detail = [];
        this.players = [];
        this.exercise_detail = [];
        this.breadcrumb = [];
        this.menu.enable(true);
    }
    // Middleware to check user is valid or not
    HomeworkDetailPage.prototype.ionViewCanEnter = function () {
        this.helper.authenticated().then(function (response) {
        }, function (err) {
        });
    };
    // Call when user enter on the view
    HomeworkDetailPage.prototype.ionViewWillEnter = function () {
        this.exercise_detail = this.navParams.get('exercise_detail');
        this.exercise_detail && this.exercise_detail.hasOwnProperty('type') && (this.type = this.exercise_detail['type']);
        this.exercise_detail && this.exercise_detail.hasOwnProperty('id') && (this.exercise_id = this.exercise_detail['id']);
        this.exercise_detail && this.exercise_detail.hasOwnProperty('classes_id') && (this.class_id = this.exercise_detail['classes_id']);
        this.breadcrumb = ['Homework', this.exercise_detail && this.exercise_detail.hasOwnProperty('title') ? this.exercise_detail['title'] : ''];
        var classes_id = this.navParams.get('classes_id');
        this.getclassList(classes_id);
    };
    HomeworkDetailPage.prototype.getReadingDetail = function (detail) {
        return String(detail).replace(/<.*?>/g, '').trim();
    };
    // Event called when the player is ready to play
    HomeworkDetailPage.prototype.onPlayerReady = function (api, index, homework) {
        var _this = this;
        if (!this.players[index]) {
            this.players[index] = {};
        }
        this.indexValue = index;
        this.players[index].api = api;
        this.homework_exercises_has_files_id = homework.homework_exercises_has_files_id;
        this.podcast_id = homework.podcast_id;
        if (homework.file_status.hasOwnProperty('current_time')) {
            this.players[index].api.currentTime = homework.file_status.current_time;
        }
        else {
            this.players[index].api.currentTime = 0;
        }
        this.players[index].api.getDefaultMedia().subscriptions.ended.subscribe(function () {
            // Set the video to the beginning
            var obj = {
                time: {
                    'current': _this.players[index].api.currentTime * 1000,
                    'total': _this.players[index].api.currentTime * 1000,
                    'status': 'COMPLETED'
                },
                'exercises_files_id': homework.exercise_id,
                'homework_exercises_has_files_id': homework.homework_exercises_has_files_id,
                'homework_podcast_recordings_id': homework.podcast_id,
            };
            _this.onPlay(obj);
            _this.players[index].api.getDefaultMedia().currentTime = 0;
        });
        this.api = api;
    };
    HomeworkDetailPage.prototype.ionViewDidLeave = function () {
        var index = this.indexValue;
        this.players[index].api.pause();
    };
    HomeworkDetailPage.prototype.getclassList = function (classes_id) {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        var data = {
            'class_id': classes_id
        };
        this.homeworkService.classList(data).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.classList = _this.result.data;
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    // Update audio/video track detail
    HomeworkDetailPage.prototype.onPlay = function (obj, index) {
        if (index === void 0) { index = null; }
        if (index != null) {
            this.players[index].api.play();
        }
        var current_time = obj.time.hasOwnProperty('current') ? obj.time.current : 0;
        var left_time = obj.time.hasOwnProperty('left') ? obj.time.left : 0;
        var total = obj.time.hasOwnProperty('total') ? obj.time.total : 0;
        var status = obj.time.hasOwnProperty('status') ? obj.time.status : 'STARTED';
        var exercises_files_id = obj.hasOwnProperty('exercises_files_id') ? obj.exercises_files_id : false;
        var exercises_id = obj.hasOwnProperty('exercises_id') ? obj.exercises_id : false;
        var files_id = obj.hasOwnProperty('files_id') ? obj.files_id : false;
        var classes_id = obj.hasOwnProperty('classes_id') ? obj.classes_id : false;
        var homework_exercises_has_files_id = obj.hasOwnProperty('homework_exercises_has_files_id') ? obj.homework_exercises_has_files_id : false;
        var homework_podcast_recordings_id = obj.hasOwnProperty('homework_podcast_recordings_id') ? obj.homework_podcast_recordings_id : false;
        var file_info = {
            current_time: current_time,
            left_time: left_time,
            total_time: total,
            file_status: status,
            exercises_files_id: exercises_files_id,
            exercises_id: exercises_id,
            files_id: files_id,
            classes_id: classes_id,
            homework_exercises_has_files_id: homework_exercises_has_files_id,
            homework_podcast_recordings_id: homework_podcast_recordings_id
        };
        this.homeworkService.exerciseTracking(file_info).then(function (result) {
        }, function (err) {
            console.log(err);
        });
    };
    // Show script content in modal
    HomeworkDetailPage.prototype.showAlert = function (title, script) {
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_modal_modal__["a" /* ModalComponent */], { 'body': script, 'title': title });
        alert.present();
    };
    HomeworkDetailPage.prototype.readingDetail = function (reading_detail, type) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__homework_reading_detail_homework_reading_detail__["a" /* HomeworkReadingDetailPage */], { 'detail': reading_detail, 'exercise_detail': this.exercise_detail, 'type': type });
    };
    HomeworkDetailPage.prototype.homeWorkPlayerReady = function (api) {
        this.api = api;
    };
    HomeworkDetailPage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__meditation_timer_meditation_timer__["a" /* MeditationTimerPage */]);
    };
    return HomeworkDetailPage;
}());
HomeworkDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-homework-detail',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\homework-detail\homework-detail.html"*/'<ion-header>\n	<ion-navbar>\n		<!-- <ion-title text-center> {{(exercise_detail?.title) ? exercise_detail?.title : "Homework Detail"}} </ion-title> -->\n		<ion-title text-center>INDEPENDENT PRACTICE</ion-title>\n		<a menuToggle end float-right>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content>\n\n	<div class="w-900"  *ngIf="!(classList?.length> 0)">\n		<div>\n			<h4 class="head-h4">No Details found</h4>\n		</div>\n	</div>\n\n	<div class="w-900"  *ngFor="let exc of classList; let index=index">\n		<section>\n			<div class="w-900" col-12>\n				<!-- <breadcrumb [data]="breadcrumb"></breadcrumb> -->\n				<!-- <div>\n					<p (click)="back()" class="back-btn"><img src=\'assets/images/arrow.svg\' width="13px" style="vertical-align: inherit;" /> BACK TO HOMEWORK</p>\n				</div> -->\n				<div>\n					<h4 class="head-h4">{{exc.title}}</h4>\n				</div>\n			</div>\n			<ion-row class="w-900">\n\n				<div col-12>\n					<p class="gray-text" [innerHtml]=\'(exc?.intro_text) ? exc?.intro_text : "Below is the course content from Class that you can watch and listen to again ."\'>\n					</p>\n				</div>\n			</ion-row>\n		</section>\n		<div class="w-900" *ngIf="exc.exercises_detail?.length > 0 && exc.type==\'exercises\'">\n			<section class="bg-odd-even" padding *ngFor="let homework of exc.exercises_detail; let index=index">\n				<div class="video-div">\n					<ion-row class="row">\n						<div *ngIf="homework.files_id" col-lg-6 col-md-6 col-sm-12 col-12 class="audios video-cont">\n							<vg-player (onPlayerReady)="onPlayerReady($event,index,homework)">\n								<vg-controls>\n									<vg-play-pause class="ion-md-im-brandbubble f-8em" (click)="onPlay({\'time\':players[index].api.time,\'homework_exercises_has_files_id\':homework.homework_exercises_has_files_id,\'exercises_id\':homework.id,\'files_id\':homework.files_id,\'classes_id\':exercise_detail?.classes_id})"></vg-play-pause>\n									<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n									<vg-scrub-bar [vgSlider]="true">\n										<vg-scrub-bar-current-time [vgSlider]="true"></vg-scrub-bar-current-time>\n									</vg-scrub-bar>\n									<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n\n									<div text-left class="read-script">\n										<span (click)="showAlert(homework.title,homework.script)">\n											<ion-icon name="im-script"></ion-icon>&nbsp;\n											<span>Read Script</span>\n										</span>\n										<vg-mute></vg-mute>\n									</div>\n								</vg-controls>\n								<audio seekTime="3" #media [vgMedia]="media" preload="auto">\n									<source [src]="homework.url">\n								</audio>\n							</vg-player>\n						</div>\n						<div *ngIf="!homework.files_id" text-center col-lg-6 col-md-6 col-sm-12 col-12 class="audios video-cont">\n							<img class="exercise-image" src="assets/images/logo.png"/>\n						</div>\n						<div col-lg-6 col-md-6 col-sm-12 col-12 class="video-cont">\n							<div class="video-content text-hide">\n								<h1 class="gray-text"> \n									<strong>{{homework.title}}</strong>\n								</h1>\n								<p class="gray-text text-concat-hide">Tip : {{homework.tip}}</p>\n								<div class="bottom">\n									<div class="border"></div>\n									<h5 class="watch" *ngIf="homework.files_id" (click)="onPlay({\'time\':players[index].api.time,\'homework_exercises_has_files_id\':homework.homework_exercises_has_files_id,\'exercises_id\':homework.id,\'files_id\':homework.files_id,\'classes_id\':exc.classes_id}, index)">Listen to the audio	<span class="audio-player"></span>\n										 <!-- &nbsp; <img src="assets/images/player.png" width="20px">   -->\n									</h5>\n								</div>\n							</div> \n						</div>\n					</ion-row>\n				</div>\n			</section>\n		</div>\n\n		 <section *ngIf="(exc.type==\'reading\') && exc.exercises_detail?.length > 0">\n			<ion-row class="w-1024 supportive-reading" *ngFor="let homework of exc.exercises_detail;">\n				<div col-lg-8 col-md-8 col-sm-10 col-10 class="center">\n					<h2>{{ homework.title}}</h2>\n					<h6>Posted by {{ homework.author}}</h6>\n					<section class="reading-content clearfix">{{getReadingDetail(homework?.reading_detail)}}</section>\n				</div>\n				<div col-lg-3 col-md-3 col-sm-12 col-12 text-right class="right">\n					<button col-12 color="primary" ion-button round class="button btn-readmore" (click)="readingDetail(homework,\'reading\')">Read More</button>\n				</div>\n			</ion-row>\n		</section> \n\n		<div class="w-900">\n			<section class="bg-odd-even" *ngIf="(exc.type==\'podcast\') && exc.exercises_detail?.length > 0">\n				<div class="video-div">\n					<ion-row class="row" *ngFor="let homework of exc.exercises_detail; let index=index">\n						<div *ngIf="homework.files_id" col-lg-6 col-md-6 col-sm-12 col-12 class="audios video-cont">\n							<vg-player (onPlayerReady)="onPlayerReady($event,index,homework)" >\n								<vg-controls>\n									<vg-play-pause (click)="onPlay({\'time\':players[index].api.time,\'homework_podcast_recordings_id\':homework.podcast_id,\'exercises_id\':homework.id,\'files_id\':homework.files_id,\'classes_id\':exc.classes_id})" class="ion-md-im-brandbubble f-8em"></vg-play-pause>\n									<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n									<vg-scrub-bar [vgSlider]="true">\n										<vg-scrub-bar-current-time [vgSlider]="true"></vg-scrub-bar-current-time>\n									</vg-scrub-bar>\n									<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n									<div text-left class="read-script">\n										<vg-mute></vg-mute>\n									</div>\n									<div text-left class="read-script">\n										<span (click)="showAlert(homework.title,homework.script)">\n											<ion-icon name="im-script"></ion-icon>\n											<span>Read Script</span>\n										</span>\n										<vg-mute></vg-mute>\n									</div>\n								</vg-controls>\n\n								<audio #media [vgMedia]="media" id="myAudio" preload="auto">\n									<source [src]="homework?.url">\n								</audio>\n							</vg-player>\n						</div>\n						<div *ngIf="!homework.files_id" col-lg-5 col-sm-12 col-12 class="audios video-cont">\n							<img class="exercise-image" src="assets/images/video_player.png"/>\n						</div>\n						<div col-lg-6 col-md-6 col-sm-12 col-12 class="video-cont">\n							<div class="video-content text-hide">\n								<h1 class="gray-text text-concat-hide">\n									<strong>{{homework.title}}</strong>\n								</h1>\n								<p><em class="gray-text">Posted by</em> <strong>{{ homework.author}}</strong></p>\n								<div class="bottom">\n									<div class="border"></div>\n									<h5 class="watch" *ngIf="homework.files_id" (click)="onPlay({\'time\':players[index].api.time,\'homework_podcast_recordings_id\':homework.podcast_id,\'exercises_id\':homework.id,\'files_id\':homework.files_id,\'classes_id\':exc.classes_id}, index)">Listen to the audio	<span class="audio-player"></span>\n										 <!-- &nbsp; <img src="assets/images/player.png" width="20px">   -->\n									</h5>\n								</div>\n							</div>\n						</div>\n					</ion-row>\n				</div>\n			</section>\n		</div>\n	</div>\n	\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\homework-detail\homework-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_homework_service__["a" /* HomeworkServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
], HomeworkDetailPage);

//# sourceMappingURL=homework-detail.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeditationTimerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_data_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_homework_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__homework_detail_homework_detail__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
/**
 * Generated class for the MeditationTimerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MeditationTimerPage = (function () {
    function MeditationTimerPage(homeworkService, loadCtrl, navCtrl, navParams, menu, helper, storage, authService, dataService, classService) {
        var _this = this;
        this.homeworkService = homeworkService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.helper = helper;
        this.storage = storage;
        this.authService = authService;
        this.dataService = dataService;
        this.classService = classService;
        this.title = 'Practice';
        this.isRunning = false;
        this.meditationId = null;
        this.players = [];
        this.practiceFileLength = 0;
        this.btnIndex = 0;
        this.REVISIT_CLASS = false;
        this.shownGroup = null;
        this.bg_image = '';
        this.course_id = '';
        this.classId = 0;
        this.menu.enable(true);
        this.REVISIT_CLASS = false;
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
        this.dataService.currentClass.subscribe(function (classId) {
            _this.classId = classId;
        });
    }
    // Check if class is loaded and go to the current page of class
    MeditationTimerPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.initClassPage();
        this.btnIndex = 0;
        this.meditationTimerCtrl = new MeditationTimerViewController(this, function (param) {
            // callback when completing the timer
            var data = {
                prepare_time: _this.meditationTimerCtrl.meditationTimer.prepareTime,
                interval_time: _this.meditationTimerCtrl.meditationTimer.intervalTime,
                meditation_time: _this.meditationTimerCtrl.meditationTimer.meditationTime,
                current_meditation_time: _this.meditationTimerCtrl.meditationTimer.currentMeditationTime,
                meditation_id: _this.meditationId,
            };
            _this.classService.updateTime(data).then(function (res) {
                if (res['status'] == 'success') {
                    clearInterval(_this.interval);
                }
            });
        });
        this.classService.course().then(function (res) {
            var data = res['data'];
            _this.meditationTimerCtrl.onLoadDisplay();
            if (data && data.hasOwnProperty('bell_audio') && data['bell_audio'].hasOwnProperty('bell_unique_name')) {
                _this.audioSrc = data.audio_url + data['bell_audio']['bell_unique_name'];
                _this.meditationTimerCtrl.loadSounds();
            }
        });
        clearInterval(this.interval);
        this.interval = setInterval(function () {
            if (_this.meditationTimerCtrl.meditationTimer.currentPhase == 2) {
                var data = {
                    prepare_time: _this.meditationTimerCtrl.meditationTimer.prepareTime,
                    interval_time: _this.meditationTimerCtrl.meditationTimer.intervalTime,
                    meditation_time: _this.meditationTimerCtrl.meditationTimer.meditationTime,
                    current_meditation_time: _this.meditationTimerCtrl.meditationTimer.currentMeditationTime,
                    meditation_id: _this.meditationId,
                };
                _this.classService.updateTime(data).then(function (res) {
                    if (res['status'] == 'success') {
                        _this.meditationId = res['id'];
                    }
                });
            }
        }, 10000);
        this.classService.practiceFile().then(function (res) {
            if (res['status'] == 'success') {
                _this.practiceFileList = res['data'];
                _this.practiceFileLength = _this.practiceFileList.length;
            }
            else {
                _this.helper.presentToast(res['msg'], 'error');
            }
        });
    };
    MeditationTimerPage.prototype.getclassHomework = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.homeworkService.homeworks().then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.homeworkList = _this.result.data;
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    MeditationTimerPage.prototype.homeworkDetail = function (exercise_detail) {
        //exercise_detail.intro_text = exercise_detail.intro_text.replace(/\n/g, '<br>');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__homework_detail_homework_detail__["a" /* HomeworkDetailPage */], { classes_id: exercise_detail });
    };
    MeditationTimerPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    MeditationTimerPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    MeditationTimerPage.prototype.checkClass = function (group, homework) {
        var flagFound = homework.classes_id == this.classId;
        flagFound && (this.shownGroup = group);
        return flagFound;
    };
    // Check to show class list or not
    MeditationTimerPage.prototype.initClassPage = function () {
        var _this = this;
        this.storage.get('course_settings').then(function (settings) {
            if (_this.course_id) {
                var is_revisit = (settings && settings[_this.course_id]) ? settings[_this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassHomework();
                }, 100);
            }
            else {
                var is_revisit = (settings && settings[__WEBPACK_IMPORTED_MODULE_6__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_6__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getclassHomework();
                }, 100);
            }
        });
    };
    // Event called when the player is ready to play
    MeditationTimerPage.prototype.onPlayerReady = function (api, index) {
        var _this = this;
        if (!this.players[index]) {
            this.players[index] = {};
        }
        this.indexValue = index;
        this.players[index].api = api;
        this.players[index].api.currentTime = 0;
        this.players[index].api.getDefaultMedia().subscriptions.ended.subscribe(function () {
            // Set the video to the beginning
            _this.players[index].api.getDefaultMedia().currentTime = 0;
        });
        this.api = api;
    };
    MeditationTimerPage.prototype.ionViewDidLoad = function () {
        // const index = this.indexValue;
        // this.players[index].api.pause();
    };
    MeditationTimerPage.prototype.timesUp = function () {
    };
    MeditationTimerPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.interval);
    };
    MeditationTimerPage.prototype.getStepsTime = function (timeType) {
        return this.meditationTimerCtrl.getStepsTime(timeType);
    };
    MeditationTimerPage.prototype.start = function ($event) {
        var _this = this;
        if (this.meditationTimerCtrl.meditationTimer.prepareTime <= 1) {
            this.helper.presentToast('Preparation time should be greater than 1.', 'error');
        }
        this.meditationId = null;
        var data = {
            prepare_time: this.meditationTimerCtrl.meditationTimer.prepareTime,
            interval_time: this.meditationTimerCtrl.meditationTimer.intervalTime,
            meditation_time: this.meditationTimerCtrl.meditationTimer.meditationTime,
            current_meditation_time: 0,
            meditation_id: this.meditationId,
        };
        this.classService.updateTime(data).then(function (res) {
            if (res['status'] == 'success') {
                _this.meditationId = res['id'];
            }
        });
        this.meditationTimerCtrl.startButtonPushedAction($event.target);
    };
    MeditationTimerPage.prototype.testSound = function () {
        this.meditationTimerCtrl.testSound();
    };
    MeditationTimerPage.prototype.settings = function (index) {
        this.btnIndex = index;
        return this.meditationTimerCtrl.settingsButtonPushedAction(index);
    };
    MeditationTimerPage.prototype.pause = function (target) {
        return this.meditationTimerCtrl.pauseButtonPushedAction(target);
    };
    MeditationTimerPage.prototype.stop = function (target) {
        var _this = this;
        var data = {
            prepare_time: this.meditationTimerCtrl.meditationTimer.prepareTime,
            interval_time: this.meditationTimerCtrl.meditationTimer.intervalTime,
            meditation_time: this.meditationTimerCtrl.meditationTimer.meditationTime,
            current_meditation_time: this.meditationTimerCtrl.meditationTimer.currentMeditationTime,
            meditation_id: this.meditationId,
        };
        this.classService.updateTime(data).then(function (res) {
            if (res['status'] == 'success') {
                _this.meditationId = null;
                clearInterval(_this.interval);
            }
        });
        return this.meditationTimerCtrl.stopButtonPushedAction(target);
    };
    MeditationTimerPage.prototype.timeArrowMinUpClick = function () {
        return false;
    };
    MeditationTimerPage.prototype.timeArrowMinUpMouseDown = function () {
        return this.meditationTimerCtrl.plusButtonPushedAction(60);
    };
    MeditationTimerPage.prototype.timeArrowMinUpMouseUp = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowMinUpMouseOut = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowMinDownClick = function () {
        return false;
    };
    MeditationTimerPage.prototype.timeArrowMinDownMouseDown = function () {
        return this.meditationTimerCtrl.plusButtonPushedAction(-60);
    };
    MeditationTimerPage.prototype.timeArrowMinDownMouseUp = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowMinDownMouseOut = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowSecUpClick = function () {
        return false;
    };
    MeditationTimerPage.prototype.timeArrowSecUpMouseDown = function () {
        return this.meditationTimerCtrl.plusButtonPushedAction(1);
    };
    MeditationTimerPage.prototype.timeArrowSecUpMouseUp = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowSecUpMouseOut = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowSecDownClick = function () {
        return false;
    };
    MeditationTimerPage.prototype.timeArrowSecDownMouseDown = function () {
        return this.meditationTimerCtrl.plusButtonPushedAction(-1);
    };
    MeditationTimerPage.prototype.timeArrowSecDownMouseUp = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    MeditationTimerPage.prototype.timeArrowSecDownMouseOut = function () {
        return this.meditationTimerCtrl.plusButtonReleasedAction();
    };
    return MeditationTimerPage;
}());
MeditationTimerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-meditation-timer',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\meditation-timer\meditation-timer.html"*/'<!--\n  Generated template for the MeditationTimerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<app-header [title]="title"></app-header>\n</ion-header>\n \n<ion-content>\n	<section class="w-988">\n		<div class="main">\n			<ion-row class="devide left-devide mr-2">\n				<span class="meditation-time-heading">Independent Practice Exercises</span>\n				<div class="scroll">					\n					<div class="scroll-devide" [ngClass]="homeworkList > 4 ? \'lg-scroll-devide\' : \'overflow-devide\'">\n						<div class="div-opacity" *ngIf="!REVISIT_CLASS">\n							<div *ngFor="let homework of homeworkList; let i=index" text-wrap (click)="homework.status" [class.open]="homework.classes_id == classId || isGroupShown(i)" [class.active]="homework.status" class="list">\n								<ion-item *ngIf="i!=0 && homework.list.length" (click)=" homework.status==1 && homeworkDetail(homework.classes_id)" >\n									<h2>\n										<strong class="c-blue-text">{{homework.class_title}}</strong>\n									</h2>\n									\n								<span class="accordian-plus-minus" *ngIf="homework.status==1 && i!=0">\n									<ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'\' : \'ios-arrow-forward\'" (click)="homeworkDetail(homework.classes_id)"></ion-icon>\n								</span>\n\n								<ion-icon padding-right color="gray" name="ios-lock-outline" item-right *ngIf="homework.status==0 && i!=0"></ion-icon>\n\n								<!-- <div *ngIf="isGroupShown(i) && i!=0" class="accordian-oppen-bg">\n									<ion-list inset>\n										<ion-item style="padding:0" detail-push ion-item *ngFor="let exercise of homework.list" (click)="homeworkDetail(exercise)">\n											<div class="accordian-text">\n												<h3>\n													<strong>{{ exercise.title }}</strong>\n												</h3>\n											</div>\n											<ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n										</ion-item>\n									</ion-list>\n								</div> -->\n								</ion-item>\n							</div>\n						</div>\n						<div class="div-opacity" *ngIf="REVISIT_CLASS">\n							<div *ngFor="let homework of homeworkList; let i=index" text-wrap  [class.open]="isGroupShown(i) || checkClass(i,homework)" [class.active]="homework.status" class="list">\n								<ion-item *ngIf="i!=0 && homework.list.length" (click)="homeworkDetail(homework.classes_id)" >\n									<h2>\n										<strong class="c-blue-text">{{homework.class_title}}</strong>\n									</h2>\n									<span class="accordian-plus-minus" >										\n										<ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'\' : \'ios-arrow-forward\'" ></ion-icon>\n									</span>\n							</ion-item>\n							</div>\n						</div>\n\n						<!-- <div class="div-opacity" *ngIf="REVISIT_CLASS">\n							<div *ngFor="let homework of homeworkList; let i=index" text-wrap (click)="toggleGroup(i)" [class.open]="isGroupShown(i) || checkClass(i,homework)" [class.active]="homework.status" class="list">\n								<ion-item *ngIf="i!=0">\n									<h2>\n										<strong class="c-blue-text">{{homework.class_title}}</strong>\n									</h2>\n									<span class="accordian-plus-minus" >\n										<ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'ios-arrow-down\' : \'ios-arrow-forward\'" ></ion-icon>\n									</span>\n									<div *ngIf="isGroupShown(i)" class="accordian-oppen-bg">\n									<ion-list inset >\n										<ion-item style="padding:0" detail-push ion-item *ngFor="let exercise of homework.list" (click)="homeworkDetail(exercise)" >\n											<div class="accordian-text">\n												<h3>\n													<strong class="strong">{{ exercise.title }}</strong>\n												</h3>\n											</div>\n											<ion-icon class="accordian-text" padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n										</ion-item>\n									</ion-list>\n								</div>\n							</ion-item>\n						</div>\n						</div> -->\n\n					</div>				\n				</div>\n			</ion-row>\n			\n			<ion-col class="devide right-devide">\n				<span class="meditation-time-heading">Meditation Timer</span>\n			<ion-row class="add-br">\n				<ion-col text-center col-lg-4 col-sm-4 col-12>\n					<ion-row id="mtSettings" *ngIf="!isRunning">\n						<div class="mr-l">\n							<div class="">\n								<button (click)="settings(0)" ion-button col-12 full color="pink" class="prepare btn-148 mr-t-10" [ngClass]="btnIndex==0 ? \'pink-active\' : \'btn-white\'" round type="button">Prepare</button>\n								<div class="c-pink prepare-time" id="mtPrepareTimeSetting">{{meditationTimerCtrl?.getStepsTime(\'prepareTime\')}}</div>\n							</div>\n							<div class="">\n								<button (click)="settings(1)"ion-button col-12 full color="pink" round type="button" class="prepare btn-148" [ngClass]="btnIndex==1 ? \'pink-active\' : \'btn-white\'">Meditation</button>\n								<div class="c-pink prepare-time" id="mtMeditationTimeSetting">{{meditationTimerCtrl?.getStepsTime(\'meditationTime\')}}</div> \n							</div>\n							<div class="">\n								<button (click)="settings(2)" ion-button col-12 full color="pink" round type="button" class="prepare btn-148" [ngClass]="btnIndex==2 ? \'pink-active\' : \'btn-white\'">Interval</button> \n								<div class="c-pink prepare-time" id="mtIntervalTimeSetting">{{meditationTimerCtrl?.getStepsTime(\'intervalTime\')}}</div>\n							</div>\n						</div>\n\n					</ion-row>\n				</ion-col>\n				<ion-col text-center col-lg-8 col-sm-8 col-12 class="D-block pd-0">\n					<ion-col col-lg-12 col-12 col-sm-12 class="D-block pd-0">\n						<div class="D-block">\n							<div text-center class="D-block c-pink meditation-time" [ngClass]="!!isRunning?\'playing\':\'\'">\n								<div class="time-set">\n									<!-- <ion-icon *ngIf="!isRunning" color="primary" name="ios-arrow-up" item-right (click)="timeArrowMinUpClick()" (mousedown)="timeArrowMinUpMouseDown()" (mouseup)="timeArrowMinUpMouseUp()" (mouseout)="timeArrowMinUpMouseOut()"></ion-icon> -->\n									<img src=\'assets/images/up_icon.png\' alt="up" *ngIf="!isRunning" (click)="timeArrowMinUpClick()" (mousedown)="timeArrowMinUpMouseDown()" (mouseup)="timeArrowMinUpMouseUp()" (mouseout)="timeArrowMinUpMouseOut()">\n									<p id="overAllMinutes">0</p>\n									<!-- <ion-icon *ngIf="!isRunning" color="primary" name="ios-arrow-down" item-right (click)="timeArrowMinDownClick()" (mousedown)="timeArrowMinDownMouseDown()" (mouseup)="timeArrowMinDownMouseUp()" (mouseout)="timeArrowMinDownMouseOut()"></ion-icon> -->\n									<img src=\'assets/images/down_icon.png\' alt="down" class="down-img" *ngIf="!isRunning" (click)="timeArrowMinDownClick()" (mousedown) ="timeArrowMinDownMouseDown()" (mouseup)="timeArrowMinDownMouseUp()" (mouseout)="timeArrowMinDownMouseOut()">\n								</div>\n								<div class="time-set">\n									<p class="seperator">:</p>\n								</div>\n								<div class="time-set">\n									<!-- <ion-icon *ngIf="!isRunning" color="primary" name="ios-arrow-up" item-right (click)="timeArrowSecUpClick()" (mousedown)="timeArrowSecUpMouseDown()" (mouseup)="timeArrowSecUpMouseUp()" (mouseout)="timeArrowSecUpMouseOut()"></ion-icon> -->\n									<img src=\'assets/images/up_icon.png\' alt="up" *ngIf="!isRunning" (click)="timeArrowSecUpClick()" (mousedown)="timeArrowSecUpMouseDown()" (mouseup)="timeArrowSecUpMouseUp()" (mouseout)="timeArrowSecUpMouseOut()">\n									<p id="overAllSeconds">30</p>\n									<!-- <ion-icon *ngIf="!isRunning" color="primary" name="ios-arrow-down" item-right (click)="timeArrowSecDownClick()" (mousedown)="timeArrowSecDownMouseDown()" (mouseup)="timeArrowSecDownMouseUp()" (mouseout)="timeArrowSecDownMouseOut()"></ion-icon> -->\n									<img src=\'assets/images/down_icon.png\' alt="down" class="down-img" *ngIf="!isRunning" (click)="timeArrowSecDownClick()" (mousedown)="timeArrowSecDownMouseDown()" (mouseup)="timeArrowSecDownMouseUp()" (mouseout)="timeArrowSecDownMouseOut()">\n								</div>\n							</div>\n							<div id="mtIntervalTime" class="c-pink" *ngIf="!!isRunning"></div>\n\n							<div class="mt-30" *ngIf="!isRunning" >\n								<button class="m-0-auto meditate" ion-button col-12 full round type="button" (click)="start($event)">Meditate</button>\n							</div>\n							<div  *ngIf="!!isRunning" class="mr-top">\n								<div col-12>\n									<button ion-button col-md-5 col-5 round type="button" (click)=" stop($event.target)" class="stop-btn">Stop</button>\n									<button ion-button col-md-5 col-5 round type="button" (click)="pause($event.target)" class="stop-btn">Pause</button>\n								</div>\n							</div>\n							<div [hidden]="true" id="audioContainer">\n								<audio controls id="audioChime_0">\n									<source>\n								  	Your browser does not support the audio element.\n								</audio>\n								<audio controls id="audioChime_1">\n									<source>\n								  	Your browser does not support the audio element.\n								</audio>\n								<audio controls id="audioChime_2">\n									<source>\n								  	Your browser does not support the audio element.\n								</audio>\n								<audio controls id="audioChime_3">\n									<source>\n								  	Your browser does not support the audio element.\n								</audio>\n							</div>\n						</div>\n					</ion-col>\n				</ion-col>\n			</ion-row>\n				\n			</ion-col>	\n			<!-- <ion-row class="devide right-devide mt--50">\n				<div class="scroll">\n					<h3>Practice Audios</h3>\n					<div class="scroll-devide" [ngClass]="practiceFileLength > 3 ? \'lg-scroll-devide\' : \'\'">\n						<div *ngFor="let practiceFile of practiceFileList" class="white-audio mb-20">\n							<p>{{practiceFile.title}}</p>\n							<div class="audio audios-practice">\n								<vg-player (onPlayerReady)="onPlayerReady($event,index)">\n									<vg-controls>\n										<vg-play-pause class="ion-md-im-brandbubble f-8em"></vg-play-pause>\n										<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n										<vg-scrub-bar [vgSlider]="true">\n											<vg-scrub-bar-current-time [vgSlider]="true"></vg-scrub-bar-current-time>\n										</vg-scrub-bar>\n										<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n		\n									</vg-controls>\n									<audio seekTime="3" #media [vgMedia]="media" preload="auto">\n										<source [src]="practiceFile.url">\n									</audio>\n								</vg-player>\n							</div>\n						</div>\n					</div>\n				</div>\n			</ion-row> -->\n		</div>\n	</section>\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\meditation-timer\meditation-timer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__providers_homework_service__["a" /* HomeworkServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_app_menu_controller__["a" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__providers_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_8__providers_data_service__["a" /* DataServiceProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_class_service__["a" /* ClassServiceProvider */]])
], MeditationTimerPage);

var Timer = (function () {
    function Timer(target, action, interval, repeat, timesUpCallback) {
        this.target = target;
        this.action = action;
        this.interval = interval;
        this.repeat = repeat;
        this.timesUpCallback = timesUpCallback;
        this.isRunning = false;
    }
    Timer.prototype.fire = function () {
        this.target[this.action].call(this.target);
        if (this.repeat === true) {
            this.cycle();
        }
    };
    Timer.prototype.cycle = function () {
        if (this.isRunning) {
            var self_1 = this;
            this.t = window.setTimeout(function () { self_1.fire(); }, this.interval);
        }
    };
    Timer.prototype.start = function () {
        this.isRunning = true;
        this.fire();
    };
    Timer.prototype.stop = function () {
        this.isRunning = false;
        window.clearTimeout(this.t);
        if (typeof this.timesUpCallback == 'function') {
            this.timesUpCallback();
        }
    };
    return Timer;
}());
;
var MeditationTimer = (function () {
    function MeditationTimer(delegateRef, mTimer, callback) {
        this.delegateRef = delegateRef;
        this.mTimer = mTimer;
        this.defaults = {
            prepareTime: 10,
            meditationTime: 60,
            intervalTime: 10
        };
        this.phases = {
            start: 0,
            prepare: 1,
            meditation: 2,
            end: 3
        };
        this.currentPhase = this.phases.start;
        this.currentMeditationTime = 0;
        this.currentIntervalTime = 0;
        this.prepareTime = 0;
        this.meditationTime = 0;
        this.intervalTime = 0;
        this.totalIntervals = 0;
        this.isRunning = false;
        this.currentInterval = 1;
        this.timer = new Timer(this, "fireTimer", 1000, true, callback);
        this.delegate = delegateRef;
    }
    MeditationTimer.prototype.saveTimer = function () {
        // var date = new Date();
        // date.setTime(date.getTime()+(365*24*60*60*1000));
        // var expires = "expires=" + date.toGMTString();
        // document.cookie = "prepareTime=" + this.prepareTime + "; " + expires + "; path=/";
        // document.cookie = "meditationTime=" + this.meditationTime + "; " + expires + "; path=/";
        // document.cookie = "intervalTime=" + this.intervalTime +  "; " + expires + "; path=/";
    };
    ;
    MeditationTimer.prototype.loadTimer = function () {
        // if (document.cookie) {
        // 	var cookieString = document.cookie.replace(' ', '');
        // 	var cookiesArray = cookieString.split(";");
        // 	var prepareArray = cookiesArray[2].split("=");
        // 	var meditationArray = cookiesArray[1].split("=");
        // 	var intervalArray = cookiesArray[0].split("=");
        // 	var prepareValue = 0;
        // 	var meditationValue = 0;
        // 	var intervalValue = 0;
        // 	for (let i=1; i<=cookiesArray.length; i++) {
        // 		var settingArray = cookiesArray[(i-1)].split("=");
        // 		if (settingArray[0].match("prepareTime")) {
        // 			this.prepareValue = settingArray[1];
        // 		}
        // 		if (settingArray[0].match("meditationTime")) {
        // 			this.meditationValue = settingArray[1];
        // 		}
        // 		if (settingArray[0].match("intervalTime")) {
        // 			this.intervalValue = settingArray[1];
        // 		}
        // 	}
        // 	this.prepareTime = parseInt(this.prepareValue);
        // 	this.meditationTime = parseInt(this.meditationValue);
        // 	this.intervalTime = parseInt(this.intervalValue);
        // }
        // else {
        this.prepareTime = this.defaults.prepareTime;
        this.meditationTime = this.defaults.meditationTime;
        this.intervalTime = this.defaults.intervalTime;
        //}
    };
    ;
    MeditationTimer.prototype.start = function () {
        if (this.isRunning) {
            return;
        }
        if (this.currentPhase == this.phases.start) {
            if (this.intervalTime > 0) {
                this.totalIntervals = Math.ceil(this.meditationTime / this.intervalTime);
            }
            this.currentPhase = this.calculateNextPhase(this.currentPhase);
        }
        if (this.currentPhase != this.phases.end) {
            this.isRunning = true;
            this.mTimer.isRunning = true;
            this.timer.start();
        }
        else {
            this.endSession();
        }
        //this.classService.updateTime().then({})
    };
    ;
    MeditationTimer.prototype.stop = function () {
        if (this.isRunning) {
            this.isRunning = false;
            this.timer.stop();
        }
    };
    ;
    MeditationTimer.prototype.reset = function () {
        this.stop();
        this.currentPhase = this.phases.start;
        this.currentMeditationTime = 0;
        this.currentIntervalTime = 0;
        this.totalIntervals = 0;
        this.currentInterval = 1;
    };
    ;
    MeditationTimer.prototype.endSession = function () {
        this.reset();
        this.mTimer.isRunning = false;
        this.delegate.sessionEnded(this); //['sessionEnded'].call(this);
        //this.meditationTimer.sessionEnded(this);
    };
    MeditationTimer.prototype.calculateNextPhase = function (phase) {
        switch (phase) {
            case this.phases.start:
                if (this.prepareTime > 0) {
                    this.currentPhase = this.phases.prepare;
                }
                else if (this.meditationTime > 0) {
                    this.currentPhase = this.phases.meditation;
                }
                else {
                    this.currentPhase = this.phases.end;
                }
                break;
            case this.phases.prepare:
                if (this.meditationTime > 0) {
                    this.currentPhase = this.phases.meditation;
                }
                else {
                    this.currentPhase = this.phases.end;
                }
                break;
            case this.phases.meditation:
                this.currentPhase = this.phases.end;
                break;
            default:
                break;
        }
        this.delegate.phaseChanged(); // ['phaseChanged'].call(this);
        //this.meditationTimer.phaseChanged(this);
        if (this.currentPhase == this.phases.prepare) {
            this.currentMeditationTime = this.prepareTime;
        }
        if (this.currentPhase == this.phases.meditation) {
            this.currentMeditationTime = this.meditationTime;
            if (this.intervalTime > 0) {
                this.currentIntervalTime = this.intervalTime;
            }
        }
        return this.currentPhase;
    };
    ;
    MeditationTimer.prototype.fireTimer = function () {
        this.currentMeditationTime = (this.currentMeditationTime - 1);
        if (this.currentPhase == this.phases.meditation && this.intervalTime > 0) {
            this.currentIntervalTime = (this.currentIntervalTime - 1);
            if (this.currentIntervalTime == 0 && this.currentMeditationTime != 0) {
                this.currentIntervalTime = this.intervalTime;
                this.currentInterval++;
                this.delegate.intervalEnded(this);
            }
            this.delegate.intervalFired(this);
        }
        this.delegate.timerFired(this);
        if (this.currentMeditationTime == 0) {
            if (this.calculateNextPhase(this.currentPhase) == this.phases.end) {
                this.delegate.sessionCompleted(this);
                this.endSession();
            }
        }
    };
    return MeditationTimer;
}());
;
var SoundEngine = (function () {
    function SoundEngine() {
        this.chimes = {};
        this.playPromise = {};
    }
    SoundEngine.prototype.loadSounds = function (mTimer) {
        for (var i = 0; i < 4; i++) {
            // let url = "";
            this.chimes[i] = document.querySelector('#audioChime_' + i);
            // if (this.chimes[i].canPlayType("audio/ogg")) {
            // 	url = "assets/media/chime.ogg";
            // } else if (this.chimes[i].canPlayType("audio/mpeg")) {
            // 	url = "assets/media/chime.mp3";
            // }
            this.chimes[i].src = mTimer.audioSrc;
            // this.chimes[i].src = url;
            this.chimes[i].loop = false;
            this.chimes[i].autoplay = false;
            this.chimes[i].load();
        }
    };
    ;
    SoundEngine.prototype.playSound = function (i) {
        if (typeof this.chimes[i] !== 'undefined') {
            if (!this.chimes[i].paused) {
                this.chimes[i].pause();
                this.chimes[i].currentTime = 0;
            }
        }
        this.playPromise[i] = this.chimes[i].play();
    };
    SoundEngine.prototype.stopSound = function () {
        for (var i = 0; i < 4; i++) {
            if (typeof this.chimes[i] !== 'undefined') {
                if (!this.chimes[i].paused) {
                    this.chimes[i].pause();
                    this.chimes[i].currentTime = 0;
                }
            }
        }
    };
    return SoundEngine;
}());
var MeditationTimerViewController = (function () {
    function MeditationTimerViewController(mTimer, callback) {
        this.mTimer = mTimer;
        this.settings = {
            prepare: 0,
            meditation: 1,
            interval: 2
        };
        this.activeSetting = this.settings.prepare;
        this.numberOfChimes = 0;
        this.totalChimes = 0;
        this.settingTimerIntervals = 300;
        this.meditationTimer = new MeditationTimer(this, mTimer, callback);
        this.soundEngine = new SoundEngine();
    }
    MeditationTimerViewController.prototype.loadSounds = function () {
        this.soundEngine.loadSounds(this.mTimer);
    };
    // SOUNDS
    MeditationTimerViewController.prototype.playChime = function (i) {
        this.soundEngine.playSound(i);
        i++;
        this.numberOfChimes--;
        var chimeDuration;
        if (this.totalChimes == 2 && this.numberOfChimes == 1) {
            chimeDuration = 6000;
        }
        if (this.totalChimes == 3 && this.numberOfChimes == 2) {
            chimeDuration = 4000;
        }
        if (this.totalChimes == 3 && this.numberOfChimes == 1) {
            chimeDuration = 10000;
        }
        if (this.numberOfChimes > 0) {
            var self_2 = this;
            this.chimeTimer = window.setTimeout(function () {
                self_2.playChime(i);
            }, chimeDuration);
        }
    };
    ;
    MeditationTimerViewController.prototype.chimeOne = function () {
        this.numberOfChimes = 1;
        this.totalChimes = 1;
        window.clearTimeout(this.chimeTimer);
        this.playChime(1);
    };
    ;
    MeditationTimerViewController.prototype.chimeTwo = function () {
        this.numberOfChimes = 1; // change to 1
        this.totalChimes = 1; // change to 1
        this.playChime(1);
    };
    ;
    MeditationTimerViewController.prototype.chimeThree = function () {
        this.numberOfChimes = 3;
        this.totalChimes = 3;
        this.playChime(1);
    };
    ;
    // HELPERS
    MeditationTimerViewController.prototype.convertToTimeFormat = function (num) {
        var hrs = Math.floor(num / 3600);
        var mins = Math.floor((num % 3600) / 60);
        var secs = (num % 60);
        return ((hrs > 0) ? hrs + ":" : "") + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
    };
    ;
    MeditationTimerViewController.prototype.getMinutesFormat = function (num) {
        var mins = Math.floor((num % 3600) / 60);
        return ((mins < 10) ? "0" : "") + mins;
    };
    ;
    MeditationTimerViewController.prototype.getSecondsFormat = function (num) {
        var secs = (num % 60);
        return ((secs < 10) ? "0" : "") + secs;
    };
    ;
    MeditationTimerViewController.prototype.getStepsTime = function (time) {
        return this.convertToTimeFormat(this.meditationTimer[time]);
    };
    MeditationTimerViewController.prototype.changeSettingValue = function (i) {
        if (this.activeSetting == this.settings.prepare) {
            this.meditationTimer.prepareTime += i;
            this.meditationTimer.prepareTime = Math.min(Math.max(this.meditationTimer.prepareTime, 0), 3599);
            document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.prepareTime));
            document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.prepareTime));
            document.querySelector("#mtPrepareTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.prepareTime));
        }
        else if (this.activeSetting == this.settings.meditation) {
            this.meditationTimer.meditationTime += i;
            this.meditationTimer.meditationTime = Math.min(Math.max(this.meditationTimer.meditationTime, 0), 3599);
            document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.meditationTime));
            document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.meditationTime));
            document.querySelector("#mtMeditationTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.meditationTime));
        }
        else if (this.activeSetting == this.settings.interval) {
            this.meditationTimer.intervalTime += i;
            this.meditationTimer.intervalTime = Math.min(Math.max(this.meditationTimer.intervalTime, 0), 3599);
            document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.intervalTime));
            document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.intervalTime));
            document.querySelector("#mtIntervalTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.intervalTime));
        }
        var self = this;
        this.settingTimer = window.setTimeout(function () { self.changeSettingValue(i); }, this.settingTimerIntervals);
        this.settingTimerIntervals = (this.settingTimerIntervals > 100) ? (this.settingTimerIntervals - 50) : this.settingTimerIntervals;
    };
    ;
    MeditationTimerViewController.prototype.invalidateChangeSettingValue = function () {
        window.clearTimeout(this.settingTimer);
        this.settingTimerIntervals = 300;
    };
    ;
    // DISPLAY
    MeditationTimerViewController.prototype.displayForSetup = function (setting) {
        switch (setting) {
            case this.settings.prepare:
                this.activeSetting = this.settings.prepare;
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.prepareTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.prepareTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.prepareTime));
                break;
            case this.settings.meditation:
                this.activeSetting = this.settings.meditation;
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.meditationTime));
                break;
            case this.settings.interval:
                this.activeSetting = this.settings.interval;
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.intervalTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.intervalTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.intervalTime));
                break;
            default:
                break;
        }
        var $activeElem = document.querySelectorAll("#mtSettings button");
        for (var i = 0, l = $activeElem.length; i < l; i++) {
            $activeElem[i].classList.add('btn-white');
        }
        document.querySelectorAll("#mtSettings ion-col") &&
            document.querySelectorAll("#mtSettings ion-col").length &&
            document.querySelectorAll("#mtSettings ion-col")[setting].querySelector('button').classList.remove("btn-white");
    };
    ;
    MeditationTimerViewController.prototype.displayForPhase = function (phase) {
        switch (phase) {
            case this.meditationTimer.phases.start:
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.meditationTime));
                //document.querySelector("#mtIntervalTime").innerHTML = ("");
                break;
            case this.meditationTimer.phases.prepare:
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.prepareTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.prepareTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.prepareTime));
                //document.querySelector("#mtIntervalTime").innerHTML = ("");
                break;
            case this.meditationTimer.phases.meditation:
                this.chimeOne();
                //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.meditationTime));
                document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.meditationTime));
                if (this.meditationTimer.intervalTime > 0) {
                    document.querySelector("#mtIntervalTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.intervalTime) + " (" + this.meditationTimer.currentInterval + "/" + this.meditationTimer.totalIntervals + ")");
                }
                else {
                    document.querySelector("#mtIntervalTime").innerHTML = ("");
                }
                break;
            default:
                break;
        }
    };
    ;
    MeditationTimerViewController.prototype.onLoadDisplay = function () {
        this.meditationTimer.loadTimer();
        this.displayForSetup(this.activeSetting);
        document.querySelector("#mtPrepareTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.prepareTime));
        document.querySelector("#mtMeditationTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.meditationTime));
        document.querySelector("#mtIntervalTimeSetting").innerHTML = (this.convertToTimeFormat(this.meditationTimer.intervalTime));
        //(document.querySelector("#mtIntervalTime") as HTMLElement).style.display="none";
        // (document.querySelector("#mtStartButton") as HTMLElement).innerText = "Meditate";
        // (document.querySelector("#mtPauseButton") as HTMLElement).innerText = "Pause";
        // (document.querySelector("#mtStopButton") as HTMLElement).innerText = "Stop";
        // (document.querySelector("#mtPauseButton") as HTMLElement).style.display="none";
        // (document.querySelector("#mtStopButton") as HTMLElement).style.display="none";
    };
    ;
    // DELEGATE
    MeditationTimerViewController.prototype.timerFired = function () {
        //document.querySelector("#mtOverallTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.currentMeditationTime));
        document.querySelector("#overAllMinutes").innerHTML = (this.getMinutesFormat(this.meditationTimer.currentMeditationTime));
        document.querySelector("#overAllSeconds").innerHTML = (this.getSecondsFormat(this.meditationTimer.currentMeditationTime));
    };
    ;
    MeditationTimerViewController.prototype.intervalFired = function () {
        if (this.meditationTimer.currentIntervalTime > 0) {
            document.querySelector("#mtIntervalTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.currentIntervalTime) + " (" + this.meditationTimer.currentInterval + "/" + this.meditationTimer.totalIntervals + ")");
        }
        else {
            document.querySelector("#mtIntervalTime").innerHTML = (this.convertToTimeFormat(this.meditationTimer.intervalTime) + " (" + this.meditationTimer.currentInterval + "/" + this.meditationTimer.totalIntervals + ")");
        }
    };
    ;
    MeditationTimerViewController.prototype.intervalEnded = function () {
        this.chimeTwo();
    };
    ;
    MeditationTimerViewController.prototype.sessionCompleted = function () {
        this.chimeThree();
    };
    ;
    MeditationTimerViewController.prototype.phaseChanged = function () {
        this.displayForPhase(this.meditationTimer.currentPhase);
    };
    ;
    MeditationTimerViewController.prototype.sessionEnded = function (m) {
        this.displayForPhase(this.meditationTimer.currentPhase);
        this.displayForSetup(this.activeSetting);
        //document.querySelector("#mtIntervalTime").innerHTML = "";
        //(document.querySelector("#mtIntervalTime") as HTMLElement).style.display="none";
        //(document.querySelector("#mtSettings") as HTMLElement).style.display="block";
        //(document.querySelector("#mtStartButton") as HTMLElement).style.display="block";
        //(document.querySelector("#mtPauseButton") as HTMLElement).style.display="none";
        //(document.querySelector("#mtPauseButton") as HTMLElement).innerText = "Pause";
        //(document.querySelector("#mtStopButton") as HTMLElement).style.display="none";
    };
    ;
    // ACTIONS
    MeditationTimerViewController.prototype.settingsButtonPushedAction = function (index) {
        if (!this.meditationTimer.isRunning) {
            this.displayForSetup(index);
        }
        return false;
    };
    ;
    MeditationTimerViewController.prototype.pauseButtonPushedAction = function (sender) {
        if (this.meditationTimer.isRunning) {
            this.meditationTimer.stop();
            sender.innerText = "Resume";
        }
        else {
            this.meditationTimer.start();
            sender.innerText = "Pause";
        }
        return false;
    };
    ;
    MeditationTimerViewController.prototype.startButtonPushedAction = function (sender) {
        if (!this.meditationTimer.isRunning) {
            this.meditationTimer.start();
            this.meditationTimer.saveTimer();
            //(document.querySelector("#mtIntervalTime") as HTMLElement).style.display="block";
            //(document.querySelector("#mtSettings") as HTMLElement).style.display="none";
            // (document.querySelector("#mtStartButton") as HTMLElement).style.display="none";
            // (document.querySelector("#mtPauseButton") as HTMLElement).style.display="block";
            // (document.querySelector("#mtStopButton") as HTMLElement).style.display="block";
            this.soundEngine.stopSound();
        }
        return false;
    };
    ;
    MeditationTimerViewController.prototype.stopButtonPushedAction = function (sender) {
        this.meditationTimer.endSession();
        this.soundEngine.stopSound();
        return false;
    };
    ;
    MeditationTimerViewController.prototype.plusButtonPushedAction = function (secs) {
        this.changeSettingValue(secs);
        return false;
    };
    ;
    MeditationTimerViewController.prototype.minusButtonPushedAction = function (secs) {
        this.changeSettingValue(secs);
        return false;
    };
    ;
    MeditationTimerViewController.prototype.plusButtonReleasedAction = function () {
        this.invalidateChangeSettingValue();
        return false;
    };
    ;
    MeditationTimerViewController.prototype.minusButtonReleasedAction = function () {
        this.invalidateChangeSettingValue();
        return false;
    };
    ;
    MeditationTimerViewController.prototype.testSound = function () {
        this.soundEngine.stopSound();
        this.soundEngine.playSound(0);
        return false;
    };
    ;
    return MeditationTimerViewController;
}());
;
//# sourceMappingURL=meditation-timer.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AboutPage = (function () {
    function AboutPage(navCtrl, helper) {
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.players = [];
        this.user = false;
        this.administartor = false;
    }
    AboutPage.prototype.onPlayerReady = function (api, index) {
        var _this = this;
        if (!this.players[index]) {
            this.players[index] = {};
        }
        this.players[index].api = api;
        this.players[index].api.getDefaultMedia().subscriptions.ended.subscribe(function () {
            _this.onPlay(api, index);
        });
    };
    AboutPage.prototype.onPlay = function (api, index) {
        this.user = true;
        this.administartor = true;
        this.players[index].api = api;
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-about',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\about\about.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n<ion-content class="about-section">\n    <section class="about-bg">\n        <ion-row w-1024 justify-content-center>\n            <ion-col col-lg-8 col-md-12 col-12>\n                <h1 class="static-headings">About Wakeful<sup>™</sup></h1>\n                <h2 text-center>Mindful awareness means paying full attention to the moments of our lives as they are unfolding in the present moment, with a quality of openness, curiosity, and compassion for ourselves and others.</h2>\n            </ion-col>\n        </ion-row>\n    </section>\n    <section class="w-1024">\n        <ion-grid margin-top col-12>\n            <ion-row class="cont-main">\n                <ion-col padding col-lg-6 col-md-6 col-12 class="about-cont">\n                    <p class="gray-text"> Being able to tune into our lives in this way is one of the ultimate gifts we can give ourselves in both good times and bad.</p>\n                    <p class="gray-text">It is also possibly one of the greatest challenges we modern humans face in our increasingly fast-pasted, multi-tasking, distracted\n                        lives.</p>\n                </ion-col>\n                <ion-col padding col-lg-6 col-md-6 col-12 text-center class="about-img">\n                    <img src="assets/images/about_img_1.png">\n                </ion-col>\n            </ion-row>\n            <ion-row class="cont-main">\n                <ion-col padding col-lg-6 col-md-6 col-12 text-center class="about-img">\n                    <img src="assets/images/about_img_2.png">\n                </ion-col>\n                <ion-col padding col-lg-6 col-md-6 col-12 class="about-cont">\n                    <p class="gray-text"> Wakeful<sup>™</sup> is an online mindfulness training environment that supports the development of foundational skills, attitudes, and behaviors of mindfulness. Created by mindfulness meditation practitioners, teachers, and researchers, Wakeful<sup>™</sup> was carefully developed to meet each mindfulness student wherever they are in their learning journey.</p>\n                </ion-col>\n            </ion-row>\n            <ion-row class="cont-main">\n                <ion-col padding col-lg-6 col-md-6 col-12 class="about-cont">\n                    <p class="gray-text">  Wakeful<sup>™</sup> was also developed by and for mindfulness researchers, so this online learning environment can also serve as a secure data collection platform that contributes to increased knowledge, insights and discoveries about this ancient contemplative practice and how it might benefit our health and wellbeing.</p>\n                </ion-col>\n                <ion-col padding col-lg-6 col-md-6 col-12 text-center class="about-img">\n                    <img src="assets/images/about_img_3.png">\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n	</section>\n	<section class="video-section">\n		<div class="w-1024">\n			<ion-grid margin-top col-12>\n				<ion-row w-1024 justify-content-center class="video-bg">\n					<ion-col col-lg-6 col-md-6 col-12>\n						<h2 class="heading" text-center><strong>New Users</strong></h2>\n						<div class="videos">\n							<vg-player (onPlayerReady)="onPlayerReady($event, 0)">\n								<vg-play-pause class="ion-md-im-brandbubble f-11em" *ngIf="players[0]?.api?.state == \'paused\'" (click)="onPlay(players[0]?.api, 0)"></vg-play-pause>\n								<vg-play-pause class="pause-icon" *ngIf="players[0]?.api?.state != \'paused\'" (click)="onPlay(players[0]?.api, 0)"></vg-play-pause>\n								<vg-controls>\n									<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n									<vg-scrub-bar>\n										<vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n									</vg-scrub-bar>\n									<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n								</vg-controls>\n								<video #media [vgMedia]="media" id="singleVideo" preload="auto" poster="assets/images/about_01.jpg" [controls]="controls"\n									style="background: #F5F5F5">\n									<source *ngIf="user" src="assets/media/16a_Video_16a_Sizzle Reel_User.mp4">\n								</video>\n							</vg-player>\n						</div>\n					</ion-col>\n					<ion-col col-lg-6 col-md-6 col-12>\n						<h2 class="heading" text-center><strong>Administrators & Researchers</strong></h2>\n						<div class="videos">\n							<vg-player (onPlayerReady)="onPlayerReady($event, 1)">\n								<vg-play-pause class="ion-md-im-brandbubble f-11em" *ngIf="players[1]?.api?.state == \'paused\'"	(click)="onPlay(players[1]?.api, 1)"></vg-play-pause>\n								<vg-play-pause class="pause-icon" *ngIf="players[1]?.api?.state != \'paused\'" (click)="onPlay(players[1]?.api, 1)"></vg-play-pause>\n								<vg-controls>\n									<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n									<vg-scrub-bar>\n										<vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n									</vg-scrub-bar>\n									<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n								</vg-controls>\n\n								<video #mediaPlayer [vgMedia]="mediaPlayer" id="singleVideo" preload="auto" poster="assets/images/about_02.jpg" [controls]="controls"	style="background: #F5F5F5">\n									<source *ngIf="administartor" src="assets/media/16b_Video_16b_Sizzle Reel_Administrator.mp4">\n								</video>\n							</vg-player>\n						</div>\n					</ion-col>\n				</ion-row>\n			</ion-grid>\n		</div>\n    </section>\n    <static-footer></static-footer>\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\about\about.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactPage = (function () {
    function ContactPage(loadCtrl, navCtrl, formBuilder, authService, helper) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.helper = helper;
        this.form = this.formBuilder.group({
            first_name: [
                '',
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern(/^\s*[a-z]+\s*$/i), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]),
            ],
            last_name: [
                '',
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern(/^\s*[a-z]+\s*$/i), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]),
            ],
            email: [
                '',
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
                ]),
            ],
            message: [
                '',
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]),
            ],
        });
    }
    // input is focused or not
    ContactPage.prototype.isTouched = function (ctrl, flag) {
        this.form.controls[ctrl]['hasFocus'] = flag;
    };
    ContactPage.prototype.contactUs = function () {
        var _this = this;
        if (this.form.valid) {
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.authService.contact_us(this.form.value).then(function (result) {
                var response = result;
                _this.loading.dismiss();
                if (response['status'] == 'success') {
                    _this.helper.presentToast(response['msg'], response['status']);
                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                }
                else {
                    _this.helper.presentToast(response['msg'], response['status']);
                }
            }, function (err) {
            });
        }
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\contact\contact.html"*/'<!--\n  Generated template for the ContactUs page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n	<back-static-header></back-static-header>\n</ion-header> \n\n<ion-content>\n	<!-- <section class="bg-pink">\n		<ion-row justify-content-center>\n			<ion-col>\n				<h1 class="static-headings">Contact us</h1>\n			</ion-col>\n		</ion-row>\n	</section> -->\n	<ion-row>\n        <div class="head">\n            <h1>Contact Us</h1>\n        </div>\n    </ion-row>\n	<section class="w-750">\n		<ion-row>\n			<ion-grid text-center col-lg-12 col-md-12 col-12>\n				<p class="gray-text text-left">If you have any other inquiries for us that are not covered in our <span class="color">Program Overview</span> or <span class="color">Feedback sections</span>, please feel free to contact us below.</p>\n				<div>\n					<form class="contact-form" [formGroup]="form" (ngSubmit)="contactUs()" autocomplete="off">\n						<ion-row>\n							<ion-col col-12 class="sub-heading">\n								<strong>What is your name?</strong>\n							</ion-col>\n							<ion-col col-12>\n								<ion-item>\n									<ion-input placeholder="Enter your first name" value="" (focus)="isTouched(\'first_name\',true)" (focusout)="isTouched(\'first_name\',false)"\n									 formControlName="first_name" id="first_name" type="text" [class.invalid]="!form.controls.first_name.valid && (form.controls.first_name.dirty)"></ion-input>\n								</ion-item>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'first_name\'].hasError(\'required\') && form.controls[\'first_name\'].touched && !form.controls[\'first_name\'].hasFocus">First name field is required.</p>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'first_name\'].hasError(\'pattern\') && form.controls[\'first_name\'].touched && !form.controls[\'first_name\'].hasFocus">Please enter a valid first name.</p>\n							</ion-col>\n							<ion-col col-12>\n								<ion-item>\n									<ion-input placeholder="Enter your last name" value="" (focus)="isTouched(\'last_name\',true)" (focusout)="isTouched(\'last_name\',false)"\n									 formControlName="last_name" id="last_name" type="text" [class.invalid]="!form.controls.last_name.valid && (form.controls.last_name.dirty)"></ion-input>\n								</ion-item>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'last_name\'].hasError(\'required\') && form.controls[\'last_name\'].touched && !form.controls[\'last_name\'].hasFocus">Last name field is required.</p>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'last_name\'].hasError(\'pattern\') && form.controls[\'last_name\'].touched && !form.controls[\'last_name\'].hasFocus">Please enter a valid last name.</p>\n							</ion-col>\n							<ion-col col-12 class="next-sub-heading">\n								<strong>What is your email address?</strong>\n							</ion-col>\n							<ion-col col-12>\n								<ion-item>\n									<ion-input placeholder="Enter your email address" value="" (focus)="isTouched(\'email\',true)" (focusout)="isTouched(\'email\',false)"\n									 formControlName="email" id="email" type="text" [class.invalid]="!form.controls.email.valid && (form.controls.email.dirty)"></ion-input>\n								</ion-item>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'email\'].hasError(\'required\') && form.controls[\'email\'].touched && !form.controls[\'email\'].hasFocus">Email field is required.</p>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'email\'].hasError(\'pattern\') && form.controls[\'email\'].touched && !form.controls[\'email\'].hasFocus">Please enter a valid email.</p>\n							</ion-col>\n							<ion-col col-12 class="next-sub-heading">\n								<strong>What would you like to ask us today?</strong>\n							</ion-col>\n							<ion-col col-12 textarea>\n								<ion-item>\n									<ion-textarea class="" text-center rows=10 padding placeholder="Your answer goes here" class="text-input" (focus)="isTouched(\'message\',true)"\n									 (focusout)="isTouched(\'message\',false)" formControlName="message" id="message" [class.invalid]="!form.controls.message.valid && (form.controls.message.dirty)"></ion-textarea>\n								</ion-item>\n								<p class="error" col-12 ion-text color="danger" *ngIf="form.controls[\'message\'].hasError(\'required\') && form.controls[\'message\'].touched && !form.controls[\'message\'].hasFocus">Message field is required.</p>\n							</ion-col>\n						</ion-row>\n						<div col-lg-6 col-md-6 col-12 class="m-0-auto send-btn">\n							<button ion-button col-12 round color="primary">Send</button>\n						</div>\n					</form>\n				</div>\n			</ion-grid>\n		</ion-row>\n	</section>\n	<static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\contact\contact.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_helper__["a" /* Helper */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { HomePage } from '../home/home';

/**
 * Generated class for the OverviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OverviewPage = (function () {
    function OverviewPage(navCtrl, helper) {
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.data = [];
        this.data = [
            {
                title: 'What is Mindfulness?',
                desc: 'While many definitions exist, a basic description of mindfulness is simply “paying attention to the present moment without judgment.” This entails training our attention to notice what is happening from moment-to-moment, and to observe whatever is arising in our thoughts, emotions, and physical sensations with openness, curiosity, and non-judgment.',
                showDetails: false,
                icon: 'ios-arrow-down',
            },
            {
                title: 'What types of activities will I complete in the Wakeful™ tool?',
                desc: 'Throughout this course, you will learn different mindfulness meditation and mindful movement practices, as well as other exercises designed to teach you foundational skills of mindfulness.',
                icon: 'ios-arrow-down',
                showDetails: false,
            },
            {
                title: 'How long does the course take?',
                desc: 'The full recommended Wakeful™ program begins with a brief orientation session, followed by 9 consecutive classes, one per week. Each class is designed to take no more than 90 minutes, however this can be broken into smaller segments if necessary. The only difference is with Class 7, where you will be asked to participate in an extended period of guided mindful mediation and mindful movement practices (± 3 hours), which can help deepen your practice and enhance your skills. Because certain professional partners we collaborate with have the ability to tailor the number of classes offered, it is possible that you are enrolled in a Wakeful™ program that is not the full 9-week course. If you are uncertain, you can see the “Journey” icon in the dashboard of the app to see how many classes you have been assigned.',
                showDetails: false,
                icon: 'ios-arrow-down',
            },
            {
                title: 'Is there homework?',
                desc: 'Yes, each week you will be asked to practice mindfulness and yoga exercises using guided audio recordings, as well as additional reflective exercises. But try not to think of it as homework in the same way you might have thought about it in school. Instead, think of it as an opportunity to do a little “life work.”',
                showDetails: false,
                icon: 'ios-arrow-down',
            },
            {
                title: 'Can I still use the Wakeful™ tool after my course has finished?',
                desc: 'Yes, once you have finished your course, you will still have access to guided mindfulness recordings and educational content from the classes. You will also be able to use our meditation timer to support your practice.',
                showDetails: false,
                icon: 'ios-arrow-down',
            },
        ];
    }
    OverviewPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    };
    OverviewPage.prototype.ionViewCanEnter = function () {
        // this.helper.authenticated().then(
        // 	response => {
        // 	},
        // 	err => {
        // 	}
        // );
    };
    return OverviewPage;
}());
OverviewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-overview',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\overview\overview.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>\n\n    <ion-navbar>\n        <button ion-button icon-only>\n            <ion-icon name="logo"></ion-icon>\n        </button>\n\n        <a menuToggle end>\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\npadding-bottom margin\n</ion-header>-->\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n\n\n<ion-content>\n   <!--  <section class="bg-pink">\n        <ion-row justify-content-center>\n            <ion-col>\n                <h1 class="static-headings">FAQs</h1>\n            </ion-col>\n        </ion-row>\n    </section> -->\n    <ion-row>\n        <div class="head">\n            <h1>Frequently Asked Questions</h1>   \n        </div>\n    </ion-row>\n    <section class="w-900 faqs">\n        <ion-row justify-content-center padding-bottom margin-bottom>\n            <ion-grid col-lg-12 col-md-10 col-12>\n                <ion-list>\n                    <ion-item padding *ngFor="let d of data" (click)="toggleDetails(d)" text-center>\n                        <h1 class="sub-heading"><strong>{{d.title}}</strong>\n                            <ion-icon class="c-pink right" item-right [name]="d.icon"></ion-icon>\n                        </h1>\n                        <div padding-top *ngIf="d.showDetails" class="toggle-cont gray-text f-14">{{d.desc}}</div>\n                    </ion-item>\n                </ion-list>\n                <!-- <ion-row>\n                <ion-col col-12>\n                    <h1 class="sub-heading"><strong>What is Mindfulness?</strong></h1>\n                </ion-col>\n                <ion-col class="gray-text ">\n                    While many definitions exist, a basic description of mindfulness is simply “paying attention to the present moment without judgment.” This entails training our attention to notice what is happening from moment-to-moment, and to observe whatever is arising\n                    in our thoughts, emotions, and physical sensations with openness, curiosity, and non-judgment.,\n                </ion-col>\n\n            </ion-row>\n            <ion-row padding-top margin-top>\n                <ion-col col-12 class="gray-text">\n                    <h1 class="sub-heading"><strong>What types of activities will I complete in the Wakeful™ tool?</strong></h1>\n                </ion-col>\n                <ion-col class="gray-text">\n                    While many definitions exist, a basic description of mindfulness is simply “paying attention to the present moment without judgment.” This entails training our attention to notice what is happening from moment-to-moment, and to observe whatever is arising\n                    in our thoughts, emotions, and physical sensations with openness, curiosity, and non-judgment.\n                </ion-col>\n            </ion-row>\n            <ion-row padding-top margin-top>\n                <ion-col col-12 class="gray-text">\n                    <h1 class="sub-heading"><strong>How long does the course take?</strong></h1>\n                </ion-col>\n\n                <ion-col class="gray-text">\n                    Throughout this course, you will learn different meditation and yoga practices, as well as other exercises designed to teach you fundational skills of mindfulness.\n                </ion-col>\n            </ion-row>\n            <ion-row padding-top margin-top>\n                <ion-col col-12 class="gray-text">\n                    <h1 class="sub-heading"><strong>Is there homework?</strong></h1>\n                </ion-col>\n\n                <ion-col class="gray-text">\n                    Throughout this course, you will learn different meditation and yoga practices, as well as other exercises designed to teach you fundational skills of mindfulness.\n                </ion-col>\n            </ion-row>\n            <ion-row padding-top margin-top>\n                <ion-col col-12 class="gray-text">\n                    <h1 class="sub-heading"><strong>How do I know if this course is for me?</strong></h1>\n                </ion-col>\n                <ion-col class="gray-text">\n                    Throughout this course, you will learn different meditation and yoga practices, as well as other exercises designed to teach you fundational skills of mindfulness.\n                </ion-col>\n            </ion-row> -->\n            </ion-grid>\n        </ion-row>\n    </section>\n    <static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\overview\overview.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */]])
], OverviewPage);

//# sourceMappingURL=overview.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_constants__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ClassServiceProvider = (function () {
    function ClassServiceProvider(http, storage, platform) {
        this.http = http;
        this.storage = storage;
        this.platform = platform;
    }
    /**
     * @desc Used to add headers for each API call, if a user is logged in then add token header also
     * @param isLoggedIn
     */
    ClassServiceProvider.prototype.getHeaders = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + __WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].AUTH
        });
    };
    /**
     * @desc Common Success Callback function used from all API calls
     * @param res
     * @param resolve
     * @param reject
     * @param status
     */
    ClassServiceProvider.prototype.successCallback = function (res, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (res.headers.get('Content-type').indexOf('application/json') !== -1) {
            resolve(res.json());
        }
        else {
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /**
     * @desc Common Error Callback function used from all API calls
     * @param err
     * @param resolve
     * @param reject
     * @param status
     */
    ClassServiceProvider.prototype.errorCallback = function (err, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (err.headers.get('Content-type') === 'application/json') {
            reject(err.json().join());
        }
        else {
            console.log(err);
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    ClassServiceProvider.prototype.get_image = function (bg) {
        var image;
        if (this.platform.is('tablet')) {
            image = bg.tablet;
        }
        else if (this.platform.is('mobile')) {
            image = bg.mobile;
        }
        else {
            image = bg.desktop;
        }
        return image;
    };
    ClassServiceProvider.prototype.get_background_images = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.get('background_images').then(function (bg) {
                if (!bg) {
                    _this.set_background_images().then(function (res) {
                        resolve(_this.get_image(res));
                    });
                }
                else {
                    resolve(_this.get_image(bg));
                }
            });
        });
    };
    ClassServiceProvider.prototype.set_background_images = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/background-images', { headers: _this.getHeaders() }).subscribe(function (res) {
                var contentType = res.headers.get('Content-type');
                if (contentType.indexOf('application/json') != -1) {
                    var response = res.json();
                    if (response.hasOwnProperty('data')) {
                        _this.storage.set('background_images', response.data);
                        resolve(response.data);
                    }
                }
                else {
                }
            });
        });
    };
    /** Calling api for get  classes */
    ClassServiceProvider.prototype.classes = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/classes?course_id=' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/classes?course_id=' + __WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /**
     * Get course details by id.
     */
    ClassServiceProvider.prototype.course = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/course?course_id=' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/course?course_id=' + __WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for get setting */
    ClassServiceProvider.prototype.setting = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('study_id')) {
                    _this.storage.get('study_id').then(function (study_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/setting?study_id=' + study_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
            });
        });
    };
    /** Calling api for get dashboard detail */
    ClassServiceProvider.prototype.dashboard = function (course_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/dashboard/' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get class pages detail */
    ClassServiceProvider.prototype.getPage = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/pages', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add reflection answer */
    ClassServiceProvider.prototype.addReflectionAnswer = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http
                    .post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/reflection_answer', data, { headers: headers })
                    .subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add intention */
    ClassServiceProvider.prototype.addIntention = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http
                    .post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/intention_answer', data, { headers: headers })
                    .subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add status of audio/video track */
    ClassServiceProvider.prototype.fileTracking = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/file_tracking', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api get remaing time of udio/video track*/
    ClassServiceProvider.prototype.getTrackTime = function (file_info) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http
                    .post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/get_file_tracking', file_info, { headers: headers })
                    .subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get current position of page */
    ClassServiceProvider.prototype.getCurrentPosition = function (classes_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http
                    .get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/position?classes_id=' + classes_id, { headers: headers })
                    .subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get feedback information of course  */
    ClassServiceProvider.prototype.feedback = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    //this.storage.get('course_id').then(course_id => {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/feedback', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    //});
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/feedback', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for save  feedback answer */
    ClassServiceProvider.prototype.save_feedback = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/feedback', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get current class detail */
    ClassServiceProvider.prototype.getCurrentClass = function (course_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/current-class/' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get current class detail */
    ClassServiceProvider.prototype.updateTime = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/update-meditation-time', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    ClassServiceProvider.prototype.classList = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/classes-list', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    ClassServiceProvider.prototype.practiceFile = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/practice-files/' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/practice-files/' + __WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    return ClassServiceProvider;
}());
ClassServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* Platform */]])
], ClassServiceProvider);

//# sourceMappingURL=class-service.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommunityDiscussionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_community_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notification_notification__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CommunityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CommunityDiscussionPage = (function () {
    function CommunityDiscussionPage(loadCtrl, navCtrl, menu, helper, navParams, storage, communityService) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.helper = helper;
        this.navParams = navParams;
        this.storage = storage;
        this.communityService = communityService;
        this.title = "Community";
        this.message = '';
        this.comment = '';
        this.data = [];
        this.textbox = [];
        this.replyStatus = '';
        this.question_id = null;
        this.post_id = null;
        this.comment_id = null;
        this.commentBox = false;
        this.isWhitespaceComment = [];
        this.isWhitespaceSecComment = [];
        this.isWhitespaceMessage = false;
        this.course_id = '';
        this.communityList = [];
        this.page = 0;
        this.totalPage = 0;
        this.ContinueReading = [];
        this.ContinuePostReading = [];
        this.ContinueSecReading = [];
        this.viewReplies = [];
        this.viewPostReplies = [];
        this.viewSecondReplies = [];
        this.strLength = 350;
        this.strSecLength = 250;
        this.strPostLength = 290;
        this.username = '';
        this.profilePicture = '';
        this.redColor = ['A', 'G', 'M', 'S', 'Y'];
        this.orangeColor = ['B', 'H', 'N', 'T', 'Z'];
        this.yellowColor = ['C', 'I', 'O', 'U'];
        this.greenColor = ['D', 'J', 'P', 'V'];
        this.blueColor = ['E', 'K', 'Q', 'W'];
        this.purpleColor = ['F', 'L', 'R', 'X'];
        this.menu.enable(true);
        this.question_id = this.navParams.get('question_id');
        this.post_id = this.navParams.get('post_id');
        this.comment_id = this.navParams.get('comment_id');
    }
    CommunityDiscussionPage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__notification_notification__["a" /* NotificationPage */]);
    };
    CommunityDiscussionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var data = { 'question_id': this.question_id, 'post_id': this.post_id };
        this.getcommunity(data);
        this.storage.get('profile_picture').then(function (profile_picture) {
            _this.profilePicture = profile_picture;
        });
        this.storage.get('username').then(function (username) {
            _this.username = username;
        });
    };
    // Check to show class list or not
    CommunityDiscussionPage.prototype.getcommunity = function (question_id) {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.communityService.community(question_id).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.communityList = _this.result.data;
                _this.totalPage = _this.result.total_pages;
                _this.ContinueReading = [];
                _this.ContinuePostReading = [];
                _this.ContinuePostReading = [];
                _this.viewReplies = [];
                _this.viewPostReplies = [];
                _this.viewSecondReplies = [];
                _this.isWhitespaceComment = [];
                _this.isWhitespaceSecComment = [];
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    CommunityDiscussionPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.page = this.page + 1;
        setTimeout(function () {
            if (_this.page < _this.totalPage) {
                _this.communityService.communities(_this.page).then(function (response) {
                    _this.loading.dismiss();
                    _this.result = response;
                    if (_this.result.status == 'success') {
                        _this.communityList = _this.communityList.concat(_this.result.data);
                        _this.totalPage = _this.result.total_pages;
                    }
                }, function (err) {
                    _this.loading.dismiss();
                });
                infiniteScroll.complete();
            }
        }, 1000);
    };
    CommunityDiscussionPage.prototype.doReply = function (question_id, answer_id, comment_id, comment, commIndex, postRplyIndex) {
        var _this = this;
        if (question_id === void 0) { question_id = null; }
        if (answer_id === void 0) { answer_id = null; }
        if (comment_id === void 0) { comment_id = null; }
        var text = '';
        if (this.message != '') {
            this.isWhitespaceMessage = (this.message || '').trim().length === 0;
            if (this.isWhitespaceMessage) {
                return false;
            }
            text = this.message;
        }
        else if (comment != '' && !comment_id) {
            this.isWhitespaceComment[commIndex] = (comment || '').trim().length === 0;
            if (this.isWhitespaceComment[commIndex]) {
                return false;
            }
            text = comment;
        }
        else if (comment != '' && comment_id) {
            this.isWhitespaceSecComment[postRplyIndex] = (comment || '').trim().length === 0;
            if (this.isWhitespaceSecComment[postRplyIndex]) {
                return false;
            }
            text = comment;
        }
        if (text != '') {
            var obj = {
                "answer_id": answer_id,
                "comment": text,
                "parent_comment_id": comment_id,
                "question_id": question_id
            };
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.communityService.add_comment(obj).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status != 'error') {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                    var data = { 'question_id': _this.question_id, 'post_id': _this.post_id };
                    _this.getcommunity(data);
                    _this.comment = '';
                    _this.message = '';
                    _this.commentBox = false;
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            });
        }
    };
    CommunityDiscussionPage.prototype.replyClick = function (index) {
        var _this = this;
        this.isWhitespaceMessage = false;
        this.commentBox = false;
        this.message = '';
        this.indexValue = index;
        this.textbox.forEach(function (element, ind) {
            ind != index && (_this.textbox[ind] = false);
        });
        if (!this.textbox[index]) {
            this.textbox[index] = true;
        }
        else {
            this.textbox[index] = !this.textbox[index];
        }
    };
    CommunityDiscussionPage.prototype.setCommentStatus = function (status, answerId, question_id) {
        var _this = this;
        if (status != '') {
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            var obj = {
                "answer_id": answerId,
                "status": status,
                "question_id": question_id
            };
            this.loading.present();
            this.communityService.add_comment_status(obj).then(function (result) {
                _this.loading.dismiss();
                _this.data = result;
                if (_this.data.status != 'error') {
                    var data = { 'question_id': _this.question_id, 'post_id': _this.post_id };
                    _this.getcommunity(data);
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            });
        }
    };
    CommunityDiscussionPage.prototype.setReplyStatus = function (comment_id, status) {
        var _this = this;
        if (status != '') {
            var obj = {
                "answer_comments_id": comment_id,
                "status": status,
            };
            this.communityService.add_reply_status(obj).then(function (result) {
                _this.data = result;
                if (_this.data.status != 'error') {
                    _this.replyStatus = status;
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            }, function (err) {
                _this.helper.presentToast('Form Invalid', 'error');
            });
        }
    };
    CommunityDiscussionPage.prototype.continueReading = function (commIndex) {
        if (!this.ContinueReading[commIndex]) {
            this.ContinueReading[commIndex] = true;
        }
        else {
            this.ContinueReading[commIndex] = !this.ContinueReading[commIndex];
        }
    };
    CommunityDiscussionPage.prototype.continuePostReading = function (commIndex) {
        if (!this.ContinuePostReading[commIndex]) {
            this.ContinuePostReading[commIndex] = true;
        }
        else {
            this.ContinuePostReading[commIndex] = !this.ContinuePostReading[commIndex];
        }
    };
    CommunityDiscussionPage.prototype.continueSecReading = function (commIndex) {
        if (!this.ContinueSecReading[commIndex]) {
            this.ContinueSecReading[commIndex] = true;
        }
        else {
            this.ContinueSecReading[commIndex] = !this.ContinueSecReading[commIndex];
        }
    };
    CommunityDiscussionPage.prototype.toggleReplies = function (commIndex) {
        if (!this.viewReplies[commIndex]) {
            this.viewReplies[commIndex] = true;
        }
        else {
            this.viewReplies[commIndex] = !this.viewReplies[commIndex];
        }
    };
    CommunityDiscussionPage.prototype.togglePreviousReplies = function (commIndex) {
        if (!this.viewPostReplies[commIndex]) {
            this.viewPostReplies[commIndex] = true;
        }
        else {
            this.viewPostReplies[commIndex] = !this.viewPostReplies[commIndex];
        }
    };
    CommunityDiscussionPage.prototype.toggleSecondReplies = function (postRplyIndex) {
        if (!this.viewSecondReplies[postRplyIndex]) {
            this.viewSecondReplies[postRplyIndex] = true;
        }
        else {
            this.viewSecondReplies[postRplyIndex] = !this.viewSecondReplies[postRplyIndex];
        }
    };
    CommunityDiscussionPage.prototype.onKeydown = function (event) {
        event.preventDefault();
    };
    CommunityDiscussionPage.prototype.makeRead = function (comment_id) {
        if (comment_id == this.comment_id) {
            var data = { 'comment_id': comment_id };
            this.communityService.updateNotification(data).then(function (response) {
            });
        }
    };
    return CommunityDiscussionPage;
}());
CommunityDiscussionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-community-discussion',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\community-discussion\community-discussion.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <app-header [title]="Community"></app-header>\n</ion-header>\n \n<ion-content>\n    <section class="w-1024">\n        <div>\n            <p (click)="back()" class="back-btn"><img src=\'assets/images/arrow.svg\' width="13px" style="vertical-align: inherit;" /> BACK TO NOTIFICATION</p>\n        </div>\n        <div *ngIf="communityList.length > 0">\n            <ion-card class="card-bg" *ngFor="let community of communityList; let commIndex=index">\n                <ion-card-content class="community-user">\n                     <ion-row>\n                        <h1>\n                            Q: {{community.question_text}}\n                        </h1>\n                        <hr />\n                    </ion-row>\n                    \n                    <ion-row padding-top>\n                        <div col-lg-12 col-sm-12 col-12>\n                            <div class="commenter-img">\n    							<div *ngIf="community.discussion_list[0]?.profile_picture != \'\'; else smallelseBlockFirst">\n                                    <img class="round-image-small" src="assets/images/profile/{{community.discussion_list[0]?.profile_picture}}" alt="{{ community.discussion_list[0]?.profile_picture }}" />\n    							</div>\n    							<ng-template #smallelseBlockFirst>\n                                    <div id="small_circle" [ngClass]="redColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="circle-large icon-circle-outline" data-profile_initials>{{ community.discussion_list[0]?.username.charAt(0).toUpperCase() }}</div>\n                                </ng-template>\n                                \n    						</div>\n                            <div class="posted-text">\n                                <p>\n                                    <strong>{{community.discussion_list[0]?.username}}</strong>\n                                </p>\n                            </div>\n                        </div>\n                    </ion-row>\n                    <!-- <hr /> -->\n\n                    <ion-row padding-top padding-bottom class="comment-icons three-center">\n                        <!-- <div col-lg-3 col-sm-3 col-4 class="icon-center" padding-top margin-top>\n    						<a href="javascript:void(0)" margin-top class="d-block" (click)="commentClick()">\n    							<ion-icon class="reply-icon" name="im-reply"></ion-icon>\n    							<span class="f-big">Reply</span>\n    						</a>\n    					</div> -->\n    					<div class="icon-center">\n                            <div col-12><p class="community-text">{{community.discussion_list[0]?.answer.length > strLength && !ContinueReading[commIndex] ? community.discussion_list[0]?.answer.substr(0, strLength)+\'...\' : community.discussion_list[0]?.answer}} </p></div>\n    						\n    						<div col-12 *ngIf="community.discussion_list[0]?.answer.length > strLength && !ContinueReading[commIndex]">\n                                    <p class="continue-reading" (click)="continueReading(commIndex)">Continue Reading...</p></div>\n    						<div class="community-three mobile-icons">\n    							<div class="icon-center">\n    								<a class="active" href="javascript:void(0)">\n    									<ion-icon name="im-inspired"></ion-icon>\n    								</a>\n    								<span class="counter-status">{{community.discussion_list[0]?.post_rply.inspired}}</span>\n    							</div>\n    							<div class="icon-center">\n    								<a class="active" href="javascript:void(0)" >\n    									<ion-icon name="im-understood"></ion-icon>\n    								</a>\n    								<span class="counter-status">{{community.discussion_list[0]?.post_rply.understood}}</span>\n    							</div>\n    							<div class="icon-center">\n    								<a class="active" href="javascript:void(0)">\n    									<ion-icon name="im-grateful"></ion-icon>\n    								</a>\n    								<span class="counter-status">{{community.discussion_list[0]?.post_rply.grateful}}</span>\n    							</div>\n    						</div>\n    					</div>\n                        <div class="w-100 comment-section">\n                            <div class="three-icons-list">\n                                <div class="makes-feel-comment">\n                                    <p class="comment-texts">This comment makes me feel...</p>\n                                </div>\n                                <div class="inspire">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'INSPIRED\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'INSPIRED\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-inspired"></ion-icon>\n                                    <span>INSPIRED</span>\n                                </a>\n                                </div>\n                                <div class="understood">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'UNDERSTOOD\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'UNDERSTOOD\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-understood"></ion-icon>\n                                    <span>UNDERSTOOD</span>\n                                </a>\n                                </div>\n                                <div class="grateful">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'GRATEFUL\'}" href="javascript:void(0)" (click)="setCommentStatus(\'GRATEFUL\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-grateful"></ion-icon>\n                                    <span>GRATEFUL</span>\n                                </a>\n                                </div>\n                            </div>\n                        </div> \n                    </ion-row>\n                    <ion-row class="review-content">\n                        <div class="d-block" *ngIf="community.discussion_list[0]?.post_rply.data.length > 0">\n                            <!-- <div col-12><h4 class="view-head curser" *ngIf="!viewReplies[commIndex]" (click)="toggleReplies(commIndex)">View {{community.discussion_list[0]?.post_rply.data.length}} reply</h4></div> -->\n\n                            <div class="d-block curser" *ngIf="!viewPostReplies[commIndex] && community.discussion_list[0]?.post_rply.data.length > 3"><h4 class="view-head mb-15" (click)="togglePreviousReplies(commIndex)">View previous replies</h4></div>\n                            <!-- <div *ngIf="!!viewReplies[commIndex]"> -->\n\n                                <ion-row class="commenter-imges d-block main-block" *ngFor="let post_rply of community.discussion_list[0]?.post_rply.data;let postRplyIndex = index;">\n\n                                    <div class="d-block" *ngIf="!!viewPostReplies[commIndex] ? postRplyIndex < community.discussion_list[0]?.post_rply.data.length : postRplyIndex < 3">\n                                        <div class="add-bg d-block">\n                                            <div class="devide-text" attr.id="post-rply-{{post_rply.comment_id}}">\n                                            \n                                                <div class="reply-img" *ngIf="post_rply.profile_picture != \'\'; else smallelseBlockSec">\n                                                    <img class="round-image-small" src="assets/images/profile/{{post_rply.profile_picture}}" alt="{{ post_rply.profile_picture }}" />\n                                                </div>\n                \n                                                <ng-template class="more-reply" #smallelseBlockSec>\n                                                <div id="small_circle" [ngClass]="redColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline " data-profile_initials>{{ post_rply.username.charAt(0).toUpperCase() }}</div>\n                                                </ng-template>\n                                                <div class="add-bg-color" (mouseover)="post_rply?.is_read == \'0\' ?makeRead(post_rply?.comment_id) : \'\'" [ngClass]="post_rply?.comment_id == comment_id && post_rply?.is_read == \'0\' ? \'dark-bg-gray\' : \'light-gray\' ">\n                                                    <h4 class="view-name">{{post_rply.username}}</h4>\n                                                    <p class="community-text">{{post_rply.comment.length > strPostLength && !ContinuePostReading[postRplyIndex] ? post_rply.comment.substr(0, strPostLength)+\'...\' : post_rply.comment}}    </p>\n                                                <p *ngIf="post_rply.comment.length > strPostLength && !ContinuePostReading[postRplyIndex]" (click)="continuePostReading(postRplyIndex)" class="continue-reading curser">Continue Reading...</p>\n                                                </div>\n                                        \n                                                <div class="revert" *ngIf="post_rply.replies.length">\n                                                    <div *ngIf="!viewSecondReplies[postRplyIndex]" (click)="toggleSecondReplies(postRplyIndex)" class="curser mb-15">\n                                                        <img src="assets/images/replay_icon.png" width="50px" class="rep-icon">\n                                                        <!-- <img src="assets/images/default-avatar.png" width="50px"> -->\n                                                        <div class="more-reply" *ngIf="post_rply.replies[0]?.profile_picture != \'\'; else smallelseBlock">\n                                                            <img class="round-image-small" src="assets/images/profile/{{post_rply.replies[0]?.profile_picture}}" alt="{{ post_rply.replies[0]?.profile_picture }}" />\n                                                        </div>\n                                                        <ng-template #smallelseBlock>\n                                                        <div class="more-reply">\n                                                            <div id="small_circle" [ngClass]="redColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ post_rply.replies[0]?.username.charAt(0).toUpperCase() }}</div>\n                                                        </div>\n                                                    </ng-template>\n                                                        <span>{{post_rply.replies[0]?.username}} replied. {{post_rply.replies.length}} more replies.</span>\n                                                    </div>\n                                                    <div *ngIf="!!viewSecondReplies[postRplyIndex]" class="second-reply">\n                                                    <div class="devide-text">\n                                                    <ion-row class="commenter-imges" *ngFor="let second_rply of post_rply.replies;let secRplyIndex = index;">\n                                                            <div class="add-bg">\n                                                                <div class="reply-img" *ngIf="second_rply.profile_picture != \'\'; else smallelseBlockThird">\n                                                                    <img src="assets/images/replay_icon.png" width="30px" class="rep-icon">\n                                                                    <img class="round-image-small" src="assets/images/profile/{{second_rply.profile_picture}}" alt="{{ second_rply.profile_picture }}" width="70px" height="70px"/>\n                                                                </div>\n                                                                <ng-template #smallelseBlockThird>\n                                                                    <div class="more-reply">\n                                                                        <img src="assets/images/replay_icon.png" width="30px" class="rep-icon"> \n                                                                        <div id="small_circle" [ngClass]="redColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ second_rply.username.charAt(0).toUpperCase() }}</div>\n                                                                    </div>\n                                                                </ng-template>\n                                                                <div class="add-bg-color" (mouseover)="second_rply?.is_read == \'0\' ?makeRead(second_rply?.comment_id) : \'\'" [ngClass]="second_rply?.comment_id == comment_id && second_rply?.is_read == \'0\' ? \'dark-bg-gray\' : \'light-gray\' ">\n                                                                    <h4 class="view-name">{{second_rply.username}}</h4>\n                                                                    <p class="community-text">{{second_rply.reply.length > strSecLength && !ContinueSecReading[secRplyIndex] ? second_rply.reply.substr(0, strSecLength)+\'...\' : second_rply.reply}}    </p>\n                                                                <p *ngIf="second_rply.reply.length > strSecLength && !ContinueSecReading[secRplyIndex]" class="continue-reading curser" (click)="continueSecReading(secRplyIndex)">Continue Reading...</p>\n                                                                </div>\n                                                            </div>\n                                                    </ion-row>\n                                                </div>\n                                                </div>\n                                                </div>\n                                            </div>\n                                            <div class="inner-reply">\n                                                <div class="textarea-div d-block">\n                                                    <div class="area-img">\n                                                        <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFour">\n                                                            <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                                        </div>\n                                                        <ng-template #smallelseBlockFour>\n                                                            <div class="text-bg-img bg-primary circle-large icon-circle-outline"> \n                                                                <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                                            </div>\n                                                        </ng-template>\n                                                    </div>\n                                                    <div class="area">\n                                                        <textarea (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, community.discussion_list[0]?.answer_id, post_rply.comment_id, $event.target.value,commIndex, postRplyIndex)" name="comment" text-center padding placeholder="Write a reply" class="text-input"></textarea>\n                                                        <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceSecComment[postRplyIndex]">Please enter valid text.</span>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        \n                                    </div>\n                                </ion-row>\n                            <!-- </div> -->\n                        </div>\n                        <div *ngIf="community.discussion_list[0]?.post_rply.data.length == 0">\n                            <div col-12><h4>No replies yet</h4></div></div>\n                    </ion-row>\n                    <ion-row padding-bottom >\n                        <div class="textarea-div d-block">\n                            <div class="area-img">\n                                 <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFive">\n                                    <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                </div>\n                                <ng-template #smallelseBlockFive>\n                                    <div class="text-bg-img bg-primary circle-large icon-circle-outline">\n                                        <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                    </div>\n                                </ng-template>\n                            </div>\n                            <div class="area">\n                               <textarea  name="comment" (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, community.discussion_list[0]?.answer_id, null , $event.target.value, commIndex)" text-center padding placeholder="Write a reply"  class="text-input"></textarea>\n                             <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceComment[commIndex]">Please enter valid text.</span>\n                            </div>\n                        </div>\n    				</ion-row>\n                </ion-card-content>\n            </ion-card>\n            <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n                <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n            </ion-infinite-scroll>\n        </div>\n    </section>\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\community-discussion\community-discussion.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_community_service__["a" /* CommunityServiceProvider */]])
], CommunityDiscussionPage);

//# sourceMappingURL=community-discussion.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_community_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__community_discussion_community_discussion__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotificationPage = (function () {
    function NotificationPage(navCtrl, navParams, communityService, loadCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.communityService = communityService;
        this.loadCtrl = loadCtrl;
        this.notificationList = [];
        this.page = 0;
        this.totalPage = 0;
    }
    // ionViewDidLoad() {
    //   this.NotificationList();
    // }
    NotificationPage.prototype.ionViewWillEnter = function () {
        this.getNotificationList();
    };
    NotificationPage.prototype.getNotificationList = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.communityService.notification(this.page).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.notificationList = _this.result.data;
                _this.totalPage = _this.result.total_pages;
            }
        }, function (err) {
            console.log(err);
            _this.loading.dismiss();
        });
    };
    NotificationPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.page = this.page + 1;
        setTimeout(function () {
            _this.communityService.notification(_this.page).then(function (response) {
                _this.loading.dismiss();
                _this.result = response;
                if (_this.result.status == 'success') {
                    _this.notificationList = _this.notificationList.concat(_this.result.data);
                    _this.totalPage = _this.result.total_pages;
                }
            }, function (err) {
                console.log(err);
                _this.loading.dismiss();
            });
            infiniteScroll.complete();
        }, 1000);
    };
    NotificationPage.prototype.showPost = function (question_id, post_id, comment_id) {
        var data = { 'comment_id': comment_id };
        this.communityService.updateNotification(data).then(function (response) {
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__community_discussion_community_discussion__["a" /* CommunityDiscussionPage */], { 'question_id': question_id, 'post_id': post_id, 'comment_id': comment_id });
    };
    return NotificationPage;
}());
NotificationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notification',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\notification\notification.html"*/'<!--\n  Generated template for the NotificationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n\n<ion-content padding>\n	<section class="w-1024">\n		<ion-row *ngIf="notificationList.length > 0">\n			<ion-card class="card-bg" *ngFor="let notification of notificationList">\n				<ion-card-content [ngClass]="notification.is_read == \'0\' ? \'grey\' : \'\'" *ngIf="!notification.parent_comment_id">\n					<div class="list-content" (click)="showPost(notification.question_id, notification.post_id, notification.comment_id)">\n						<div col-lg-1 col-sm-2 col-3 class="letr-img">\n							<div class="letter-icon-blue text-bg-img circle-large icon-circle-outline" *ngIf="notification.commenter_profile_picture != \'\'; else smallelseBlock">\n								<img class="round-image-small" src="assets/images/profile/{{notification.commenter_profile_picture}}" alt="{{ notification.commenter_profile_picture }}" width="70px" height="70px"/>\n							</div>\n							<ng-template #smallelseBlock>\n								<div class="letter-icon-blue text-bg-img bg-primary circle-large icon-circle-outline">\n									<div id="small_circle" class="text-bg-img bg-primary circle-large icon-circle-outline" data-profile_initials>{{ notification.commenter_name.charAt(0).toUpperCase() }}</div>\n								</div>\n							</ng-template>\n						</div>\n						<div col-lg-11 col-sm-10 col-9>\n							<p>\n								<span class="name">{{notification.commenter_name}}</span>\n								commented on \n								<span class="post">your post</span>\n							</p>\n						</div>\n					</div>\n				</ion-card-content>\n				<ion-card-content [ngClass]="notification.is_read == \'0\' ? \'grey\' : \'\'" *ngIf="notification.parent_comment_id">\n					<div class="list-content" (click)="showPost(notification.question_id, notification.post_id, notification.comment_id)" >\n						<div col-lg-1 col-sm-3 col-2 class="letr-img">\n							<div class="letter-icon-blue text-bg-img circle-large icon-circle-outline" *ngIf="notification.sub_comm_profilepicture != \'\'; else smallelseBlock">\n								<img class="round-image-small" src="assets/images/profile/{{notification.sub_comm_profilepicture}}" alt="{{ notification.sub_comm_profilepicture }}" width="70px" height="70px"/>\n							</div>\n							<ng-template #smallelseBlock>\n								<div class="letter-icon-blue text-bg-img bg-primary circle-large icon-circle-outline">\n									<div id="small_circle" class="text-bg-img bg-primary circle-large icon-circle-outline" data-profile_initials>{{ notification.sub_commenter_name.charAt(0).toUpperCase() }}</div>\n								</div>\n							</ng-template>\n						</div>\n						<div col-lg-11 col-sm-3 col-10>\n							<p>\n								<span class="name">{{notification.sub_commenter_name}}</span>\n								replied to \n								<span class="name">{{notification.commenter_name}}\'s</span>\n								comment on\n								<span class="post">post</span>\n							</p>\n						</div>\n					</div>\n				</ion-card-content>\n			</ion-card>\n			<ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n				<ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n			</ion-infinite-scroll>\n		</ion-row>\n		<ion-row *ngIf="notificationList.length == 0">\n			<ion-col>\n				<h1 class="not-found">No notification found</h1>\n			</ion-col>\n		</ion-row>\n	</section>\n	\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\notification\notification.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_community_service__["a" /* CommunityServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
], NotificationPage);

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthServiceProvider = (function () {
    function AuthServiceProvider(http, storage, events) {
        this.http = http;
        this.storage = storage;
        this.events = events;
    }
    /**
     * @desc Used to add headers for each API call
     */
    AuthServiceProvider.prototype.getHeaders = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + __WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].AUTH
        });
    };
    /**
     * @desc Common Success Callback function used from all API calls
     * @param res
     * @param resolve
     * @param reject
     * @param status
     */
    AuthServiceProvider.prototype.successCallback = function (res, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (res.headers.get('Content-type').indexOf('application/json') !== -1) {
            resolve(res.json());
        }
        else {
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /**
     * @desc Common Error Callback function used from all API calls
     * @param err
     * @param resolve
     * @param reject
     * @param status
     */
    AuthServiceProvider.prototype.errorCallback = function (err, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (err.headers.get('Content-type') === 'application/json') {
            reject(err.json().join());
        }
        else {
            console.log(err);
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /** Calling api for check user is login or not */
    AuthServiceProvider.prototype.check_login = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.storage.get('study_id').then(function (study_id) {
                    _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check_login', { study_id: study_id }, { headers: headers }).subscribe(function (res) {
                        var contentType = res.headers.get('Content-type');
                        if (contentType.indexOf('application/json') != -1) {
                            var response = res.json();
                            if (response.status == 'INVALID_TOKEN') {
                                _this.storage.clear();
                                resolve(false);
                            }
                            else {
                                response.hasOwnProperty('settings') &&
                                    _this.storage.set('course_settings', response.settings);
                                _this.events.publish('course:updateSettings', response.settings);
                                resolve(true);
                            }
                        }
                        else {
                            _this.storage.clear();
                            resolve(false);
                        }
                    }, function (err) {
                        _this.storage.clear();
                        resolve(false);
                    });
                });
            });
        });
    };
    /** ACalling api for for user login with username/email and password */
    AuthServiceProvider.prototype.login = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/login', credentials, { headers: _this.getHeaders() })
                .subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for for user signup with user detail */
    AuthServiceProvider.prototype.signup_user_data = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/user-data', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    AuthServiceProvider.prototype.signup = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/signup', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    AuthServiceProvider.prototype.get_course_id = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.get('course_id').then(function (course_id) {
                resolve(course_id);
                return true;
            });
        });
    };
    AuthServiceProvider.prototype.check_user_class_count = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'classes/check-user-class-read-count/' + data.course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for user login/signup with the help of facebook/gmail */
    AuthServiceProvider.prototype.social_login = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/social-login', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for check email is exist in system or not */
    AuthServiceProvider.prototype.isEmailRegisterd = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check-email', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for check username is exist in system or not */
    AuthServiceProvider.prototype.isUsernameRegisterd = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check-username', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for logout function and expire user token  */
    AuthServiceProvider.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/logout', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get login user detail */
    AuthServiceProvider.prototype.get_profile = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/profile', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for check  login user current password */
    AuthServiceProvider.prototype.isCurrentPassword = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check_password', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /**
     * @desc Cheching a password entered is previous or not, called from edit profile page
     * @param data
     */
    AuthServiceProvider.prototype.isPreviousPassword = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check-previous-password', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for update login user detail */
    AuthServiceProvider.prototype.update_profile = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/profile', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for forgot password */
    AuthServiceProvider.prototype.forgot_password = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/forgot_password', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for forgot password */
    AuthServiceProvider.prototype.reset_password_code = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/reset_password_code', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /** Calling api for forgot password */
    AuthServiceProvider.prototype.reset_password = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/reset_password', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    /**
     * @desc Calling api for Contact Us
     * @param data
    */
    AuthServiceProvider.prototype.contact_us = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/contact_us', data, { headers: _this.getHeaders() }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
        });
    };
    AuthServiceProvider.prototype.getNotificationCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/check-notification-count', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    AuthServiceProvider.prototype.clearNotificationCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_5__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'auth/clear-notification-count', { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    return AuthServiceProvider;
}());
AuthServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Events */]])
], AuthServiceProvider);

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONSTANTS; });
var CONSTANTS = {
    APP_TITLE: 'wakeful',
    AUTH: 'YXBwVXNlcjpwd2FBcHBzQDIwMTg=',
    ASSETS_PATH: 'assets/',
    SESSION_TIMEOUT: 1800,
    CURRENT_COURSE: 1,
    ENV: {
        PROD: true,
    },
    API_ENDPOINT: 'https://pub1.brightoutcome-dev.com/wakeful/admin/Api/' // "http://localhost/wakeful/admin/Api/ //https://pub1.brightoutcome-dev.com/wakeful/admin/Api/
};
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__audio_audio__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__general_general__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__intention_intention__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__numbered_general_numbered_general__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__question_question__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__testimonial_testimonial__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__topic_topic__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__video_intro_video_intro__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { DashboardPage } from '../dashboard/dashboard';








/* Generated class for the ClassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClassPage = (function () {
    function ClassPage(loadCtrl, navParams, classService, authService, dataService, navCtrl, helper, storage, events) {
        var _this = this;
        this.loadCtrl = loadCtrl;
        this.navParams = navParams;
        this.classService = classService;
        this.authService = authService;
        this.dataService = dataService;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.storage = storage;
        this.events = events;
        this.classlist = [];
        this.title = 'Journey';
        this.msg = '';
        this.pages = {
            GENERAL: __WEBPACK_IMPORTED_MODULE_8__general_general__["a" /* GeneralPage */],
            AUDIO: __WEBPACK_IMPORTED_MODULE_7__audio_audio__["a" /* AudioPage */],
            PODCAST: __WEBPACK_IMPORTED_MODULE_7__audio_audio__["a" /* AudioPage */],
            VIDEO: __WEBPACK_IMPORTED_MODULE_15__video_intro_video_intro__["a" /* VideoIntroPage */],
            TOPIC: __WEBPACK_IMPORTED_MODULE_14__topic_topic__["a" /* TopicPage */],
            TESTIMONIAL: __WEBPACK_IMPORTED_MODULE_13__testimonial_testimonial__["a" /* TestimonialPage */],
            INTENTION: __WEBPACK_IMPORTED_MODULE_10__intention_intention__["a" /* IntentionPage */],
            QUESTION: __WEBPACK_IMPORTED_MODULE_12__question_question__["a" /* QuestionPage */],
            NUMBERED_GENERAL: __WEBPACK_IMPORTED_MODULE_11__numbered_general_numbered_general__["a" /* NumberedGeneralPage */],
        };
        this.REVISIT_CLASS = false;
        this.class_title = '';
        this.bg_image = '';
        this.course_id = '';
        //this.initClassPage()
        this.REVISIT_CLASS = false;
        this.classlist = [];
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
    }
    // Check to show class list or not
    ClassPage.prototype.initClassPage = function () {
        var _this = this;
        this.storage.get('course_settings').then(function (settings) {
            var is_revisit;
            if (_this.course_id) {
                is_revisit = (settings && settings[_this.course_id]) ? settings[_this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
            }
            _this.REVISIT_CLASS = is_revisit;
            setTimeout(function () {
                is_revisit ? _this.classes() : _this.startClass();
            }, 100);
        });
    };
    // Check if class is loaded and go to the current page of class
    ClassPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.initClassPage();
    };
    // All classes
    ClassPage.prototype.classes = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present().then(function () {
            _this.classService.classes().then(function (response) {
                _this.loading && _this.loading.dismiss();
                _this.result = response;
                if (_this.result.status == 'success') {
                    _this.classlist = _this.result.data;
                }
            }, function (err) {
                _this.loading && _this.loading.dismiss();
            });
        });
    };
    ClassPage.prototype.startClassFromList = function (class_id) {
        this.getCurrentPosition(class_id);
    };
    // Start current page for current class
    ClassPage.prototype.startClass = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present().then(function () {
            _this.classService.getCurrentClass(_this.course_id).then(function (response) {
                _this.loading && _this.loading.dismiss();
                _this.result = response;
                if (_this.result.status == 'success') {
                    //let class_detail = this.result.data;
                    var class_id = _this.result.data.class_id;
                    _this.getCurrentPosition(class_id);
                }
                else if (_this.result.status == 'NO_MORE_CLASS') {
                    _this.helper.presentToast(_this.result.msg, 'success');
                    _this.navCtrl.parent.select(0);
                }
            }, function (err) {
                _this.loading && _this.loading.dismiss();
            });
        });
    };
    // Get current page
    ClassPage.prototype.getPage = function (class_id, position) {
        var _this = this;
        var class_detail = { class_id: class_id, position: position };
        //this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        //this.loading.present().then(() => {
        this.classService.getPage(class_detail).then(function (response) {
            _this.result = response;
            if (_this.result.status == 'success') {
                var page_detail = _this.result.data;
                var pageType = page_detail.page_type;
                if (_this.pages.hasOwnProperty(pageType)) {
                    _this.navCtrl.push(_this.pages[pageType], { page_detail: page_detail, parent: _this });
                }
            }
            else if (_this.result.status == 'complete') {
                if (_this.result.redirectTo == 'homework') {
                    _this.helper.presentToast(_this.result.msg, 'success');
                    _this.dataService.changeClass(class_id);
                    _this.navCtrl.parent.select(4); // set homework page
                }
                else if (_this.result.redirectTo == 'next_class') {
                    _this.startClass();
                }
                else {
                    _this.helper.presentToast(_this.result.msg, 'success');
                    _this.navCtrl.parent.select(0);
                }
            }
            else if (_this.result.status == 'error') {
                _this.helper.presentToast(_this.result.msg, 'error');
                _this.navCtrl.parent.select(0);
            }
        }, function (err) {
            // this.loading.dismiss();
        });
        //})
    };
    // Get current page position for a class
    ClassPage.prototype.getCurrentPosition = function (class_id) {
        var _this = this;
        this.classService.getCurrentPosition(class_id).then(function (response) {
            _this.result = response;
            if (_this.result.status == 'success') {
                var current_class = _this.result.data;
                _this.getPage(current_class.classes_id, current_class.current_page_position);
            }
            else if (_this.result.status == 'INVALID_TOKEN') {
                _this.helper.presentToast('Your session has been expired. Please log in to continue.', 'error');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__home_home__["a" /* HomePage */]);
            }
            else {
                _this.getPage(class_id, 0);
            }
        }, function (err) {
            //this.loading.dismiss();
        });
    };
    return ClassPage;
}());
ClassPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-class',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\class\class.html"*/'<!--\n  Generated template for the ClassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n    <app-header [title]="title"></app-header>\n</ion-header>\n\n<ion-content [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section margin-top class="w-1024" [hidden]="!REVISIT_CLASS">\n        <ion-list inset class="btn-list">\n            <p class="text-light-gray mob-p-l-r" text-center>Here you can start the class. Select the class below you\'d like to start.</p>\n            <ion-item detail-push ion-item *ngFor="let class of classlist" (click)="startClassFromList(class.id)">\n                <h2><strong>{{ class.title }}</strong></h2>\n                <ion-icon name="ios-arrow-forward" item-right class="icon-gray"></ion-icon>\n                <!-- <ion-icon color="primary" name="ios-arrow-forward" item-right *ngIf="class.class_status==1"></ion-icon>\n                <ion-icon color="gray" name="ios-lock-outline" item-right *ngIf="class.class_status==0"></ion-icon> -->\n            </ion-item>\n        </ion-list>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\class\class.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_data_service__["a" /* DataServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */]])
], ClassPage);

//# sourceMappingURL=class.js.map

/***/ }),

/***/ 339:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 339;

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helper__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { AuthService } from "angular4-social-login";
// import { FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
// import { SocialUser } from "angular4-social-login";
var ForgotPasswordPage = (function () {
    function ForgotPasswordPage(loadCtrl, navCtrl, formBuilder, authService, helper) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.helper = helper;
        this.is_unique_email = false;
        this.is_unique_email_msg = '';
        this.is_success = false;
        this.forgotPasswordForm = this.formBuilder.group({
            email: [
                '',
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                ]),
            ],
        });
    }
    ForgotPasswordPage.prototype.forgotPassword = function () {
        var _this = this;
        if (this.forgotPasswordForm.valid && this.is_unique_email) {
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.authService.forgot_password(this.forgotPasswordForm.value).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status != 'error') {
                    _this.is_success = true;
                }
                else {
                    if (_this.data.data) {
                        _this.is_unique_email = false;
                        _this.is_unique_email_msg = 'Email address does not exist in system.';
                    }
                }
            }, function (err) {
                _this.loading.dismiss();
                _this.helper.presentToast('Form Invalid', 'error');
            });
        }
        else {
            this.validateAllFormFields(this.forgotPasswordForm);
        }
    };
    // input is focused or not
    ForgotPasswordPage.prototype.isTouched = function (ctrl, flag) {
        this.forgotPasswordForm.controls[ctrl]['hasFocus'] = flag;
    };
    // Check Email is exits or not
    ForgotPasswordPage.prototype.isEmailUnique = function () {
        var _this = this;
        if (this.forgotPasswordForm.controls['email'].valid) {
            this.authService.isEmailRegisterd({ 'current_email': this.forgotPasswordForm.controls['email'].value }).then(function (result) {
                _this.data = result;
                _this.is_unique_email = _this.data.status == 'success';
                _this.is_unique_email_msg = _this.is_unique_email ? '' : 'Email address does not exist in system.';
                _this.is_unique_email && _this.forgotPassword();
            }, function (err) {
            });
        }
        else {
            this.is_unique_email = true;
            this.is_unique_email_msg = '';
            this.validateAllFormFields(this.forgotPasswordForm);
        }
    };
    ForgotPasswordPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        //{1}
        Object.keys(formGroup.controls).forEach(function (field) {
            //{2}
            var control = formGroup.get(field); //{3}
            if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]) {
                //{4}
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]) {
                //{5}
                _this.validateAllFormFields(control); //{6}
            }
        });
    };
    return ForgotPasswordPage;
}());
ForgotPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-forgot-password',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\forgot-password\forgot-password.html"*/'<ion-content padding class="container password-contant">\n    <section class="w-1024">\n        <div col-12 text-center>\n            <img width="60px" src="assets/images/logo.png" />\n        </div>\n        <div col-lg-6 col-12 class="m-0-auto p-t-65" *ngIf="!is_success">\n            <h2 text-center padding-bottom class="f-26 primary-text">\n                <strong>Forgot your password?</strong>\n            </h2>\n            <p text-center padding-bottm class="f-18 gray-text">Please enter your email address and we will send you a link to reset your password.</p>\n            <form [formGroup]="forgotPasswordForm" autocomplete="off">\n                <ion-row>\n                    <ion-label stacked>EMAIL ADDRESS</ion-label>\n                    <ion-item>\n                        <ion-input formControlName="email" id="email" type="text" [class.invalid]="!forgotPasswordForm.controls.email.valid && (forgotPasswordForm.controls.email.dirty || submitAttempt)" (focus)="isTouched(\'email\',true)" (focusout)="isTouched(\'email\',false)"></ion-input>\n                        <!-- <ion-icon name="checkmark" item-right color="green" *ngIf="forgotPasswordForm.controls[\'email\'].valid && is_unique_email"></ion-icon> -->\n                    </ion-item>\n\n                    <p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="forgotPasswordForm.controls[\'email\'].hasError(\'required\') && forgotPasswordForm.controls[\'email\'].touched&& !forgotPasswordForm.controls[\'email\'].hasFocus">Email field is required.</p>\n                    <p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="forgotPasswordForm.controls[\'email\'].hasError(\'pattern\') && forgotPasswordForm.controls[\'email\'].touched&& !forgotPasswordForm.controls[\'email\'].hasFocus">Please enter a valid email.</p>\n                    <p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_unique_email && !forgotPasswordForm.controls[\'email\'].hasError(\'pattern\')">{{is_unique_email_msg}}</p>\n                </ion-row>\n                <div class="m-t-10">\n                    <div padding-top text-center>\n                        <button col-12 col-lg-8 col-md-6 ion-button round class="submit-btn" (click)="isEmailUnique()" color="primary">request reset link</button>\n                    </div>\n                    <div col-12 text-center>\n                        <a [navPush]="signInPage">Back to sign in</a>\n                    </div>\n                </div>\n            </form>\n        </div>\n\n        <div col-lg-6 col-12 class="m-0-auto p-t-65" *ngIf="is_success">\n            <h2 text-center margin-bottom padding-bottom class="f-26 primary-text">\n                <strong>Please check your email</strong>\n            </h2>\n            <p padding-top class="gray-text">\n                <strong>You will receive an email from us in the next few minutes, if the email you indicated exists in our system and you have\n					an active account. Please follow the instruction in the email to change your password.</strong>\n            </p>\n            <p text-center padding-bottm class="gray-text">If you don\'t see any email from us in your inbox, please check your junk mail folder as it may have been mistakenly categorized as spam.</p>\n\n            <div padding-top>\n                <div padding-top text-center>\n                    <a col-12 col-lg-8 col-md-6 ion-button round color="primary" [navPush]="signInPage">go back to homepage</a>\n                </div>\n            </div>\n        </div>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\forgot-password\forgot-password.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_helper__["a" /* Helper */]])
], ForgotPasswordPage);

//# sourceMappingURL=forgot-password.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassSchedulePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helper__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ClassSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClassSchedulePage = (function () {
    function ClassSchedulePage(navCtrl, navParams, classService, helper) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.classService = classService;
        this.helper = helper;
        this.title = 'Class Schedule';
        this.breadcrumb = ['Dashboard', 'Class Schedule'];
    }
    ClassSchedulePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.classList().then(function (res) {
            if (res['status'] == 'success') {
                _this.classList = res['data'];
            }
            else {
                _this.helper.presentToast(res['msg'], 'error');
            }
        });
    };
    return ClassSchedulePage;
}());
ClassSchedulePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-class-schedule',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\class-schedule\class-schedule.html"*/'<!--\n  Generated template for the ClassSchedulePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <app-header [title]="title"></app-header>\n</ion-header>\n<ion-content>\n  <section margin-top class="w-1024" >\n    <div class="w-1024">\n			<breadcrumb [data]="breadcrumb"></breadcrumb>\n		</div>\n      <ion-list inset class="btn-list">\n\n          <div class="table-schedule table-responsive">\n            <table>\n              <thead>\n                <th class="text-center">Class No.</th>\n                <th width="80%">Class Name</th>\n                <th>Start date</th>\n              </thead>\n              <tbody>\n                <ng-container *ngFor="let class of classList;  let i = index">\n                <tr *ngIf="class.week_number > 0">\n                  <td class="text-center" [ngClass]="[class.week_status == \'next_week\' ? \'bold\' : \'\']">{{i}}</td> \n                  <td [ngClass]="[class.week_status == \'next_week\' ? \'bold\' : \'\']">{{class.title}}<span>\n                    <img src="./assets/images/check-table.png" *ngIf="class.class_start_at" width="15" style="float: right;"></span></td>\n                  <td class="bold" *ngIf="class.week_status == \'next_week\'">\n                    {{ class.week_status == \'next_week\' && class.start_at ? (class.start_at | date : \'MM/dd/yyyy\') : \'N/A\'}}</td>\n                  <td *ngIf="class.week_status == \'current_week\'">\n                    {{ class.week_status == \'current_week\' ? (class.class_start_at ? (class.class_start_at | date : \'MM/dd/yyyy\') : (class.start_at | date : \'MM/dd/yyyy\')) : \'N/A\'}}</td>\n               </tr>\n              </ng-container>\n              </tbody>\n            </table>\n          </div>\n      </ion-list>\n  </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\class-schedule\class-schedule.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_helper__["a" /* Helper */]])
], ClassSchedulePage);

//# sourceMappingURL=class-schedule.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AudioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__class_class__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_constants__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { ProgressBarComponent } from '../../components/progress-bar/progress-bar';


var AudioPage = (function () {
    function AudioPage(classService, navCtrl, loadCtrl, modalCtrl, navParams, storage, authService) {
        var _this = this;
        this.classService = classService;
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.authService = authService;
        this.controls = false;
        this.autoplay = false;
        this.loop = false;
        this.preload = 'auto';
        this.bg_image = '';
        this.nativeFs = true;
        this.page_info = [];
        this.page_detail = [];
        this.audio_detail = [];
        this.file_status = false;
        this.AUDIO_VIDEO_BUTTON_ENABLED = false;
        this.course_id = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
    }
    AudioPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    // Check if class is loaded and go to the current page of class
    AudioPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.storage.get('course_settings').then(function (settings) {
            _this.AUDIO_VIDEO_BUTTON_ENABLED = false;
            if (_this.course_id) {
                if (settings[_this.course_id]['AUDIO/VIDEO_BUTTON_CLICKABLE_BEFORE_FINISHING'] == 1) {
                    _this.AUDIO_VIDEO_BUTTON_ENABLED = true;
                }
            }
            else {
                if (settings[__WEBPACK_IMPORTED_MODULE_7__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['AUDIO/VIDEO_BUTTON_CLICKABLE_BEFORE_FINISHING'] == 1) {
                    _this.AUDIO_VIDEO_BUTTON_ENABLED = true;
                }
            }
            _this.getTrackDetail();
        });
    };
    // Get the previous page in the current class
    AudioPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    // When the page to about to leave then update current time of audio (if playing)
    AudioPage.prototype.ionViewDidLeave = function () {
        var _this = this;
        this.api.pause();
        this.api.getDefaultMedia().subscriptions.pause.subscribe(function () {
            // Set the video to the beginning
            var time = {
                'current': _this.api.currentTime * 1000,
                'total': _this.totalTime,
            };
            _this.onPlay(time);
        });
    };
    // Event called when the player is ready to play
    AudioPage.prototype.onPlayerReady = function (api) {
        var _this = this;
        this.api = api;
        this.fsAPI = this.api.fsAPI;
        this.api.currentTime = 0;
        this.api.getDefaultMedia().subscriptions.ended.subscribe(function ($event) {
            // Set the video to the beginning
            var time = {
                'current': _this.api.currentTime * 1000,
                'total': _this.totalTime,
                'status': 'COMPLETED',
            };
            _this.onPlay(time);
            _this.api.getDefaultMedia().currentTime = 0;
            _this.file_status = true;
            // add auto finish and got to next page
            console.log('end audio', $event.type);
            if ($event.type === 'ended') {
                console.log('end ended');
                _this.nextPage();
            }
            //($event) => { console.log($event)}
        });
    };
    // Fetching current audio track details
    AudioPage.prototype.getTrackDetail = function () {
        var _this = this;
        //this.file_status = this.AUDIO_VIDEO_BUTTON_ENABLED;
        var file_info = {
            'user_page_activity_id': this.page_info.page_activity_id,
            'files_id': this.page_detail.files_id,
        };
        this.classService.getTrackTime(file_info).then(function (response) {
            _this.audio_detail = response;
            if (_this.audio_detail.status == 'success') {
                _this.audio_detail = _this.audio_detail.data;
                //this.api.currentTime = (this.audio_detail.status == 'COMPLETED') ? 0 : this.audio_detail.current_time;
                _this.api.currentTime = _this.audio_detail.current_time;
                _this.file_status = _this.AUDIO_VIDEO_BUTTON_ENABLED ? true : (_this.audio_detail.status == 'COMPLETED' ? true : false);
            }
            else {
                _this.file_status = _this.AUDIO_VIDEO_BUTTON_ENABLED;
            }
        }, function (err) {
        });
    };
    AudioPage.prototype.onPlay = function (time) {
        this.totalTime = (time.hasOwnProperty('total')) ? time.total : 0;
        var current_time = (time.hasOwnProperty('current')) ? time.current : 0;
        var left_time = (time.hasOwnProperty('left')) ? time.left : 0;
        var total = this.totalTime;
        var status = (time.hasOwnProperty('status')) ? time.status : 'STARTED';
        var file_info = {
            'current_time': current_time,
            'left_time': left_time,
            'total_time': total,
            'user_page_activity_id': this.page_info.page_activity_id,
            'files_id': this.page_detail.files_id,
            'file_status': status
        };
        this.classService.fileTracking(file_info).then(function (result) {
        }, function (err) {
        });
    };
    AudioPage.prototype.showAlert = function () {
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_modal_modal__["a" /* ModalComponent */], { 'body': this.page_detail.script, 'title': this.page_info.title });
        alert.present();
    };
    AudioPage.prototype.ngOnInit = function () { };
    AudioPage.prototype.nextPage = function () {
        console.log('blanck');
        if (this.file_status) {
            console.log('file_status', this.file_status);
            var class_id = this.page_info.classes_id;
            var position = this.page_info.position;
            this.parent.getPage(class_id, +position + 1);
        }
    };
    return AudioPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], AudioPage.prototype, "navBar", void 0);
AudioPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-audio',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\audio\audio.html"*/'<!--\n  Generated template for the AudioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center> {{ page_info.title }} </ion-title>\n        <a menuToggle end>\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 v-align-center" padding-top padding-bottom>\n        <!-- <div col-lg-8 col-12 class="m-0-auto audios"> -->\n        <div col-12 class="m-0-auto audios" padding>\n            <div margin-top padding-bottom>\n                <p class="gray-text">{{ page_detail.audio_text }}</p>\n            </div>\n            <vg-player (onPlayerReady)="onPlayerReady($event)">\n                <vg-controls>\n                    <vg-play-pause class="ion-md-im-brandbubble f-11em" (click)="onPlay(api.time)"></vg-play-pause>\n                    <vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n					<!-- <vg-scrub-bar-current-time></vg-scrub-bar-current-time> -->\n					<vg-scrub-bar>\n						<vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n					</vg-scrub-bar>\n                    <vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n\n                    <div text-left class="read-script">\n                        <span (click)="showAlert()">\n							<ion-icon name="im-script"></ion-icon>\n							<span>Read Script</span>\n                        </span>\n                        <vg-mute></vg-mute>\n                    </div>\n                </vg-controls>\n\n                <audio #media [vgMedia]="media" id="myAudio" preload="auto">\n					<source [src]="page_detail.url">\n				</audio>\n            </vg-player>\n\n\n        </div>\n        <div justify-content-center padding-top col-lg-8 col-12 class="m-0-auto">\n            <div col-12 col-lg-7 col-md-7 col-sm-12 class="m-0-auto">\n                <button ion-button col-12 full round type="button" [disabled]="!file_status" color="{{!file_status ? \'disabled\' : \'primary\'}}" (click)="nextPage()">{{ page_detail.button_text }}</button>\n            </div>\n        </div>\n    </section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n    <div class="w-1024">\n        <div col-md-12 col-sm-12 col-12 class="m-0-auto">\n            <progress-bar [progress]="progress"></progress-bar>\n        </div>\n    </div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\audio\audio.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */]])
], AudioPage);

//# sourceMappingURL=audio.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_class__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GeneralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralPage = (function () {
    function GeneralPage(navCtrl, navParams, loadCtrl, classService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadCtrl = loadCtrl;
        this.classService = classService;
        this.page_info = [];
        this.page_detail = [];
        this.title = "Test";
        this.bg_image = "";
        this.isShownImage = false;
        this.comunity = false;
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
    }
    GeneralPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            var imgType = _this.page_detail.remove_foreground_objects == '1' ? 'inner_page' : 'main_page';
            if (data.hasOwnProperty(imgType)) {
                _this.bg_image = data[imgType];
            }
        });
    };
    GeneralPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    GeneralPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    GeneralPage.prototype.nextPage = function () {
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.parent.getPage(class_id, +position + 1);
    };
    GeneralPage.prototype.practicePage = function () {
        this.navCtrl.parent.select(4);
    };
    GeneralPage.prototype.communityPage = function () {
        //this.navCtrl.setRoot(CommunityUserPage);
        this.navCtrl.parent.select(3);
    };
    return GeneralPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], GeneralPage.prototype, "navBar", void 0);
GeneralPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-general',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\general\general.html"*/'<!--\n  Generated template for the GeneralPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center> {{ page_info.title }} </ion-title>\n        <a menuToggle end class="menu-btn">\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\n</ion-header>\n\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 v-align-center">\n        <div col-12 col-lg-6 class="m-0-auto" padding-top>\n            <div text-center margin-bottom *ngIf="page_detail.url">\n                <img width="85px;" src=\'{{ page_detail.url }}\' alt="{{ page_detail.header }}" />\n            </div>\n            <section>\n                <h1 class="gray-text f-32 l-height-35" padding-bottom>\n                    <strong>{{ page_detail.header }}</strong>\n                </h1>\n                <p class="gray-text"> {{ page_detail.content }}</p>\n            </section>\n            <div justify-content-center padding-top>\n                <div col-12 col-lg-9 col-md-9 col-sm-12 *ngIf="page_detail.pages_id != page_info.last_page_id" class="m-0-auto">\n                    <button ion-button col-12 full round type="button" (click)="nextPage()">{{ page_detail.button_text }}</button>\n                </div>\n\n                <div col-12 col-lg-9 col-md-9 col-sm-12 *ngIf="page_detail.pages_id == page_info.last_page_id"  class="m-0-auto">\n                    <button ion-button col-12 full round type="button" (click)="nextPage()">Practice</button>\n                    <button ion-button col-12 full round type="button" (click)="communityPage()" >Community </button> \n                </div>\n            </div>\n        </div>\n    </section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n    <div class="w-1024">\n        <div col-md-12 col-sm-12 col-12 class="m-0-auto">\n            <progress-bar [progress]="progress"></progress-bar>\n        </div>\n    </div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\general\general.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_class_service__["a" /* ClassServiceProvider */]])
], GeneralPage);

//# sourceMappingURL=general.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntentionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__class_class__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the IntentionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IntentionPage = (function () {
    function IntentionPage(helper, classService, loadCtrl, navCtrl, navParams, formBuilder) {
        this.helper = helper;
        this.classService = classService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.page_info = [];
        this.page_detail = [];
        this.bg_image = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
        this.intentionForm = this.formBuilder.group({
            intention: [this.page_detail.sub_details['intention'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern(/^(?!\s*$).+/),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
                ]),],
        });
    }
    IntentionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    IntentionPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    IntentionPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    IntentionPage.prototype.nextPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        if (this.intentionForm.valid) {
            var intention = {
                intention: this.intentionForm.value.intention,
                intention_id: this.page_detail.id,
            };
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.classService.addIntention(intention).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status != 'error') {
                    _this.parent.getPage(class_id, +position + 1);
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                    _this.parent.getPage(class_id, +position + 1);
                }
            }, function (err) {
                _this.loading.dismiss();
                console.log(err);
                _this.helper.presentToast('Form Invalid', 'error');
            });
        }
    };
    return IntentionPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], IntentionPage.prototype, "navBar", void 0);
IntentionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-intention',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\intention\intention.html"*/'<ion-header>\n	<ion-navbar>\n		<ion-title text-center> {{ page_info.title }} </ion-title>\n		<a menuToggle end>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n	<section class="w-1024 v-align-center" padding-top padding-bottom>\n		<form [formGroup]="intentionForm" (ngSubmit)="nextPage()" autocomplete="off">\n			<div col-lg-8 col-12 class="m-0-auto">\n				<h1 padding-top text-center class="gray-text f-32">{{ page_detail.header }}</h1>\n				<ion-row>\n					<ion-col col-12 margin-bottom class="f-14 gray-text">\n						{{ page_detail.intro_text }}\n					</ion-col>\n					<ion-col col-12>\n						<ion-textarea padding text-center formControlName="intention" id="intention" [class.invalid]="!intentionForm.controls.intention.valid && (intentionForm.controls.intention.dirty)"\n						 rows=10 placeholder="Your answer goes here"></ion-textarea>\n						<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="intentionForm.controls[\'intention\'].hasError(\'required\') && intentionForm.controls[\'intention\'].touched && !intentionForm.controls[\'intention\'].hasFocus">Intention field is required.</p>\n					</ion-col>\n				</ion-row>\n				<div class="m-0-auto" text-center col-12 col-lg-7 col-md-7 col-sm-12>\n					<button ion-button col-12 round color="primary">{{ page_detail.button_text }}</button>\n				</div>\n			</div>\n		</form>\n	</section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n	<div class="w-1024">\n		<div col-md-12 col-sm-12 col-12 class="m-0-auto">\n			<progress-bar [progress]="progress"></progress-bar>\n		</div>\n	</div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\intention\intention.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
], IntentionPage);

//# sourceMappingURL=intention.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NumberedGeneralPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__class_class__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the NumberedGeneralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NumberedGeneralPage = (function () {
    function NumberedGeneralPage(helper, classService, loadCtrl, navCtrl, navParams) {
        this.helper = helper;
        this.classService = classService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.page_info = [];
        this.page_detail = [];
        this.answer = '';
        this.bg_image = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
    }
    NumberedGeneralPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    NumberedGeneralPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    NumberedGeneralPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    NumberedGeneralPage.prototype.nextPage = function () {
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.parent.getPage(class_id, +position + 1);
    };
    return NumberedGeneralPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], NumberedGeneralPage.prototype, "navBar", void 0);
NumberedGeneralPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-numbered-general',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\numbered-general\numbered-general.html"*/'<!--\n  Generated template for the NumberedGeneralPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n		<ion-title text-center> {{ page_info.header }} </ion-title>\n		<a menuToggle end>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n	<section class="w-1024 v-align-center" padding-top padding-bottom>\n			<div col-lg-8 col-12 class="m-0-auto">\n				<div text-center padding>\n					<ion-col col-4 col-sm-3 col-md-2>\n						<ion-icon [style.color]="page_detail.question_color" name="im-brandbubble" class="question-no">\n							<span>{{ page_detail.question_number }}</span>\n						</ion-icon>\n					</ion-col>\n				</div>\n				<ion-row>\n					<ion-col col-12>\n						<h3 class="gray-text f-26 l-height-35">\n							<strong>{{ page_detail.content }}</strong>\n						</h3>\n					</ion-col>\n				</ion-row>\n				<div text-center col-12 col-lg-7 col-md-7 col-sm-8 class="m-0-auto">\n					<button ion-button col-12 round color="primary" (click)="nextPage()">{{ page_detail.button_text }}</button>\n				</div>\n			</div>\n	</section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n	<div class="w-1024">\n		<div col-md-12 col-sm-12 col-12 class="m-0-auto">\n			<progress-bar [progress]="progress"></progress-bar>\n		</div>\n	</div>\n</ion-footer>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\numbered-general\numbered-general.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_2__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], NumberedGeneralPage);

//# sourceMappingURL=numbered-general.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__class_class__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the QuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuestionPage = (function () {
    function QuestionPage(helper, classService, loadCtrl, navCtrl, navParams, formBuilder) {
        this.helper = helper;
        this.classService = classService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.page_info = [];
        this.page_detail = [];
        this.answer = '';
        this.bg_image = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
        this.answerForm = this.formBuilder.group({
            answer: [this.page_detail.sub_details['answer'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern(/^(?!\s*$).+/),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
                ]),]
        });
    }
    QuestionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    QuestionPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    QuestionPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    QuestionPage.prototype.nextPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        if (this.answerForm.valid) {
            var answer = {
                'answer': this.answerForm.value.answer,
                'question_id': this.page_detail.id
            };
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.classService.addReflectionAnswer(answer).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status != 'error') {
                    _this.parent.getPage(class_id, +position + 1);
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                    _this.parent.getPage(class_id, +position + 1);
                }
            }, function (err) {
                console.log(err);
                _this.helper.presentToast('Form Invalid', 'error');
            });
        }
        else {
            this.validateAllFormFields(this.answerForm); //{7}
        }
    };
    QuestionPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        //{1}
        Object.keys(formGroup.controls).forEach(function (field) {
            //{2}
            var control = formGroup.get(field); //{3}
            if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]) {
                //{4}
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]) {
                //{5}
                _this.validateAllFormFields(control); //{6}
            }
        });
    };
    return QuestionPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], QuestionPage.prototype, "navBar", void 0);
QuestionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-question',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\question\question.html"*/'<!--\n  Generated template for the QuestionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n		<ion-title text-center> {{ page_info.title }} </ion-title>\n		<a menuToggle end>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n	<section class="w-1024 v-align-center" padding-top padding-bottom>\n		<form [formGroup]="answerForm" (ngSubmit)="nextPage()" autocomplete="off" class="wp-100">\n			<div col-lg-8 col-12 class="m-0-auto">\n				<div text-center padding>\n					<ion-col col-4 col-sm-3 col-md-2>\n						<ion-icon [style.color]="page_detail.question_color" name="im-brandbubble" class="question-no">\n							<span>{{ page_detail.question_number }}</span>\n						</ion-icon>\n					</ion-col>\n				</div>\n				<ion-row>\n					<ion-col col-12>\n						<h3 class="gray-text f-26 l-height-35">\n							<strong>{{ page_detail.question_text }}</strong>\n						</h3>\n					</ion-col>\n					<ion-col col-12>\n						<ion-textarea text-center padding formControlName="answer" id="answer" type="text" [class.invalid]="!answerForm.controls.answer.valid && (answerForm.controls.answer.dirty)"\n						 rows=10 placeholder="Your answer goes here"></ion-textarea>\n						<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="answerForm.controls[\'answer\'].hasError(\'required\') && answerForm.controls[\'answer\'].touched && !answerForm.controls[\'answer\'].hasFocus">Answer field is required.</p>\n						<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="answerForm.controls[\'answer\'].hasError(\'pattern\') && answerForm.controls[\'answer\'].touched && !answerForm.controls[\'answer\'].hasFocus">Please enter valid answer.</p>\n					</ion-col>\n				</ion-row>\n				<div text-center col-12 col-lg-7 col-md-7 col-sm-8 class="m-0-auto">\n					<button ion-button col-12 round color="primary">{{ page_detail.button_text }}</button>\n				</div>\n			</div>\n		</form>\n	</section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n	<div class="w-1024">\n		<div col-md-12 col-sm-12 col-12 class="m-0-auto">\n			<progress-bar [progress]="progress"></progress-bar>\n		</div>\n	</div>\n</ion-footer>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\question\question.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
], QuestionPage);

//# sourceMappingURL=question.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestimonialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__class_class__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { CONSTANTS } from '../../config/constants';


/**
 * Generated class for the TestimonialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TestimonialPage = (function () {
    function TestimonialPage(navCtrl, navParams, classService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.classService = classService;
        this.page_info = [];
        this.page_detail = [];
        this.bg_image = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
    }
    TestimonialPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    TestimonialPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    TestimonialPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    TestimonialPage.prototype.nextPage = function () {
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.parent.getPage(class_id, +position + 1);
    };
    // Method that shows the next slide
    TestimonialPage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    // Method that shows the previous slide
    TestimonialPage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    return TestimonialPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
], TestimonialPage.prototype, "slides", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], TestimonialPage.prototype, "navBar", void 0);
TestimonialPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-testimonial',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\testimonial\testimonial.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center> {{ page_info.title }} </ion-title>\n        <a menuToggle end>\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\n</ion-header>\n\n<ion-content [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 v-align-center" justify-content-center padding-bottom>\n        <div col-lg-8 col-11 class="m-0-auto">\n            <h1 class="gray-text f-32 l-height-35" padding text-center><strong>{{ page_detail.header }}</strong></h1>\n            <div class="slider-container">\n                <ion-slides pager="true">\n                    <ion-slide *ngFor="let testimonial of  page_detail.sub_details">\n                        <img class="round-image" src=\'{{testimonial.url}}\' alt="{{testimonial.name}}" />\n                        <p class="slide-text text-center gray-text">\n                            <ion-icon style="color:#af96c1" class="icon-flipped" ios="ios-quote" md="md-quote" float-left></ion-icon>\n                            {{testimonial.quote}}\n                            <ion-icon style="color:#af96c1" class="" ios="ios-quote" md="md-quote" float-right margin-right></ion-icon>\n                        </p>\n                        <p class="gray-text t-uper testi-name f-16" padding-bottom padding-top text-center>- {{testimonial.name}}\n\n                        </p>\n                    </ion-slide>\n\n                </ion-slides>\n                <div class="swiper-button-prev" (click)="slidePrev()"></div>\n                <div class="swiper-button-next" (click)="slideNext()"></div>\n            </div>\n            <div margin-top class="m-0-auto" text-center col-12 col-lg-7 col-md-7 col-sm-12>\n                <button ion-button col-12 round type="button" color="primary" (click)="nextPage()">{{ page_detail.button_text }}</button>\n            </div>\n        </div>\n    </section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n    <div class="w-1024">\n        <div col-md-12 col-sm-12 col-12 class="m-0-auto">\n            <progress-bar [progress]="progress"></progress-bar>\n        </div>\n    </div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\testimonial\testimonial.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */]])
], TestimonialPage);

//# sourceMappingURL=testimonial.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopicPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__class_class__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the TopicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TopicPage = (function () {
    function TopicPage(classService, modalCtrl, navCtrl, navParams, loadCtrl) {
        this.classService = classService;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadCtrl = loadCtrl;
        this.page_info = [];
        this.page_detail = [];
        this.title = "Topic";
        this.bg_image = "";
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
    }
    TopicPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    TopicPage.prototype.showDetail = function (topic) {
        var color = topic.topic_color;
        var title = topic.topic_title;
        var script = topic.topic_text;
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_modal_modal__["a" /* ModalComponent */], { 'title': title, 'body': script, 'bgColor': color, 'type': 'topic' });
        alert.present();
    };
    TopicPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    TopicPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +(position) - 1);
            }
        };
    };
    TopicPage.prototype.nextPage = function () {
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.parent.getPage(class_id, +(position) + 1);
    };
    return TopicPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], TopicPage.prototype, "navBar", void 0);
TopicPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-topic',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\topic\topic.html"*/'<!--\n  Generated template for the TopicPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center> {{ page_info.title }} </ion-title>\n        <a menuToggle end>\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\n</ion-header>\n\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 v-align-center" justify-content-center padding-bottom>\n        <p padding col-sm-7 col-12 class="f-14 gray-text">{{ page_detail.intro_text }}</p>\n        <ion-row col-lg-6 col-sm-8 col-12 padding-top class="m-0-auto" justify-content-start>\n            <ion-col class="bubble-icon" col-md-4 col-sm-6 *ngFor="let topic of page_detail.sub_details">\n                <ion-icon [style.color]="topic.topic_color" name="im-brandbubble" (click)="showDetail(topic)">\n                    <span>{{ topic.topic_title}} </span>\n                </ion-icon>\n            </ion-col>\n        </ion-row>\n        <div margin-top col-lg-8 col-12 class="m-0-auto">\n            <div text-center col-12 col-lg-7 col-md-7 col-sm-8 class="m-0-auto">\n                <!-- <div margin-top text-center padding-top class="m-0-auto" col-lg-7 col-sm-10 > -->\n                <button col-12 ion-button round type="button" color="primary" (click)="nextPage()">{{ page_detail.button_text }}</button>\n            </div>\n        </div>\n    </section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n    <div class="w-1024">\n        <div col-md-12 col-sm-12 col-12 class="m-0-auto">\n            <progress-bar [progress]="progress"></progress-bar>\n        </div>\n    </div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\topic\topic.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
], TopicPage);

//# sourceMappingURL=topic.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoIntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__video_video__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_class__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_class_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the VideoIntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoIntroPage = (function () {
    function VideoIntroPage(classService, navCtrl, navParams, tab) {
        this.classService = classService;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab = tab;
        this.page_info = [];
        this.page_detail = [];
        this.video_page = __WEBPACK_IMPORTED_MODULE_2__video_video__["a" /* VideoPage */];
        this.bg_image = '';
        this.page_info = this.navParams.get('page_detail');
        this.parent = this.navParams.get('parent');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
    }
    VideoIntroPage.prototype.playVideo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__video_video__["a" /* VideoPage */], { page_detail: this.page_info, parent: this });
    };
    VideoIntroPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
    };
    VideoIntroPage.prototype.ionViewDidLoad = function () {
        this.previousPage();
    };
    VideoIntroPage.prototype.ionViewWillUnload = function () {
        if (this.tab.getSelected().index != 1) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
            this.tab.select(0);
        }
        else {
            this.previousPage();
        }
    };
    VideoIntroPage.prototype.previousPage = function () {
        var _this = this;
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.navBar.backButtonClick = function () {
            if (position == 0) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__class_class__["a" /* ClassPage */]);
            }
            else {
                _this.parent.getPage(class_id, +position - 1);
            }
        };
    };
    VideoIntroPage.prototype.nextPage = function () {
        var class_id = this.page_info.classes_id;
        var position = this.page_info.position;
        this.parent.getPage(class_id, +position + 1);
    };
    return VideoIntroPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
], VideoIntroPage.prototype, "navBar", void 0);
VideoIntroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-video-intro',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\video-intro\video-intro.html"*/'<!--\n  Generated template for the VideoIntroPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center> {{ page_info.title }} </ion-title>\n        <a menuToggle end>\n            <ion-icon name="im-menu"></ion-icon>\n        </a>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 v-align-center video-intro-bg">\n        <div col-lg-5 col-12 class="m-0-auto intro-content">\n            <section class="video-intro-text">\n                <h1 class="gray-text f-32"><strong>{{ page_detail.header }}</strong></h1>\n                <p class="f-14 gray-text">{{ page_detail.pretext }}.</p>\n            </section>\n\n        </div>\n        <div justify-content-center margin-top padding-top text-center col-lg-8 col-12 class="m-0-auto">\n            <!-- <ion-col col-4 col-sm-3 col-md-2 (click)="playVideo()">\n                <ion-icon color="dashbg-blue" name="im-brandbubble" class="f-11em c-white">\n                    <span class="btn-play">\n                        <ion-icon ios="ios-play" md="md-play" color="pink"></ion-icon>\n                    </span>\n                </ion-icon>\n            </ion-col> -->\n            <div col-12 col-lg-7 col-md-7 col-sm-12 class="m-0-auto">\n                <button ion-button col-12 full round type="button" (click)="playVideo()" color="\'primary\'">Let\'s begin</button>\n            </div>\n        </div>\n           \n    </section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n    <div class="w-1024">\n        <div col-md-12 col-sm-12 col-12 class="m-0-auto">\n            <progress-bar [progress]="progress"></progress-bar>\n        </div>\n    </div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\video-intro\video-intro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Tabs */]])
], VideoIntroPage);

//# sourceMappingURL=video-intro.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_class_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the VideoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoPage = (function () {
    function VideoPage(classService, authService, navCtrl, loadCtrl, modalCtrl, navParams, storage) {
        var _this = this;
        this.classService = classService;
        this.authService = authService;
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.controls = false;
        this.page_info = [];
        this.page_detail = [];
        this.file_status = false;
        this.AUDIO_VIDEO_BUTTON_ENABLED = false;
        this.video_detail = [];
        this.bg_image = '';
        this.course_id = '';
        this.page_info = this.navParams.get('page_detail');
        this.page_detail = this.page_info.page_data;
        this.progress = this.page_info.percentage;
        this.parent = this.navParams.get('parent');
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
    }
    // Check if class is loaded and go to the current page of class
    VideoPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.storage.get('course_settings').then(function (settings) {
            _this.AUDIO_VIDEO_BUTTON_ENABLED = false;
            if (_this.course_id) {
                if (settings[_this.course_id]['AUDIO/VIDEO_BUTTON_CLICKABLE_BEFORE_FINISHING'] == 1) {
                    _this.AUDIO_VIDEO_BUTTON_ENABLED = true;
                }
            }
            else {
                if (settings[__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['AUDIO/VIDEO_BUTTON_CLICKABLE_BEFORE_FINISHING'] == 1) {
                    _this.AUDIO_VIDEO_BUTTON_ENABLED = true;
                }
            }
            _this.getTrackDetail();
        });
    };
    VideoPage.prototype.ionViewDidLeave = function () {
        var _this = this;
        this.api.pause();
        this.api.getDefaultMedia().subscriptions.pause.subscribe(function () {
            // Set the video to the beginning
            var time = {
                'current': _this.api.currentTime * 1000,
                'total': _this.totalTime,
            };
            _this.onPlay(time);
        });
    };
    // Player is ready and video is about to play
    VideoPage.prototype.onPlayerReady = function (api) {
        var _this = this;
        this.api = api;
        this.api.getDefaultMedia().subscriptions.ended.subscribe(function ($event) {
            // Set the video to the beginning
            var time = {
                current: _this.api.currentTime * 1000,
                total: _this.totalTime,
                status: 'COMPLETED',
            };
            _this.onPlay(time);
            _this.api.getDefaultMedia().currentTime = 0;
            _this.file_status = true;
            // 
            console.log('vedio', $event.type);
            if ($event.type === 'ended') {
                console.log('end vedio');
                _this.nextPage();
            }
        });
    };
    VideoPage.prototype.getPlayerStatus = function () {
        return this.api && this.api.state;
    };
    // Get current video details
    VideoPage.prototype.getTrackDetail = function () {
        var _this = this;
        var file_info = {
            user_page_activity_id: this.page_info.page_activity_id,
            files_id: this.page_detail.files_id,
        };
        this.classService.getTrackTime(file_info).then(function (response) {
            _this.video_detail = response;
            if (_this.video_detail.status == 'success') {
                _this.video_detail = _this.video_detail.data;
                //this.api.currentTime = this.video_detail.status == 'COMPLETED' ? 0 : this.video_detail.current_time;
                _this.api.currentTime = _this.video_detail.current_time;
                //this.file_status = this.video_detail.status == 'COMPLETED' ? true : false;
                _this.file_status = _this.AUDIO_VIDEO_BUTTON_ENABLED ? true : (_this.video_detail.status == 'COMPLETED' ? true : false);
            }
            else {
                _this.file_status = _this.AUDIO_VIDEO_BUTTON_ENABLED;
            }
        }, function (err) {
        });
    };
    // On play event to capture current time of video
    VideoPage.prototype.onPlay = function (time) {
        this.totalTime = time.hasOwnProperty('total') ? time.total : 0;
        var file_info = {
            current_time: time.hasOwnProperty('current') ? time.current : 0,
            left_time: time.hasOwnProperty('left') ? time.left : 0,
            total_time: this.totalTime,
            user_page_activity_id: this.page_info.page_activity_id,
            files_id: this.page_detail.files_id,
            file_status: time.hasOwnProperty('status') ? time.status : 'STARTED',
        };
        this.classService.fileTracking(file_info).then(function (result) {
        }, function (err) {
        });
    };
    VideoPage.prototype.showAlert = function (script) {
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_modal_modal__["a" /* ModalComponent */], {
            body: this.page_detail.script,
            title: this.page_info.title,
        });
        alert.present();
    };
    VideoPage.prototype.ngOnInit = function () { };
    // Calling next page of class
    VideoPage.prototype.nextPage = function () {
        console.log('blanck');
        if (this.file_status) {
            console.log('file_status', this.file_status);
            this.parent.nextPage();
        }
    };
    return VideoPage;
}());
VideoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-video',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\video\video.html"*/'<!--\n  Generated template for the VideoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n		<ion-title text-center> {{ page_info.title }} </ion-title>\n		<a menuToggle end>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content text-center [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n	<section class="w-1024 v-align-center" padding-top padding-bottom>\n		<div col-12 class="m-0-auto" padding>\n			<!-- col-lg-8  -->\n			<section padding-top padding-bottom>\n				<p class="gray-text">{{ page_detail.post_text }}.</p>\n			</section>\n			<div class="videos">\n				<vg-player (onPlayerReady)="onPlayerReady($event)">\n					<!-- <vg-overlay-play></vg-overlay-play> -->\n					<!-- <vg-overlay-play (click)="onPlay(api.time)" name="im-brandbubble"></vg-overlay-play> -->\n					<vg-play-pause class="ion-md-im-brandbubble f-11em" *ngIf="getPlayerStatus() == \'paused\'"\n						(click)="onPlay(api.time)"></vg-play-pause>\n					<vg-play-pause class="pause-icon" *ngIf="getPlayerStatus() != \'paused\'" (click)="onPlay(api.time)"></vg-play-pause>\n					<vg-controls>\n						 <!-- *ngIf="!controls" -->\n						<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n						<!-- <span class="divider"> / </span> -->\n						<vg-scrub-bar>\n							<vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n						</vg-scrub-bar>\n						<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n						<!-- <vg-scrub-bar-current-time [vgSlider]="file_status"></vg-scrub-bar-current-time> -->\n						\n						<div text-left class="read-script">\n							<span (click)="showAlert()">\n								<ion-icon name="im-script"></ion-icon>\n								<span>Read Script</span>\n							</span>\n							<vg-mute></vg-mute>\n							<vg-fullscreen></vg-fullscreen>\n						</div>\n					</vg-controls>\n\n					<video #media [vgMedia]="media" id="singleVideo" preload="auto" [controls]="controls"\n						style="background: #F5F5F5">\n						<source [src]="page_detail.url">\n					</video>\n				</vg-player>\n			</div>\n\n\n\n		</div>\n		<div justify-content-center padding-top col-lg-8 col-12 class="m-0-auto">\n			<div col-12 col-lg-7 col-md-7 col-sm-12 class="m-0-auto">\n				<!-- <div col-md-5 col-sm-12 col-12 class="m-0-auto"> -->\n				<button ion-button col-12 full round type="button" [disabled]="!file_status"\n					color="{{!file_status ? \'disabled\' : \'primary\'}}"\n					(click)="nextPage()">{{ page_detail.button_text }}</button>\n			</div>\n\n		</div>\n	</section>\n</ion-content>\n<ion-footer class="progress-wrapper">\n	<div class="w-1024">\n		<div col-md-12 col-sm-12 col-12 class="m-0-auto">\n			<progress-bar [progress]="progress"></progress-bar>\n		</div>\n	</div>\n</ion-footer>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\video\video.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], VideoPage);

//# sourceMappingURL=video.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_review_service__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__review_review__ = __webpack_require__(132);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the ReviewDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReviewDetailPage = (function () {
    function ReviewDetailPage(loadCtrl, navCtrl, navParams, menu, helper, modalCtrl, reviewService, storage) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.helper = helper;
        this.modalCtrl = modalCtrl;
        this.reviewService = reviewService;
        this.storage = storage;
        this.review_detail = [];
        this.players = [];
        this.breadcrumb = [];
        this.status = false;
        this.menu.enable(true);
    }
    // Middleware to check user is valid or not
    ReviewDetailPage.prototype.ionViewCanEnter = function () {
        this.helper.authenticated().then(function (response) {
        }, function (err) {
            console.log("err");
        });
    };
    // Call when user enter on the view
    ReviewDetailPage.prototype.ionViewWillEnter = function () {
        var review_id = this.navParams.get('review_id');
        this.getReviewDetail(review_id);
    };
    // Event called when the player is ready to play
    ReviewDetailPage.prototype.onPlayerReady = function (api, index, review) {
        var _this = this;
        setTimeout(function () {
            if (!_this.players[index]) {
                _this.players[index] = {};
            }
            _this.players[index].api = api;
            if (review.file_status.hasOwnProperty('current_time')) {
                _this.players[index].api.currentTime = review.file_status.current_time;
            }
            else {
                _this.players[index].api.currentTime = 0;
            }
            _this.players[index].api.getDefaultMedia().subscriptions.ended.subscribe(function () {
                // Set the video to the beginning
                var obj = {
                    time: {
                        'current': _this.players[index].api.currentTime * 1000,
                        'total': _this.players[index].api.currentTime * 1000,
                        'status': 'COMPLETED'
                    },
                    'reviews_files_id': review.id
                };
                _this.onPlay(obj);
                _this.players[index].api.getDefaultMedia().currentTime = 0;
            });
        });
    };
    // Get review detail
    ReviewDetailPage.prototype.getReviewDetail = function (review_id) {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.reviewService.review_detail(review_id).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                var data = _this.result.data;
                _this.review_detail = data;
                _this.breadcrumb = ['Review', _this.review_detail.title];
            }
        }, function (err) {
            console.log(err);
        });
    };
    // Update audio/video track detail
    ReviewDetailPage.prototype.onPlay = function (obj) {
        var current_time = obj.time.hasOwnProperty('current') ? obj.time.current : 0;
        var left_time = obj.time.hasOwnProperty('left') ? obj.time.left : 0;
        var total = obj.time.hasOwnProperty('total') ? obj.time.total : 0;
        var status = obj.time.hasOwnProperty('status') ? obj.time.status : 'STARTED';
        var reviews_files_id = obj.hasOwnProperty('reviews_files_id') ? obj.reviews_files_id : false;
        var reviews_id = obj.hasOwnProperty('reviews_id') ? obj.reviews_id : false;
        var files_id = obj.hasOwnProperty('files_id') ? obj.files_id : false;
        var file_info = {
            current_time: current_time,
            left_time: left_time,
            total_time: total,
            file_status: status,
            reviews_files_id: reviews_files_id,
            reviews_id: reviews_id,
            files_id: files_id,
            classes_id: this.review_detail.classes_id
        };
        this.reviewService.reviewTracking(file_info).then(function (result) {
        }, function (err) {
            console.log(err);
        });
    };
    // Show script content in modal
    ReviewDetailPage.prototype.showAlert = function (script, description) {
        var alert = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_modal_modal__["a" /* ModalComponent */], { 'body': description, 'title': script });
        alert.present();
    };
    ReviewDetailPage.prototype.ngOnInit = function () { };
    ReviewDetailPage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__review_review__["a" /* ReviewPage */]);
    };
    return ReviewDetailPage;
}());
ReviewDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-review-detail',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\review-detail\review-detail.html"*/'<ion-header>\n	<ion-navbar>\n		<ion-title text-center> {{(review_detail.title) ? review_detail.title : "Review Detail"}} </ion-title>\n		<a menuToggle end float-right>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content>\n	<section>\n		<div class="w-1024">\n			<!-- <breadcrumb [data]="breadcrumb"></breadcrumb> -->\n			<div>\n	        <p (click)="back()" class="back-btn"><img src=\'assets/images/arrow.svg\' width="13px" style="vertical-align: inherit;" /> BACK TO REVIEW</p>\n	    </div>\n		</div>\n		<ion-row class="w-1024" justify-content-center padding-left padding-right>\n\n			<div col-12 padding-bottom margin-bottom>\n				<p class="gray-text" text-center>{{(review_detail.intro_text) ? review_detail.intro_text : "Below is the course content from Class that you can watch\n					and listen to again ."}}</p>\n			</div>\n		</ion-row>\n	</section>\n	<div *ngIf="review_detail.review_data?.length > 0">\n		<section class="bg-odd-even" padding *ngFor="let review of review_detail.review_data; let index=index">\n			<div>\n				<ion-row class="w-1024 row-cont p-l-r-0-mob" justify-content-center padding>\n					<div col-lg-5 col-sm-12 col-12 class="{{review.type}} video-cont">\n						<vg-player (onPlayerReady)="onPlayerReady($event,index,review)">\n							<!-- <vg-overlay-play *ngIf="review.type === \'videos\'" (click)="onPlay({\'time\':players[index].api.time,\'reviews_files_id\':review.id,\'reviews_id\':review.reviews_id,\'files_id\':review.files_id})"></vg-overlay-play> -->\n							<vg-play-pause *ngIf="review.type === \'videos\' && players[index]?.api.state == \'paused\' || players[index]?.api.state == \'ended\'"  class="ion-md-im-brandbubble f-11em" (click)="onPlay({\'time\':players[index].api.time,\'reviews_files_id\':review.id,\'reviews_id\':review.reviews_id,\'files_id\':review.files_id})"></vg-play-pause>\n\n							<vg-controls>\n								<vg-play-pause *ngIf="review.type === \'audios\'" class="ion-md-im-brandbubble f-11em" (click)="onPlay({\'time\':players[index].api.time,\'reviews_files_id\':review.id,\'reviews_id\':review.reviews_id,\'files_id\':review.files_id})"></vg-play-pause>\n								<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n								<!-- <vg-scrub-bar-current-time></vg-scrub-bar-current-time> -->\n								<vg-scrub-bar [vgSlider]="true">\n									<vg-scrub-bar-current-time [vgSlider]="true"></vg-scrub-bar-current-time>\n								</vg-scrub-bar>\n								<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n\n								<div text-left class="read-script">\n									<vg-play-pause class="pause-icon" *ngIf="review.type === \'videos\' && players[index]?.api.state != \'paused\' && players[index]?.api.state != \'ended\'" (click)="onPlay({\'time\':players[index].api.time,\'reviews_files_id\':review.id,\'reviews_id\':review.reviews_id,\'files_id\':review.files_id})"></vg-play-pause>\n									<span *ngIf="review.type === \'videos\'" (click)="showAlert(review.video_title,review.video_script)">\n										<ion-icon name="im-script"></ion-icon>\n										<span>Read Script</span>\n									</span>\n									<span *ngIf="review.type === \'audios\'" (click)="showAlert(review.audio_title,review.audio_script)">\n										<ion-icon name="im-script"></ion-icon>\n										<span>Read Script</span>\n									</span>\n									<vg-mute></vg-mute>\n									<vg-fullscreen *ngIf="review.type !== \'audios\'"></vg-fullscreen>\n								</div>\n							</vg-controls>\n							<audio seekTime="3" *ngIf="review.type === \'audios\'" #media [vgMedia]="media" preload="auto">\n								<source [src]="review.url">\n							</audio>\n							<video *ngIf="review.type === \'videos\'" #media [vgMedia]="media" preload="auto" [controls]="controls">\n								<source [src]="review.url">\n							</video>\n						</vg-player>\n\n					</div>\n					<div col-lg-7 col-sm-12 col-12 class="video-cont">\n						<div class="video-content text-hide">\n							<h1 class="gray-text text-concat-hide">\n								<strong *ngIf="review.type === \'videos\'">{{review.video_title}}</strong>\n								<strong *ngIf="review.type === \'audios\'">{{review.audio_title}}</strong>\n							</h1>\n							<p class="gray-text text-concat-hide">{{review.pretext}}</p>\n						</div>\n\n					</div>\n\n				</ion-row>\n			</div>\n		</section>\n	</div>\n\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\review-detail\review-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_review_service__["a" /* ReviewServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
], ReviewDetailPage);

//# sourceMappingURL=review-detail.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeworkReadingDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__homework_homework__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_homework_service__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the HomeworkReadingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomeworkReadingDetailPage = (function () {
    function HomeworkReadingDetailPage(loadCtrl, navCtrl, navParams, helper, events, sanitizer, homeworkService) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.helper = helper;
        this.events = events;
        this.sanitizer = sanitizer;
        this.homeworkService = homeworkService;
        this.full_detail = [];
        this.exercise_detail = [];
        this.breadcrumb = [];
        this.type = '';
        this.players = [];
        this.homework_page = __WEBPACK_IMPORTED_MODULE_3__homework_homework__["a" /* HomeworkPage */];
        // this.navBar.backButtonClick = () => {
        // 	this.navCtrl.setRoot(ClassPage);
        // }
    }
    // Middleware to check user is valid or not
    HomeworkReadingDetailPage.prototype.ionViewCanEnter = function () {
        this.helper.authenticated().then(function (response) {
        }, function (err) {
        });
    };
    // Call when user enter on the view
    HomeworkReadingDetailPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.events.publish('homework:disableTabs', true);
        this.full_detail = this.navParams.get('detail');
        this.id = this.full_detail['reading_id']; //reading id 
        this.type = this.navParams.get('type');
        this.exercise_detail = this.navParams.get('exercise_detail');
        this.breadcrumb = ['Homework', this.exercise_detail && this.exercise_detail.hasOwnProperty('title') ? this.exercise_detail['title'] : '', this.full_detail && this.full_detail.hasOwnProperty('title') ? this.full_detail['title'] : ''];
        this.full_detail['reading_detail'] = this.sanitizer.bypassSecurityTrustHtml(this.full_detail['reading_detail']);
        clearInterval(this.interval);
        this.interval = setInterval(function () {
            if (document.hasFocus()) {
                _this.homeworkService.updateReadingTime({ id: _this.id }).then(function (response) {
                    console.log('data', response);
                }, function (err) {
                });
            }
        }, 10000);
    };
    HomeworkReadingDetailPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.interval);
        this.events.publish('homework:disableTabs', false);
    };
    // Event called when the player is ready to play
    HomeworkReadingDetailPage.prototype.onPlayerReady = function (api) {
        this.api = api;
    };
    return HomeworkReadingDetailPage;
}());
HomeworkReadingDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-homework-reading-detail',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\homework-reading-detail\homework-reading-detail.html"*/'<!-- <ion-header>\n	<ion-navbar>\n		<ion-title text-center>Homework</ion-title>\n		<a menuToggle end float-right>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header> -->\n<ion-header>\n	<ion-navbar>\n		<ion-title text-center>Homework</ion-title>\n		<a menuToggle end>\n			<ion-icon name="im-menu"></ion-icon>\n		</a>\n	</ion-navbar>\n</ion-header>\n<ion-content>\n\n	<section class="h-reading-detail" margin-bottom>\n		<ion-row class="w-1024" justify-content-center padding-left padding-right>\n			<div col-12 padding-bottom>\n				<h1 class="gray-text">\n					{{(full_detail?.title) ? full_detail?.title : ""}}\n				</h1>\n			</div>\n		</ion-row>\n		<ion-row class="w-1024 h-reading-detail-bg" *ngIf="(type==\'reading\')">\n			<div col-12 [innerHTML]="full_detail?.reading_detail"></div>\n		</ion-row>\n		<ion-row class="w-1024 h-reading-detail-bg" *ngIf="(type==\'podcast\')">\n			<div col-12 class="m-0-auto audios" padding>\n				<div margin-top padding-bottom>\n					<p class="gray-text">{{(full_detail?.script) ? full_detail?.script : ""}}</p>\n				</div>\n				<vg-player (onPlayerReady)="onPlayerReady($event)">\n					<vg-controls>\n						<vg-play-pause class="ion-md-im-brandbubble f-11em"></vg-play-pause>\n						<vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>\n						<vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n						<vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>\n						<div text-left class="read-script">\n							<vg-mute></vg-mute>\n						</div>\n					</vg-controls>\n\n					<audio #media [vgMedia]="media" id="myAudio" preload="auto">\n						<source [src]="full_detail?.url">\n					</audio>\n				</vg-player>\n			</div>\n		</ion-row>\n	</section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\homework-reading-detail\homework-reading-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_5__providers_homework_service__["a" /* HomeworkServiceProvider */]])
], HomeworkReadingDetailPage);

//# sourceMappingURL=homework-reading-detail.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommunityUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_community_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CommunityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CommunityUserPage = (function () {
    function CommunityUserPage(loadCtrl, navCtrl, menu, helper, storage, authService, communityService) {
        var _this = this;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.helper = helper;
        this.storage = storage;
        this.authService = authService;
        this.communityService = communityService;
        this.title = "Community";
        this.message = '';
        this.comment = '';
        this.data = [];
        this.textbox = [];
        this.breadcrumb = [];
        this.status = '';
        this.replyStatus = '';
        this.question_id = false;
        this.commentBox = false;
        this.isWhitespaceComment = [];
        this.isWhitespaceSecComment = [];
        this.isWhitespaceMessage = false;
        this.course_id = '';
        this.communityList = [];
        this.page = 0;
        this.totalPage = null;
        this.ContinueReading = [];
        this.ContinuePostReading = [];
        this.ContinueSecReading = [];
        this.ContinueReadingD = [];
        this.ContinuePostReadingD = [];
        this.ContinueSecReadingD = [];
        this.viewReplies = [];
        this.viewPostReplies = [];
        this.viewSecondReplies = [];
        this.strLength = 350;
        this.strSecLength = 250;
        this.strPostLength = 290;
        this.username = '';
        this.profilePicture = '';
        this.redColor = ['A', 'G', 'M', 'S', 'Y'];
        this.orangeColor = ['B', 'H', 'N', 'T', 'Z'];
        this.yellowColor = ['C', 'I', 'O', 'U'];
        this.greenColor = ['D', 'J', 'P', 'V'];
        this.blueColor = ['E', 'K', 'Q', 'W'];
        this.purpleColor = ['F', 'L', 'R', 'X'];
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
    }
    CommunityUserPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.page = 0;
        this.getcommunityList(this.page);
        this.storage.get('profile_picture').then(function (profile_picture) {
            _this.profilePicture = profile_picture;
        });
        this.storage.get('username').then(function (username) {
            _this.username = username;
        });
    };
    // Check to show class list or not
    CommunityUserPage.prototype.getcommunityList = function (page) {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.communityService.communities(page).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.communityList = _this.result.data;
                _this.totalPage = _this.result.total_pages;
                _this.ContinueReading = [];
                _this.ContinueReadingD = [];
                _this.ContinuePostReading = [];
                _this.ContinuePostReadingD = [];
                _this.ContinueSecReading = [];
                _this.ContinueSecReadingD = [];
                _this.viewReplies = [];
                _this.viewPostReplies = [];
                _this.viewSecondReplies = [];
                _this.isWhitespaceComment = [];
                _this.isWhitespaceSecComment = [];
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    CommunityUserPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.page = this.page + 1;
        setTimeout(function () {
            if (_this.page < _this.totalPage) {
                _this.communityService.communities(_this.page).then(function (response) {
                    _this.loading.dismiss();
                    _this.result = response;
                    if (_this.result.status == 'success') {
                        _this.communityList = _this.communityList.concat(_this.result.data);
                        _this.totalPage = _this.result.total_pages;
                    }
                }, function (err) {
                    _this.loading.dismiss();
                });
                infiniteScroll.complete();
            }
        }, 1000);
    };
    CommunityUserPage.prototype.doReply = function (question_id, answer_id, comment_id, comment, commIndex, postRplyIndex) {
        var _this = this;
        if (question_id === void 0) { question_id = null; }
        if (answer_id === void 0) { answer_id = null; }
        if (comment_id === void 0) { comment_id = null; }
        var text = '';
        if (this.message != '') {
            this.isWhitespaceMessage = (this.message || '').trim().length === 0;
            if (this.isWhitespaceMessage) {
                return false;
            }
            text = this.message;
        }
        else if (comment != '' && !comment_id) {
            this.isWhitespaceComment[commIndex] = (comment || '').trim().length === 0;
            if (this.isWhitespaceComment[commIndex]) {
                return false;
            }
            text = comment;
        }
        else if (comment != '' && comment_id) {
            this.isWhitespaceSecComment[postRplyIndex] = (comment || '').trim().length === 0;
            if (this.isWhitespaceSecComment[postRplyIndex]) {
                return false;
            }
            text = comment;
        }
        if (text != '') {
            var obj = {
                "answer_id": answer_id,
                "comment": text,
                "parent_comment_id": comment_id,
                "question_id": question_id
            };
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            this.loading.present();
            this.communityService.add_comment(obj).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status != 'error') {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                    if (_this.page == _this.totalPage) {
                        _this.page = _this.page - 1;
                    }
                    _this.getcommunityList(_this.page);
                    _this.comment = '';
                    _this.message = '';
                    _this.commentBox = false;
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            });
        }
    };
    CommunityUserPage.prototype.setCommentStatus = function (status, answerId, question_id) {
        var _this = this;
        if (status != '') {
            this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
            var obj = {
                "answer_id": answerId,
                "status": status,
                "question_id": question_id
            };
            this.loading.present();
            this.communityService.add_comment_status(obj).then(function (result) {
                _this.loading.dismiss();
                _this.data = result;
                if (_this.data.status != 'error') {
                    if (_this.page == _this.totalPage) {
                        _this.page = _this.page - 1;
                    }
                    _this.getcommunityList(_this.page);
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            });
        }
    };
    CommunityUserPage.prototype.setReplyStatus = function (comment_id, status) {
        var _this = this;
        if (status != '') {
            var obj = {
                "answer_comments_id": comment_id,
                "status": status,
            };
            this.communityService.add_reply_status(obj).then(function (result) {
                _this.data = result;
                if (_this.data.status != 'error') {
                    _this.replyStatus = status;
                }
                else {
                    _this.helper.presentToast(_this.data.msg, 'error');
                }
            }, function (err) {
                _this.helper.presentToast('Form Invalid', 'error');
            });
        }
    };
    CommunityUserPage.prototype.continueReading = function (commIndex) {
        if (!this.ContinueReading[commIndex]) {
            this.ContinueReading[commIndex] = true;
        }
        else {
            this.ContinueReading[commIndex] = !this.ContinueReading[commIndex];
        }
    };
    CommunityUserPage.prototype.continueReadingD = function (dIndex) {
        if (!this.ContinueReadingD[dIndex]) {
            this.ContinueReadingD[dIndex] = true;
        }
        else {
            this.ContinueReadingD[dIndex] = !this.ContinueReadingD[dIndex];
        }
    };
    CommunityUserPage.prototype.continuePostReading = function (commIndex) {
        if (!this.ContinuePostReading[commIndex]) {
            this.ContinuePostReading[commIndex] = true;
        }
        else {
            this.ContinuePostReading[commIndex] = !this.ContinuePostReading[commIndex];
        }
    };
    CommunityUserPage.prototype.continuePostReadingD = function (dIndex) {
        if (!this.ContinuePostReadingD[dIndex]) {
            this.ContinuePostReadingD[dIndex] = true;
        }
        else {
            this.ContinuePostReadingD[dIndex] = !this.ContinuePostReadingD[dIndex];
        }
    };
    CommunityUserPage.prototype.continueSecReading = function (commIndex) {
        if (!this.ContinueSecReading[commIndex]) {
            this.ContinueSecReading[commIndex] = true;
        }
        else {
            this.ContinueSecReading[commIndex] = !this.ContinueSecReading[commIndex];
        }
    };
    CommunityUserPage.prototype.continueSecReadingD = function (dIndex) {
        if (!this.ContinueSecReadingD[dIndex]) {
            this.ContinueSecReadingD[dIndex] = true;
        }
        else {
            this.ContinueSecReadingD[dIndex] = !this.ContinueSecReadingD[dIndex];
        }
    };
    CommunityUserPage.prototype.toggleReplies = function (commIndex) {
        if (!this.viewReplies[commIndex]) {
            this.viewReplies[commIndex] = true;
        }
        else {
            this.viewReplies[commIndex] = !this.viewReplies[commIndex];
        }
    };
    CommunityUserPage.prototype.togglePreviousReplies = function (commIndex) {
        if (!this.viewPostReplies[commIndex]) {
            this.viewPostReplies[commIndex] = true;
        }
        else {
            this.viewPostReplies[commIndex] = !this.viewPostReplies[commIndex];
        }
    };
    CommunityUserPage.prototype.toggleSecondReplies = function (postRplyIndex) {
        if (!this.viewSecondReplies[postRplyIndex]) {
            this.viewSecondReplies[postRplyIndex] = true;
        }
        else {
            this.viewSecondReplies[postRplyIndex] = !this.viewSecondReplies[postRplyIndex];
        }
    };
    CommunityUserPage.prototype.onKeydown = function (event) {
        event.preventDefault();
    };
    return CommunityUserPage;
}());
CommunityUserPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-community-user',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\community-user\community-user.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <app-header [title]="Discussion"></app-header>\n</ion-header>\n\n<ion-content> \n    <section class="w-1024">\n        <p class="text-light-gray mr-top mob-p-l-r t-c-mob" text-center padding-bottom padding-top>Join the discussion</p>\n        <div *ngIf="communityList.length > 0">\n            <ion-card class="card-bg" *ngFor="let community of communityList; let commIndex=index">\n                <ion-card-content class="community-user" *ngIf="community.discussion_list.length > 0">\n                    <ion-row>\n                        <h1 class="questn">\n                            Q: {{community.question_text}}\n                        </h1>\n                        <hr />\n                    </ion-row>\n                    <ion-row>\n                        <div col-lg-12 col-sm-12 col-12>\n                            <div class="commenter-img">\n    							<div *ngIf="community.discussion_list[0]?.profile_picture != \'\'; else smallelseBlockFirst">\n                                    <img class="round-image-small" src="assets/images/profile/{{community.discussion_list[0]?.profile_picture}}" alt="{{ community.discussion_list[0]?.profile_picture }}" />\n    							</div>\n    							<ng-template #smallelseBlockFirst>\n    								<div id="small_circle" [ngClass]="redColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(community.discussion_list[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="circle-large icon-circle-outline" data-profile_initials>{{ community.discussion_list[0]?.username.charAt(0).toUpperCase() }}</div>\n    							</ng-template>\n    						</div>\n                            <div class="posted-text">\n                                <p>\n                                    <strong>{{community.discussion_list[0]?.username}}</strong>\n                                </p>\n                            </div>\n                        </div>\n                    </ion-row>\n                    <ion-row padding-top padding-bottom class="comment-icons three-center">\n    					<div class="icon-center">\n                            <div col-12>\n                                <p class="community-text">{{community.discussion_list[0]?.answer.length > strLength && !ContinueReading[commIndex] ? community.discussion_list[0]?.answer.substr(0, strLength)+\'...\' : community.discussion_list[0]?.answer}} </p>\n                            </div>\n						\n							<div col-12 *ngIf="community.discussion_list[0]?.answer.length > strLength && !ContinueReading[commIndex]">\n                                <p class="continue-reading" (click)="continueReading(commIndex)">Continue Reading...</p>\n                            </div>\n    					</div>\n                        <div class="w-100 comment-section">\n                            <div class="three-icons-list">\n                                <div class="makes-feel-comment">\n                                    <p class="comment-texts">This comment makes me feel...</p>\n                                </div>\n                                <div class="inspire">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'INSPIRED\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'INSPIRED\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-inspired"></ion-icon>\n                                    <span>INSPIRED ({{community.discussion_list[0]?.post_rply.inspired}})</span>\n                                    </a>\n                                </div>\n                                <div class="understood">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'UNDERSTOOD\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'UNDERSTOOD\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-understood"></ion-icon>\n                                    <span>UNDERSTOOD ({{community.discussion_list[0]?.post_rply.understood}})</span>\n                                     </a>\n                                </div>\n                                <div class="grateful">\n                                    <a [ngClass]="{\'active\': community.discussion_list[0]?.status==\'GRATEFUL\'}" href="javascript:void(0)" (click)="setCommentStatus(\'GRATEFUL\', community.discussion_list[0]?.answer_id, community.id)">\n                                    <ion-icon name="im-grateful"></ion-icon>\n                                    <span>GRATEFUL ({{community.discussion_list[0]?.post_rply.grateful}})</span>\n                                    </a>\n                                </div>\n                            </div>\n                        </div>\n                    </ion-row>\n                    <ion-row class="review-content">\n                        <div class="d-block">\n                            <div col-12 >\n                                <h4 class="view-head">\n                                    <span class="curser"  (click)="toggleReplies(commIndex)">{{!!viewReplies[commIndex] ? \'Hide\' : \'View\'}} all answers & replies </span>                                    \n                                    <span class="not-click">( {{community.discussion_list.length -1 }} answers . {{community.discussion_list[0]?.post_rply.data.length}} replies )</span>\n                                    <small class="dot">.</small>\n                                </h4> \n                            </div>\n                            <div col-12 *ngIf="!!viewReplies[commIndex]">\n\n                                <div class="commenter-imges d-block main-block" *ngFor="let post_rply of community.discussion_list[0]?.post_rply.data;let postRplyIndex = index;">\n                                        <div class="add-bg d-block">\n                                            <div class="devide-text">\n                                            \n                                            <div class="reply-img" *ngIf="post_rply.profile_picture != \'\'; else smallelseBlockSec">\n                                                <img class="round-image-small" src="assets/images/profile/{{post_rply.profile_picture}}" alt="{{ post_rply.profile_picture }}" />\n                                            </div>\n            \n                                            <ng-template class="more-reply" #smallelseBlockSec>\n                                                <div id="small_circle" [ngClass]="redColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rply.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep mob-rep text-bg-img circle-large icon-circle-outline " data-profile_initials>{{ post_rply.username.charAt(0).toUpperCase() }}</div>\n                                            </ng-template>\n                                            <div class="add-bg-color light-gray">\n                                                <h4 class="view-name">{{post_rply.username}}</h4>\n                                                <p class="community-text">{{post_rply.comment.length > strLength && !ContinuePostReading[postRplyIndex] ? post_rply.comment.substr(0, strLength)+\'...\' : post_rply.comment}}    </p>\n                                            <p *ngIf="post_rply.comment.length > strLength && !ContinuePostReading[postRplyIndex]" (click)="continuePostReading(postRplyIndex)" class="continue-reading curser">Continue Reading...</p>\n                                            </div>\n                                       \n                                            <div class="revert" *ngIf="post_rply.replies && post_rply.replies.length">\n                                                <div *ngIf="!viewSecondReplies[postRplyIndex]" (click)="toggleSecondReplies(postRplyIndex)" class="curser mb-15">\n                                                    <small class="arrow-imgs">\n                                                         <img src="assets/images/replay_icon.png" width="50px" class="rep-icon">\n                                                     </small>\n                                                    <!-- <img src="assets/images/default-avatar.png" width="50px"> --> \n                                                    <div class="more-reply" *ngIf="post_rply.replies[0]?.profile_picture != \'\'; else smallelseBlock">\n                                                        <img class="round-image-small" src="assets/images/profile/{{post_rply.replies[0]?.profile_picture}}" alt="{{ post_rply.replies[0]?.profile_picture }}" />\n                                                    </div>\n                                                    <ng-template #smallelseBlock>\n                                                        <div class="more-reply">\n                                                            <div id="small_circle" [ngClass]="redColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rply.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ post_rply.replies[0]?.username.charAt(0).toUpperCase() }}</div>\n                                                        </div>\n                                                    </ng-template>\n                                                    <span>{{post_rply.replies[0]?.username}} replied. {{post_rply.replies.length}} more replies.</span>\n                                                </div>\n                                                <div *ngIf="!!viewSecondReplies[postRplyIndex]" class="second-reply">\n                                                    <div class="devide-text">\n                                                    <ion-row class="commenter-imges" *ngFor="let second_rply of post_rply.replies;let secRplyIndex = index;">\n                                                            <div class="add-bg">\n                                                                <div class="reply-img" *ngIf="second_rply.profile_picture != \'\'; else smallelseBlockThird">\n                                                                    <small class="arrow-imgs">\n                                                                        <img src="assets/images/replay_icon.png" *ngIf="secRplyIndex==0" width="30px" class="rep-icon">\n                                                                    </small>\n                                                                    <small class="arrow-imgs">\n                                                                    <img src="assets/images/replay_icon.png" *ngIf="secRplyIndex==0" width="30px" class="rep-icon">\n                                                                </small>\n                                                                    <img class="round-image-small" src="assets/images/profile/{{second_rply.profile_picture}}" alt="{{ second_rply.profile_picture }}" width="70px" height="70px"/>\n                                                                </div>\n                                                                <ng-template #smallelseBlockThird>\n                                                                    <div class="more-reply">\n                                                                        <small class="arrow-imgs">\n                                                                            <img src="assets/images/replay_icon.png" *ngIf="secRplyIndex==0" width="30px" class="rep-icon"> \n                                                                        </small>\n                                                                        <div id="small_circle" [ngClass]="redColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(second_rply?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ second_rply.username.charAt(0).toUpperCase() }}</div>\n                                                                    </div>\n                                                                </ng-template>\n                                                                <div class="add-bg-color">\n                                                                    <h4 class="view-name">{{second_rply.username}}</h4>\n                                                                    <p class="community-text">{{second_rply.comment.length > strSecLength && !ContinueSecReading[secRplyIndex] ? second_rply.comment.substr(0, strSecLength)+\'...\' : second_rply.comment}}    </p>\n                                                                    <p *ngIf="second_rply.comment && second_rply.comment.length > strSecLength && !ContinueSecReading[secRplyIndex]" class="continue-reading curser" (click)="continueSecReading(secRplyIndex)">Continue Reading...</p>\n                                                                   <div class="border"></div>\n                                                                </div>\n                                                            </div>\n                                                    </ion-row>\n                                                </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                            <div class="inner-reply">\n                                                <div class="textarea-div d-block">\n                                                    <div class="area-img">\n                                                        <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFour">\n                                                            <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                                        </div>\n                                                        <ng-template #smallelseBlockFour>\n                                                            <div class="text-bg-img bg-primary circle-large icon-circle-outline"> \n                                                                <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                                            </div>\n                                                        </ng-template>\n                                                    </div>\n                                                    <div class="area">\n                                                        <textarea (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, community.discussion_list[0].answer_id, post_rply.comment_id, $event.target.value,commIndex, postRplyIndex)" name="comment" text-center padding placeholder="Write a reply" class="text-input"></textarea>\n                                                        <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceSecComment[postRplyIndex]">Please enter valid text.</span>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        \n                                    <!-- </div> -->\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div *ngIf="community.discussion_list[0]?.post_rply.data.length == 0">\n                            <div col-12><h4>No replies yet</h4></div></div> -->\n                    </ion-row>\n                    <ion-row padding-bottom >\n                        <div class="textarea-div d-block">\n                            <div class="area-img">\n                                 <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFive">\n                                    <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                </div>\n                                <ng-template #smallelseBlockFive>\n                                    <div class="text-bg-img bg-primary circle-large icon-circle-outline">\n                                        <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                    </div>\n                                </ng-template>\n                            </div>\n                            <div class="area">\n                               <textarea  name="comment" (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, community.discussion_list[0]?.answer_id, null , $event.target.value, commIndex)" text-center padding placeholder="Write a reply"  class="text-input"></textarea>\n                             <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceComment[commIndex]">Please enter valid text.</span>\n                            </div>\n                        </div>\n    				</ion-row>\n                </ion-card-content>\n                <div *ngIf="community?.discussion_list.length > 0 && !!viewReplies[commIndex]" >\n                    <ion-card-content class="community-user" *ngFor="let discussion_list of community.discussion_list;let dIndex = index">\n                        <!-- <h1>\n                            Q: {{community.question_text}}\n                        </h1> -->\n                        <hr *ngIf="dIndex > 0" />\n                        <div *ngIf="dIndex > 0">\n                            <ion-row>\n                                <div col-lg-12 col-sm-12 col-12>\n                                    <div class="commenter-img">\n                                        <div *ngIf="discussion_list?.profile_picture != \'\'; else smallelseBlockFirstD">\n                                            <img class="round-image-small" src="assets/images/profile/{{discussion_list?.profile_picture}}" alt="{{ discussion_list?.profile_picture }}" />\n                                        </div>\n                                        <ng-template #smallelseBlockFirstD>\n                                            <div id="small_circle" [ngClass]="redColor.indexOf(discussion_list?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(discussion_list?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(discussion_list?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(discussion_list?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(discussion_list?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="circle-large icon-circle-outline" data-profile_initials>{{ discussion_list?.username.charAt(0).toUpperCase() }}</div>\n                                        </ng-template>\n                                    </div>\n                                    <div class="posted-text">\n                                        <p>\n                                            <strong>{{discussion_list?.username}}</strong>\n                                        </p>\n                                    </div>\n                                </div>\n                            </ion-row>\n                            <ion-row padding-top padding-bottom class="comment-icons three-center">\n                                <div class="icon-center">\n                                    <div col-12>\n                                        <p class="community-text">{{discussion_list?.answer.length > strLength && !ContinueReadingD[dIndex] ? discussion_list?.answer.substr(0, strLength)+\'...\' : discussion_list?.answer}} </p>\n                                    </div>\n                                    <div col-12 *ngIf="discussion_list?.answer.length > strLength && !ContinueReadingD[dIndex]">\n                                        <p class="continue-reading" (click)="continueReadingD(dIndex)">Continue Reading...</p>\n                                    </div>\n                                </div>\n                                <div class="w-100 comment-section">\n                                    <div col-lg-4 col-sm-12 col-12 class="">\n                                        <p class="comment-texts">This comment makes me feel...</p>\n                                    </div>\n                                    <div col-lg-2 col-sm-3 col-6 class="mr-tb">\n                                        <a [ngClass]="{\'active\': discussion_list?.status==\'INSPIRED\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'INSPIRED\', discussion_list?.answer_id, community.id)">\n                                            <ion-icon name="im-inspired"></ion-icon>\n                                            <span>INSPIRED ({{discussion_list?.post_rply.inspired}})</span>\n                                        </a>\n                                    </div>\n                                    <div col-lg-3 col-sm-4 col-6 class="center-text mr-tb">\n                                        <a [ngClass]="{\'active\': discussion_list?.status==\'UNDERSTOOD\'}" class="active" href="javascript:void(0)" (click)="setCommentStatus(\'UNDERSTOOD\', discussion_list?.answer_id, community.id)">\n                                            <ion-icon name="im-understood"></ion-icon>\n                                            <span>UNDERSTOOD ({{discussion_list?.post_rply.understood}})</span>\n                                        </a>\n                                    </div>\n                                    <div col-lg-3 col-sm-3 col-6 class="mr-tb">\n                                        <a [ngClass]="{\'active\': discussion_list?.status==\'GRATEFUL\'}" href="javascript:void(0)" (click)="setCommentStatus(\'GRATEFUL\', discussion_list?.answer_id, community.id)">\n                                            <ion-icon name="im-grateful"></ion-icon>\n                                            <span>GRATEFUL ({{discussion_list?.post_rply.grateful}})</span>\n                                        </a>\n                                    </div>\n                                </div>\n                            </ion-row>\n                            <ion-row class="review-content">\n                                <div class="d-block" *ngIf="discussion_list?.post_rply.data.length > 0">\n                                    <!-- <div col-12 >\n                                        <h4 class="view-head curser"  (click)="toggleReplies(commIndex)">\n                                            {{!!viewReplies[commIndex] ? \'Hide\' : \'View\'}} all answers & replies ( {{community.discussion_list.length -1 }} answers . {{discussion_list?.post_rply.data.length}} replies )\n                                        </h4>\n                                    </div> -->\n        \n                                    <!-- <div class="d-block curser" *ngIf="!!viewReplies[commIndex] && discussion_list?.post_rply.data.length > 3 && !viewPostReplies[commIndex]"><h4 class="view-head mb-15" (click)="togglePreviousReplies(commIndex)">View previous replies</h4></div> -->\n                                    <!-- <div *ngIf="!!viewReplies[commIndex]"> -->\n        \n                                        <div class="commenter-imges d-block main-block" *ngFor="let post_rplyD of discussion_list?.post_rply.data;let postRplyIndex = index;">\n        \n                                            <!-- <div class="d-block" *ngIf="!!viewPostReplies[commIndex] ? postRplyIndex < discussion_list?.post_rply.data.length : postRplyIndex < 3"> -->\n                                                <div class="add-bg d-block">\n                                                    <div class="devide-text">\n                                                    \n                                                    <div class="reply-img" *ngIf="post_rplyD.profile_picture != \'\'; else smallelseBlockSecD">\n                                                        <img class="round-image-small" src="assets/images/profile/{{post_rplyD?.profile_picture}}" alt="{{ post_rplyD?.profile_picture }}" />\n                                                    </div>\n                    \n                                                    <ng-template class="more-reply" #smallelseBlockSecD>\n                                                        <div id="small_circle" [ngClass]="redColor.indexOf(post_rplyD.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rplyD.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rplyD.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rplyD.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rplyD.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline " data-profile_initials>{{ post_rplyD.username.charAt(0).toUpperCase() }}</div>\n                                                    </ng-template>\n                                                    <div class="add-bg-color light-gray">\n                                                        <h4 class="view-name">{{post_rplyD.username}}</h4>\n                                                        <p class="community-text">{{post_rplyD.comment.length > strLength && !ContinuePostReadingD[postRplyIndex] ? post_rplyD.comment.substr(0, strLength)+\'...\' : post_rplyD.comment}}    </p>\n                                                    <p *ngIf="post_rplyD.comment.length > strLength && !ContinuePostReadingD[postRplyIndex]" (click)="continuePostReadingD(postRplyIndex)" class="continue-reading curser">Continue Reading...</p>\n                                                    </div>\n                                            \n                                                    <div class="revert" *ngIf="post_rplyD.replies.length">\n                                                        <div *ngIf="!viewSecondReplies[postRplyIndex]" (click)="toggleSecondReplies(postRplyIndex)" class="curser mb-15">\n                                                            <small class="arrow-imgs">\n                                                            <img src="assets/images/replay_icon.png" width="50px" class="rep-icon">\n                                                            </small>\n                                                            <!-- <img src="assets/images/default-avatar.png" width="50px"> --> \n                                                            <div class="more-reply" *ngIf="post_rplyD.replies[0]?.profile_picture != \'\'; else smallelseBlockD">\n                                                                <img class="round-image-small" src="assets/images/profile/{{post_rplyD.replies[0]?.profile_picture}}" alt="{{ post_rplyD.replies[0]?.profile_picture }}" />\n                                                            </div>\n                                                            <ng-template #smallelseBlockD>\n                                                                <div class="more-reply">\n                                                                    <div id="small_circle" [ngClass]="redColor.indexOf(post_rplyD.replies[0]?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(post_rplyD.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(post_rplyD.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(post_rplyD.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(post_rplyD.replies[0]?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ post_rplyD.replies[0]?.username.charAt(0).toUpperCase() }}</div>\n                                                                </div>\n                                                            </ng-template>\n                                                            <span>{{post_rplyD.replies[0]?.username}} replied. {{post_rplyD.replies.length}} more replies.</span>\n                                                        </div>\n                                                        <div *ngIf="!!viewSecondReplies[postRplyIndex]" class="second-reply">\n                                                            <div class="devide-text">\n                                                            <ion-row class="commenter-imges" *ngFor="let second_rplyD of post_rplyD.replies;let secRplyIndex = index;">\n                                                                    <div class="add-bg">\n                                                                        <div class="reply-img" *ngIf="second_rplyD.profile_picture != \'\'; else smallelseBlockThirdD">\n                                                                            <small class="arrow-imgs">\n                                                                            <img src="assets/images/replay_icon.png" *ngIf="secRplyIndex == 0" width="30px" class="rep-icon">\n                                                                            </small>\n                                                                            <img class="round-image-small" src="assets/images/profile/{{second_rplyD.profile_picture}}" alt="{{ second_rplyD.profile_picture }}" width="70px" height="70px"/>\n                                                                        </div>\n                                                                        <ng-template #smallelseBlockThirdD>\n                                                                            <div class="more-reply">\n                                                                                <small class="arrow-imgs">\n                                                                                <img src="assets/images/replay_icon.png" width="30px" *ngIf="secRplyIndex==0" class="rep-icon">\n                                                                                </small> \n                                                                                <div id="small_circle" [ngClass]="redColor.indexOf(second_rplyD?.username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(second_rplyD?.username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(second_rplyD?.username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(second_rplyD?.username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(second_rplyD?.username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="more-rep text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ second_rplyD.username.charAt(0).toUpperCase() }}</div>\n                                                                            </div>\n                                                                        </ng-template>\n                                                                        <div class="add-bg-color">\n                                                                            <h4 class="view-name">{{second_rplyD.username}}</h4>\n                                                                            <p class="community-text">{{second_rplyD.comment.length > strSecLength && !ContinueSecReadingD[secRplyIndex] ? second_rplyD.comment.substr(0, strSecLength)+\'...\' : second_rplyD.comment}}    </p>\n                                                                            <p *ngIf="second_rplyD.comment.length > strSecLength && !ContinueSecReadingD[secRplyIndex]" class="continue-reading curser" (click)="continueSecReadingD(secRplyIndex)">Continue Reading...</p>\n                                                                            <div class="border"></div>\n                                                                        </div>\n                                                                    </div>\n                                                            </ion-row>\n                                                        </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                    <div class="inner-reply">\n                                                        <div class="textarea-div d-block">\n                                                            <div class="area-img">\n                                                                <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFourD">\n                                                                    <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                                                </div>\n                                                                <ng-template #smallelseBlockFourD>\n                                                                    <div class="text-bg-img bg-primary circle-large icon-circle-outline"> \n                                                                        <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                                                    </div>\n                                                                </ng-template>\n                                                            </div>\n                                                            <div class="area">\n                                                                <textarea (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, discussion_list.answer_id, post_rplyD.comment_id, $event.target.value,commIndex, postRplyIndex)" name="comment" text-center padding placeholder="Write a reply" class="text-input"></textarea>\n                                                                <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceSecComment[postRplyIndex]">Please enter valid text.</span>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                \n                                            <!-- </div> -->\n                                        </div>\n                                    <!-- </div> -->\n                                </div>\n                                <div *ngIf="discussion_list?.post_rply.data.length == 0">\n                                    <div col-12><h4>No replies yet</h4></div></div>\n                            </ion-row>\n                            <ion-row padding-bottom >\n                                <div class="textarea-div d-block">\n                                    <div class="area-img">\n                                        <div class="text-bg-img circle-large icon-circle-outline" *ngIf="profilePicture != \'\'; else smallelseBlockFiveD">\n                                            <img class="round-image-small" src="assets/images/profile/{{profilePicture}}" alt="{{ profilePicture }}" width="70px" height="70px"/>\n                                        </div>\n                                        <ng-template #smallelseBlockFiveD>\n                                            <div class="text-bg-img bg-primary circle-large icon-circle-outline">\n                                                <div id="small_circle" [ngClass]="redColor.indexOf(username.charAt(0).toUpperCase())>-1 ? \'avatar-red\' : orangeColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-orange\' : yellowColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-yellow\' : greenColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-green\':blueColor.indexOf(username.charAt(0).toUpperCase())>-1?\'avatar-sky-blue\' :\'avatar-purple\'" class="text-bg-img circle-large icon-circle-outline" data-profile_initials>{{ username.charAt(0).toUpperCase() }}</div>\n                                            </div>\n                                        </ng-template>\n                                    </div>\n                                    <div class="area">\n                                    <textarea  name="comment" (keydown.enter)="onKeydown($event)" (keyup.enter)="doReply(community.id, discussion_list?.answer_id, null , $event.target.value, dIndex)" text-center padding placeholder="Write a reply"  class="text-input"></textarea>\n                                    <span class="error m-0-auto" col-12 ion-text color="danger" *ngIf="isWhitespaceComment[dIndex]">Please enter valid text.</span>\n                                    </div>\n                                </div>\n                            </ion-row>\n                        </div>\n                    </ion-card-content>\n                </div>\n            </ion-card>\n\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n            <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n        </div>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\community-user\community-user.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_community_service__["a" /* CommunityServiceProvider */]])
], CommunityUserPage);

//# sourceMappingURL=community-user.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FeedbackPage = (function () {
    function FeedbackPage(classService, loadCtrl, navCtrl, helper, formBuilder) {
        this.classService = classService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.formBuilder = formBuilder;
        this.answer = [];
        this.model = {};
        this.error = {};
        this.feedbackForm = this.formBuilder.group({
            feedback_answers: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            question_id: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            courses_id: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
        });
        this.getFeedback();
    }
    FeedbackPage.prototype.ionViewCanEnter = function () {
        this.helper.authenticated().then(function (response) {
        }, function (err) {
        });
    };
    FeedbackPage.prototype.getFeedback = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.classService.feedback().then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.questionList = _this.result.data;
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    FeedbackPage.prototype.save = function () {
        var _this = this;
        var data = { feedback_answers: [], question_id: [], course_id: [] }, feedback_answers = document.querySelectorAll('textarea[name^=feedback_answers]'), question_ids = document.querySelectorAll('input[name^=question_id]');
        //course_ids = document.querySelectorAll('input[name^=course_id]');
        for (var i = 0, ans = feedback_answers, len = ans.length; i < len; i++) {
            data['feedback_answers'][i] = ans[i]['value'];
            data['question_id'][i] = question_ids[i]['value'];
            //data['course_id'][i] = course_ids[i]['value'];
        }
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.classService.save_feedback(data).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.helper.presentToast(_this.result.msg, _this.result.status);
                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                _this.error = {};
            }
            else {
                _this.error = _this.result.data;
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    return FeedbackPage;
}());
FeedbackPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-feedback',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\feedback\feedback.html"*/'<!--\n  Generated template for the FeedbackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n--> \n\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n<ion-content>\n    <!-- <section class="bg-pink">\n        <ion-row justify-content-center>\n            <ion-col>\n                <h1 class="static-headings">Feedback</h1>\n            </ion-col>\n        </ion-row>\n    </section> --> \n    <ion-row>\n        <div class="head">\n            <h1>Feedback</h1>\n        </div>\n    </ion-row>\n    <section class="w-750">\n        \n        <ion-row>\n            <ion-grid text-center col-lg-12 col-md-12 col-12>\n                <p class="gray-text pera">We would love to hear about your experiences with the Wakeful™ tool, so please let us know what you think at any point during the course by answering the questions below.</p>\n                <div *ngIf="questionList?.length > 0" padding-bottom margin-bottom>\n                    <form [formGroup]="feedbackForm" autocomplete="off" (ngSubmit)="save()">\n                        <ion-row *ngFor="let question of questionList; let index=index">\n                            <ion-col col-12 class="sub-heading">\n                                <strong>{{question.question}}</strong>\n                            </ion-col>\n                            <ion-col col-12>\n                                <textarea name="feedback_answers[{{index}}]" tabindex="{{index}}" text-center rows=10 padding placeholder="Your answer goes here" class="text-input"></textarea>\n                                <p class="error m-0-auto" col-12 ion-text color="danger">\n                                    {{ error[\'feedback_answers[\'+index+\']\'] }}</p>\n                                <input type="hidden" name="question_id[{{index}}]" value="{{question.question_id}}">\n                                <!-- <input type="hidden" name="course_id[{{index}}]" value="{{question.courses_id}}"> -->\n                            </ion-col>\n                        </ion-row>\n                        <div col-lg-6 col-md-6 col-12 class="m-0-auto">\n                            <button ion-button col-12 round color="primary">Submit</button>\n                        </div>\n                    </form>\n                </div>\n\n                <!-- <ion-row>\n                    <ion-col col-12 margin-bottom margin-top class="sub-heading">\n                        Have you found any aspects of the Wakeful tool to be less helpful? If so, how?\n                    </ion-col>\n                    <ion-col col-12>\n                        <ion-textarea tabindex="2" text-center rows=10 padding placeholder="Your answer goes here"></ion-textarea>\n                    </ion-col>\n                </ion-row>\n\n                <ion-row>\n                    <ion-col col-12 margin-bottom margin-top class="sub-heading">\n                        If you want to give any additional feedback about the Wakeful tool or its content, please share it with us in the box below.\n                    </ion-col>\n                    <ion-col col-12>\n                        <ion-textarea tabindex="3" text-center rows=10 padding placeholder="Your answer goes here"></ion-textarea>\n                    </ion-col>\n                </ion-row> -->\n            </ion-grid>\n        </ion-row>    \n    </section>\n    <static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\feedback\feedback.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"]])
], FeedbackPage);

//# sourceMappingURL=feedback.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helper__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(loadCtrl, navCtrl, menu, formBuilder, authService, helper, storage) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.helper = helper;
        this.storage = storage;
        this.profileMenPics = [
            { title: 'Man 1', src: 'men-1.png' },
            { title: 'Man 2', src: 'men-2.png' },
            { title: 'Man 3', src: 'men-3.png' },
            { title: 'Man 4', src: 'men-4.png' },
            { title: 'Man 5', src: 'men-5.png' },
            { title: 'Man 6', src: 'men-6.png' },
            { title: 'Man 7', src: 'men-7.png' },
            { title: 'Man 8', src: 'men-8.png' },
            { title: 'Man 9', src: 'men-9.png' },
            { title: 'Man 10', src: 'men-10.png' },
            { title: 'Man 11', src: 'men-11.png' },
            { title: 'Man 12', src: 'men-12.png' },
            { title: 'Man 13', src: 'men-13.png' },
            { title: 'Man 14', src: 'men-14.png' },
            { title: 'Man 15', src: 'men-15.png' },
            { title: 'Man 16', src: 'men-16.png' },
            { title: 'Man 17', src: 'men-17.png' },
            { title: 'Man 18', src: 'men-18.png' },
            { title: 'Man 19', src: 'men-19.png' },
            { title: 'Man 20', src: 'men-20.png' },
        ];
        this.profileWomenPics = [
            { title: 'Woman 1', src: 'women-1.png' },
            { title: 'Woman 2', src: 'women-2.png' },
            { title: 'Woman 3', src: 'women-3.png' },
            { title: 'Woman 4', src: 'women-4.png' },
            { title: 'Woman 5', src: 'women-5.png' },
            { title: 'Woman 6', src: 'women-6.png' },
            { title: 'Woman 7', src: 'women-7.png' },
            { title: 'Woman 8', src: 'women-8.png' },
            { title: 'Woman 9', src: 'women-9.png' },
            { title: 'Woman 10', src: 'women-10.png' },
            { title: 'Woman 11', src: 'women-11.png' },
            { title: 'Woman 12', src: 'women-12.png' },
            { title: 'Woman 13', src: 'women-13.png' },
            { title: 'Woman 14', src: 'women-14.png' },
            { title: 'Woman 15', src: 'women-15.png' },
            { title: 'Woman 16', src: 'women-16.png' },
            { title: 'Woman 17', src: 'women-17.png' },
            { title: 'Woman 18', src: 'women-18.png' },
            { title: 'Woman 19', src: 'women-19.png' },
            { title: 'Woman 20', src: 'women-20.png' },
        ];
        this.is_unique_email = true;
        this.is_unique_username = true;
        this.is_current_password = true;
        this.is_unique_email_msg = '';
        this.is_unique_username_msg = '';
        this.is_current_password_msg = '';
        this.is_previous_password = true;
        this.overlayHidden = false;
        this.is_previous_password_msg = '';
        this.allowed_symbol = "$@!%*?&";
        this.isNotAllowedSymbol = false;
        this.user_detail = {
            // first_name: '',
            // last_name: '',
            unique_id: '',
            email: '',
            username: '',
            profile_picture: '',
        };
        this.menu.enable(true);
        this.getUserProfile();
        this.profileForm = this.formBuilder.group({
            // unique_id: [
            // 	this.user_detail['unique_id']
            // ],
            // first_name: [
            // 	this.user_detail['first_name'],
            // 	Validators.compose([Validators.pattern(/^\s*[a-z]+\s*$/i), Validators.required]),
            // ],
            // last_name: [
            // 	this.user_detail['last_name'],
            // 	Validators.compose([Validators.pattern(/^\s*[a-z]+\s*$/i), Validators.required]),
            // ],
            email: [
                this.user_detail['email'],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                ]),
            ],
            username: [
                this.user_detail['username'],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^\s*[a-z0-9\@\.\$\#\_]+\s*$/i), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]),
            ],
            current_password: [''],
            password: [
                '',
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!%*?&])[A-Za-z\d$@!%*?&]{8,}/),
                ]),
            ],
            confirm_password: [''],
            profile_picture: '',
            previous_image: this.user_detail['profile_picture'],
        }, {
            validator: this.matchingPasswords('password', 'confirm_password'),
        });
    }
    // input is focused or not
    ProfilePage.prototype.isTouched = function (ctrl, flag) {
        this.profileForm.controls[ctrl]['hasFocus'] = flag;
    };
    ProfilePage.prototype.ionViewCanEnter = function () {
        this.helper.authenticated().then(function (response) {
            return true;
        }, function (err) {
            return false;
        });
    };
    // Password match validation
    ProfilePage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirm_password = group.controls[confirmPasswordKey];
            if (password.value !== confirm_password.value) {
                return {
                    mismatchedPasswords: true,
                };
            }
        };
    };
    // Check Email is exits or not
    ProfilePage.prototype.isCurrentPassword = function (password) {
        var _this = this;
        if (this.profileForm.controls['password'].valid && password != '') {
            this.authService.isCurrentPassword({ password: encodeURIComponent(password) }).then(function (result) {
                _this.data = result;
                if (_this.data.status == 'success') {
                    _this.is_current_password_msg = 'Incorrect current password.';
                    _this.is_current_password = false;
                }
                else {
                    _this.is_current_password_msg = '';
                    _this.is_current_password = true;
                }
            }, function (err) {
                console.log(err);
            });
        }
        else {
            this.is_current_password = true;
        }
    };
    // Check Previous Password
    ProfilePage.prototype.isPreviousPassword = function (password) {
        var _this = this;
        if (this.profileForm.controls['password'].valid && password !== '') {
            this.authService.isPreviousPassword({ password: encodeURIComponent(password) }).then(function (result) {
                _this.data = result;
                if (_this.data.status === 'success') {
                    _this.is_previous_password_msg = 'Your password must be different from the previous 6 passwords.';
                    _this.is_previous_password = false;
                }
                else {
                    _this.is_previous_password_msg = '';
                    _this.is_previous_password = true;
                }
            }, function (err) {
                console.log(err);
            });
        }
        else {
            this.is_previous_password = true;
        }
    };
    // Check Email is exits or not
    ProfilePage.prototype.isEmailUnique = function (email) {
        var _this = this;
        if (this.profileForm.controls['email'].valid) {
            var email_info = {
                previous_email: this.user_detail['email'],
                current_email: email,
            };
            this.helper.isEmailUnique(email_info).then(function (response) {
                _this.is_unique_email_msg = 'This email address is already exits.';
                _this.is_unique_email = false;
            }, function (err) {
                _this.is_unique_email_msg = '';
                _this.is_unique_email = true;
            });
        }
    };
    // Check Username is exits or not
    ProfilePage.prototype.isUsernameUnique = function (username) {
        var _this = this;
        if (this.profileForm.controls['username'].valid) {
            var username_info = {
                previous_username: this.user_detail['username'],
                current_username: username,
            };
            this.helper.isUsernameUnique(username_info).then(function (response) {
                _this.is_unique_username_msg = 'This username is already exits.';
                _this.is_unique_username = false;
            }, function (err) {
                _this.is_unique_username_msg = '';
                _this.is_unique_username = true;
            });
        }
    };
    ProfilePage.prototype.getUserProfile = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.authService.get_profile().then(function (response) {
            _this.loading.dismiss();
            _this.data = response;
            if (_this.data.status == 'success') {
                _this.user_detail = _this.data.data;
                _this.getColor();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ProfilePage.prototype.updateProfile = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        var form = JSON.parse(JSON.stringify(this.profileForm.value));
        if (this.profileForm.valid && !this.notAllowedSymbol(this.profileForm.value['password'])) {
            form['previous_username'] = this.user_detail['username'];
            form['previous_email'] = this.user_detail['email'];
            form['password'] = encodeURIComponent(this.profileForm.value['password']);
            form['confirm_password'] = encodeURIComponent(this.profileForm.value['confirm_password']);
            form['current_password'] = encodeURIComponent(this.profileForm.value['current_password']);
            this.authService.update_profile(form).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status == 'success') {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                    _this.getUserProfile();
                }
                else {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                }
            }, function (err) {
                var error = err.json();
                if (error.status == 'error') {
                    _this.error_message = error.data;
                }
                _this.helper.presentToast(error.msg, error.status);
            });
        }
        else {
            this.loading.dismiss();
        }
    };
    ProfilePage.prototype.getColor = function () {
        var color_arr = ['bg-primary', 'bg-pink', 'bg-blue', 'bg-yellow', 'bg-secondary', 'bg-danger'], len = color_arr.length, name = this.user_detail['username'].toUpperCase(), //this.user_detail['first_name'].toUpperCase(),
        code = name.charCodeAt(0), index = (code - 65) % len, color = color_arr[index];
        var allElems = document.querySelectorAll('[data-profile_initials]');
        for (var i = 0, len_1 = allElems.length; i < len_1; i++) {
            allElems[i].setAttribute('class', color + ' circle-large icon-circle-outline');
        }
    };
    ProfilePage.prototype.logout = function () {
        this.helper.logout().then(function (response) { }, function (err) { });
    };
    ProfilePage.prototype.removeAvatar = function () {
        var _this = this;
        this.user_detail['profile_picture'] = '';
        this.profile_picture = '';
        this.profileForm.get('profile_picture').setValue('');
        setTimeout(function () {
            _this.getColor();
        }, 200);
    };
    ProfilePage.prototype.hideOverlay = function () {
        this.overlayHidden = !this.overlayHidden;
    };
    ProfilePage.prototype.selectImage = function (imageName, imageTitle) {
        this.profileForm.get('profile_picture').setValue(imageName);
        this.profile_picture = imageName;
        this.overlayHidden = !this.overlayHidden;
    };
    ProfilePage.prototype.notAllowedSymbol = function (password) {
        var flag = (/[\\" "#'()+,-./:;<=>[\]^_`{|}~]/g.test(password));
        if (flag) {
            var error = "Allowed special characters are " + this.allowed_symbol + " only.";
            this.helper.presentToast(error, 'error');
        }
        this.isNotAllowedSymbol = flag;
        return flag;
    };
    return ProfilePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('fileInput'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], ProfilePage.prototype, "fileInput", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageContainer'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], ProfilePage.prototype, "imageContainer", void 0);
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-profile',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\profile\profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<back-static-header></back-static-header>\n</ion-header>\n<ion-content>\n	<section class="w-1024" padding>\n		<div col-lg-5 col-12 class="m-0-auto">\n\n			<div text-center>\n				<div *ngIf="user_detail[\'profile_picture\'] != \'\'; else elseBlock">\n					<img class="round-image-big" src="assets/images/profile/{{user_detail[\'profile_picture\']}}" alt="{{ user_detail[\'fullname\'] }}" />\n				</div>\n\n				<ng-template #elseBlock>\n					<div id="large_circle" data-profile_initials>{{ user_detail[\'username\'].charAt(0).toUpperCase() }}</div>\n				</ng-template>\n\n				<h1 class="gray-text">\n					<strong>{{ user_detail[\'fullname\'] }}</strong>\n				</h1>\n			</div>\n			<form [formGroup]="profileForm" (ngSubmit)="updateProfile()" autocomplete="off">\n				<ion-row margin-top margin-bottom padding-top>\n					<h2 col-12 text-center class="gray-text">\n						<strong>Personal details</strong>\n					</h2>\n				</ion-row>\n				<ion-row>\n					<ion-label stacked>EMAIL</ion-label>\n					<ion-item>\n						<ion-input value="{{ user_detail[\'email\'] }}" (focus)="isTouched(\'email\',true)" (focusout)="isTouched(\'email\',false)"\n						 formControlName="email" id="email" type="text" (blur)="isEmailUnique($event.target.value)" [class.invalid]="!profileForm.controls.email.valid && (profileForm.controls.email.dirty)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="profileForm.controls[\'email\'].touched && profileForm.controls[\'email\'].valid && is_unique_email"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'email\'].hasError(\'required\') && profileForm.controls[\'email\'].touched && !profileForm.controls[\'email\'].hasFocus">Email\n						field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'email\'].hasError(\'pattern\') && profileForm.controls[\'email\'].touched && !profileForm.controls[\'email\'].hasFocus">Please\n						enter a valid email.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_unique_email && !profileForm.controls[\'email\'].hasError(\'pattern\')">{{is_unique_email_msg}}</p>\n				</ion-row>\n				<ion-row>\n					<ion-label col-12 stacked>CHOOSE AN AVATAR </ion-label>\n					<ion-avatar col-6>\n						<input type="hidden" value="{{user_detail[\'profile_picture\']}}" formControlName="profile_picture" name="profile_picture">\n						<div *ngIf="user_detail[\'profile_picture\'] != \'\'; else smallelseBlock">\n\n							<img *ngIf="!profile_picture" class="round-image-small" src="assets/images/profile/{{user_detail[\'profile_picture\']}}" alt="{{ user_detail[\'username\'] }}" />\n							<img *ngIf="profile_picture" class="round-image-small" src="assets/images/profile/{{profile_picture}}" alt="{{ user_detail[\'username\'] }}" />\n							<a (click)="removeAvatar()" margin-top href="javascript:void(0);">Remove avatar</a>\n						</div>\n\n						<ng-template #smallelseBlock>\n							<img *ngIf="profile_picture" class="round-image-small" src="assets/images/profile/{{profile_picture}}" alt="{{ user_detail[\'username\'] }}" />\n							<div *ngIf="!profile_picture" id="small_circle" data-profile_initials>{{ user_detail[\'username\'].charAt(0).toUpperCase() }}</div>\n						</ng-template>\n					</ion-avatar>\n					<label col-6 text-right class="lbl-upload-pic">\n						<!-- <input (change)="addImage($event)" type="file" hidden/> -->\n						<span (click)="hideOverlay()" class="browse-link">Change avatar</span>\n					</label>\n				</ion-row>\n\n				<ion-row text-center margin-top margin-bottom padding-top>\n					<h2 class="gray-text" col-12>\n						<strong>Sign in details</strong>\n					</h2>\n				</ion-row>\n				<ion-row>\n					<ion-label stacked>CHOOSE A USERNAME</ion-label>\n					<ion-item>\n						<ion-input value="{{ user_detail[\'username\'] }}" (focus)="isTouched(\'username\',true)" (focusout)="isTouched(\'username\',false)"\n						 formControlName="username" id="username" type="text" (blur)="isUsernameUnique($event.target.value)"\n						 [class.invalid]="!profileForm.controls.username.valid && (profileForm.controls.username.dirty)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="profileForm.controls[\'username\'].touched && profileForm.controls[\'username\'].valid && is_unique_username"></ion-icon>\n					</ion-item>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'username\'].hasError(\'required\') && profileForm.controls[\'username\'].touched && !profileForm.controls[\'username\'].hasFocus">Username\n						field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'username\'].hasError(\'pattern\') && profileForm.controls[\'username\'].touched && !profileForm.controls[\'username\'].hasFocus">Please\n						enter a valid username.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_unique_username">{{is_unique_username_msg}}</p>\n				</ion-row>\n\n				<ion-row>\n					<h5 col-12 class="gray-text" margin-top>Update password</h5>\n\n					<ion-label stacked>CURRENT PASSWORD</ion-label>\n					<ion-item>\n						<ion-input value="" (focus)="isTouched(\'current_password\',true)" (focusout)="isTouched(\'current_password\',false)"\n						 formControlName="current_password" id="current_password" type="password" (blur)="isCurrentPassword($event.target.value)"\n						 [class.invalid]="!profileForm.controls.current_password.valid && (profileForm.controls.current_password.dirty)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="profileForm.controls[\'current_password\'].touched && is_current_password && !isNotAllowedSymbol"></ion-icon>\n					</ion-item>\n					<span class="gray-text addtional-text">For security reason please enter your current password</span>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_current_password">{{is_current_password_msg}}</p>\n\n\n					<ion-label stacked>NEW PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="password" (focus)="isTouched(\'password\',true)" (focusout)="isTouched(\'password\',false)"\n						 id="password" type="password" [class.invalid]="!profileForm.controls.password.valid && (profileForm.controls.password.dirty)"\n						 (blur)="isPreviousPassword($event.target.value)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="profileForm.controls[\'password\'].touched && profileForm.controls[\'password\'].valid &&  !isNotAllowedSymbol"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'password\'].hasError(\'required\') && profileForm.controls[\'password\'].touched && !profileForm.controls[\'password\'].hasFocus">Password\n						field is required.</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'password\'].hasError(\'pattern\') && profileForm.controls[\'password\'].touched && !profileForm.controls[\'password\'].hasFocus">The\n						password must contain minimum 8 characters, at least 1 uppercase letter, 1 lowercase letter, 1 number and 1\n						special character ({{allowed_symbol}}).\n					</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="!is_previous_password">{{is_previous_password_msg}}</p>\n\n					<ion-label stacked>RE-TYPE NEW PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="confirm_password" (focus)="isTouched(\'confirm_password\',true)" (focusout)="isTouched(\'confirm_password\',false)"\n						 id="confirm_password" type="password" [class.invalid]="!profileForm.controls.confirm_password.valid && (profileForm.controls.confirm_password.dirty)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="profileForm.controls[\'confirm_password\'].touched && (!profileForm.hasError(\'mismatchedPasswords\') && profileForm.controls.confirm_password.valid && !isNotAllowedSymbol)"></ion-icon>\n					</ion-item>\n					<span class="gray-text addtional-text">To change your password enter the new password again</span>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.hasError(\'mismatchedPasswords\') && profileForm.controls.confirm_password.valid && (!profileForm.controls[\'confirm_password\'].hasFocus && !profileForm.controls[\'password\'].hasFocus)">\n						Please enter a valid confirm password.\n					</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="profileForm.controls[\'confirm_password\'].hasError(\'required\') && profileForm.controls[\'confirm_password\'].touched && !profileForm.controls[\'confirm_password\'].hasFocus">Confirm\n						password field is required.</p>\n				</ion-row>\n				<div padding-top text-center margin-top>\n					<button ion-button col-12 round color="primary" [disabled]="!(profileForm.valid && is_unique_email && is_unique_username && is_current_password && is_previous_password)">Update\n						details</button>\n				</div>\n				<div text-center>\n					<button ion-button col-12 round type="button" color="dark" (click)="logout()">sign out</button>\n				</div>\n\n			</form>\n		</div>\n\n	</section>\n</ion-content>\n<div class="my-overlay" padding [hidden]="!overlayHidden">\n	<div class="overlay-container">\n		<div class="overlay-bg">\n			<div class="cancel-btn">\n				<a href="javascript:void(0)" (click)="hideOverlay()"><ion-icon name="ios-close-outline"  class="icon icon-md ion-ios-close-outline item-icon" aria-label="close outline"></ion-icon></a>\n			</div>\n			<div>\n				<div class="text-heading">\n				<h1>Men</h1>\n				</div>\n				<div class="img-bg" *ngFor="let profileImage of profileMenPics;">\n					<span (click)="selectImage(profileImage.src)"><img src="assets/images/profile/{{profileImage.src}}" alt="{{profileImage.title}}"></span>\n				</div>\n				<div class="text-heading"><h1>Women</h1></div>\n				<div class="img-bg" *ngFor="let profileWmImage of profileWomenPics;">\n					<span (click)="selectImage(profileWmImage.src)"><img src="assets/images/profile/{{profileWmImage.src}}" alt="{{profileWmImage.title}}"></span>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\profile\profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResetPasswordPage = (function () {
    // Add in constructor
    // private socialService: AuthService,
    function ResetPasswordPage(loadCtrl, navCtrl, formBuilder, authService, navParams, helper, app) {
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.navParams = navParams;
        this.helper = helper;
        this.app = app;
        this.code = '';
        this.is_success = false;
        this.is_password_valid = {
            'is_length': false,
            'is_space': false,
            'is_capital': false,
            'is_small': false,
            'is_symbol': false,
            'is_number': false
        };
        this.allowed_symbol = "$@!%*?&";
        this.code = this.navParams.get('code');
        this.checkCode();
        this.resetPasswordForm = this.formBuilder.group({
            password: [
                '',
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    // Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                ]),
            ],
            confirm_password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            code: [this.code],
        }, {
            validator: this.matchingPasswords('password', 'confirm_password'),
        });
    }
    ResetPasswordPage.prototype.homePage = function () {
        this.Nav = this.app.getRootNavById('n4');
        this.Nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
        this.refreshPage();
    };
    // Password match validation
    ResetPasswordPage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirm_password = group.controls[confirmPasswordKey];
            if (password.value !== confirm_password.value) {
                return {
                    mismatchedPasswords: true,
                };
            }
        };
    };
    // Save User Data
    ResetPasswordPage.prototype.resetPassword = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        if (this.resetPasswordForm.valid && this.check_password_validaty()) {
            var input = JSON.parse(JSON.stringify(this.resetPasswordForm.value));
            input.code = this.code;
            input.password = encodeURIComponent(this.resetPasswordForm.value['password']);
            input.confirm_password = encodeURIComponent(this.resetPasswordForm.value['confirm_password']);
            this.authService.reset_password(input).then(function (result) {
                _this.data = result;
                _this.loading.dismiss();
                if (_this.data.status == 'success') {
                    _this.is_success = true;
                    // this.Nav = this.app.getRootNavById('n4');
                    // this.helper.presentToast(this.data.msg, this.data.status);
                    // this.Nav.setRoot(HomePage);
                    // this.refreshPage();
                }
                else {
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                }
            }, function (err) {
                var error = err.json();
                _this.loading.dismiss();
                _this.helper.presentToast(error.msg, error.status);
            });
        }
        else {
            this.loading.dismiss();
            this.validateAllFormFields(this.resetPasswordForm);
        }
    };
    ResetPasswordPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        //{1}
        Object.keys(formGroup.controls).forEach(function (field) {
            //{2}
            var control = formGroup.get(field); //{3}
            if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]) {
                //{4}
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]) {
                //{5}
                _this.validateAllFormFields(control); //{6}
            }
        });
    };
    ResetPasswordPage.prototype.checkCode = function () {
        var _this = this;
        if (this.code != undefined) {
            var data = { code: this.code };
            this.authService.reset_password_code(data).then(function (result) {
                _this.data = result;
                if (_this.data.status == 'error') {
                    _this.Nav = _this.app.getRootNavById('n4');
                    _this.helper.presentToast(_this.data.msg, _this.data.status);
                    _this.Nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                    _this.refreshPage();
                }
            }, function (err) {
                _this.Nav = _this.app.getRootNavById('n4');
                _this.helper.presentToast('Forgot passowrd link is invalid', 'error');
                _this.Nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                _this.refreshPage();
            });
        }
    };
    ResetPasswordPage.prototype.refreshPage = function () {
        var uri = window.location.toString();
        if (uri.indexOf('?') > 0) {
            var clean_uri = uri.substring(0, uri.indexOf('?'));
            window.history.replaceState({}, document.title, clean_uri);
        }
    };
    ResetPasswordPage.prototype.check_password_validaty = function () {
        var p = this.is_password_valid;
        if (p.is_not_symbol) {
            var error = 'Allowed special characters are ' + this.allowed_symbol + ' only.';
            this.helper.presentToast(error, 'error');
            this.is_password_valid.is_symbol = !1;
        }
        return (p.is_length && p.is_space && p.is_capital && p.is_small && p.is_symbol && p.is_number && !p.is_not_symbol);
    };
    ResetPasswordPage.prototype.checkPassword = function (password) {
        this.is_password_valid = {
            'is_length': !(password.length < 8),
            'is_space': !(/\s/g.test(password)),
            'is_capital': (/[A-Z]/g.test(password)),
            'is_small': (/[a-z]/g.test(password)),
            'is_symbol': (/[$@!%*?&]/g.test(password)),
            'is_not_symbol': (/[\\" "#'()+,-./:;<=>[\]^_`{|}~]/g.test(password)),
            'is_number': (/\d/g.test(password))
        };
        if (this.is_password_valid.is_not_symbol) {
            this.is_password_valid.is_symbol = !1;
        }
    };
    return ResetPasswordPage;
}());
ResetPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-reset-password',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\reset-password\reset-password.html"*/'<ion-content padding class="container password-contant">\n	<section class="w-1024">\n		<div col-12 text-center>\n			<img width="60px" src="assets/images/logo.png" />\n		</div>\n		<div col-lg-6 col-12 class="m-0-auto p-t-65" *ngIf="!is_success">\n			<h2 text-center padding-bottom class="f-26 primary-text">\n				<strong>Please enter your new password.</strong>\n			</h2>\n			<form [formGroup]="resetPasswordForm" (ngSubmit)="resetPassword()" autocomplete="off">\n				<ion-row>\n					<ion-label stacked>CHOOSE A PASSWORD</ion-label>\n					<ion-item>\n						<ion-input (keyup)="checkPassword($event.target.value)" formControlName="password" id="password" type="password" [class.invalid]="!resetPasswordForm.controls.password.valid && (resetPasswordForm.controls.password.dirty || submitAttempt)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="resetPasswordForm.controls[\'password\'].valid && is_password_valid.is_symbol"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="resetPasswordForm.controls[\'password\'].hasError(\'required\') && resetPasswordForm.controls[\'password\'].touched">Password field is required.</p>\n					<!-- <p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="resetPasswordForm.controls[\'password\'].hasError(\'pattern\') && resetPasswordForm.controls[\'password\'].touched">The password must be alphanumeric, 1 uppercase letter and 1 special character.</p> -->\n\n					<div class="password-hint gray-text">\n						<ul>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_length) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_length) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>8 characters or more</span>\n							</li>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_space) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_space) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>No spaces</span>\n							</li>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_capital) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_capital) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>1 uppercase letter</span>\n							</li>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_small) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_small) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>1 lowercase letter</span>\n							</li>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_symbol) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_symbol) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>1 symbol ({{allowed_symbol}})</span>\n							</li>\n							<li>\n								<ion-icon [color]="(is_password_valid.is_number) ? \'green\' : \'danger\'" [name]="(is_password_valid.is_number) ? \'checkmark\' : \'close\'"></ion-icon>\n								<span>1 number</span>\n							</li>\n						</ul>\n						<p class="p-b-20">For added security, easily guessed passwords (i.e. “password” or “football”), Keyboard patterns (i.e. “qwertyui of\n							”abcdefgh”) and sequential patterns (i.e. “abcd1234” or “12345678”) have been blocked </p>\n					</div>\n\n					<ion-label stacked>CONFIRM PASSWORD</ion-label>\n					<ion-item>\n						<ion-input formControlName="confirm_password" id="confirm_password" type="password" [class.invalid]="!resetPasswordForm.controls.confirm_password.valid && (resetPasswordForm.controls.confirm_password.dirty || submitAttempt)"></ion-input>\n						<ion-icon name="checkmark" item-right color="green" *ngIf="(!resetPasswordForm.hasError(\'mismatchedPasswords\') && resetPasswordForm.controls.confirm_password.valid && is_password_valid.is_symbol)"></ion-icon>\n					</ion-item>\n\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="resetPasswordForm.hasError(\'mismatchedPasswords\') && resetPasswordForm.controls.confirm_password.valid">\n						Please enter a valid confirm password.\n					</p>\n					<p class="error m-0-auto" col-12 ion-text color="danger" *ngIf="resetPasswordForm.controls[\'confirm_password\'].hasError(\'required\') && resetPasswordForm.controls[\'confirm_password\'].touched">Confirm password field is required.</p>\n\n				</ion-row>\n				<div class="m-t-10">\n					<div padding-top text-center>\n						<button col-12 col-lg-8 col-md-6 ion-button round class="submit-btn" [disabled]="!(is_password_valid.is_length && is_password_valid.is_space && is_password_valid.is_capital && is_password_valid.is_small && is_password_valid.is_number && is_password_valid.is_symbol && (!resetPasswordForm.hasError(\'mismatchedPasswords\') && resetPasswordForm.controls.confirm_password.valid))" color="primary">set new password</button>\n\n					</div>\n					<div col-12 text-center>\n						<a href="javascript:void(0)" (click)="homePage()">Back to sign in</a>\n					</div>\n				</div>\n			</form>\n		</div>\n		<div col-lg-6 col-12 class="m-0-auto p-t-65" *ngIf="is_success">\n			<h2 text-center margin-bottom padding-bottom class="f-26 primary-text">\n				<strong>Your password was successfully changed.</strong>\n			</h2>\n			<p text-center padding-bottm class="gray-text">You can now log in with your new password.</p>\n\n			<div padding-top>\n				<div padding-top text-center>\n					<a col-12 col-lg-8 col-md-6 ion-button round color="primary" (click)="homePage()">login</a>\n				</div>\n			</div>\n		</div>\n	</section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\reset-password\reset-password.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */]])
], ResetPasswordPage);

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_module__ = __webpack_require__(416);




if (__WEBPACK_IMPORTED_MODULE_2__config_constants__["a" /* CONSTANTS */].ENV.PROD === true) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_idle_core__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_moment__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_trim_directive__ = __webpack_require__(481);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_trim_directive___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_trim_directive__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_videogular2_buffering__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_videogular2_buffering___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_videogular2_buffering__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_videogular2_controls__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_videogular2_controls___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_videogular2_controls__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_videogular2_core__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_videogular2_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_videogular2_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_videogular2_overlay_play__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_videogular2_overlay_play___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_videogular2_overlay_play__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_app_header_app_header__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_back_static_header_back_static_header__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_breadcrumb_breadcrumb__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_modal_modal__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_progress_bar_progress_bar__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_static_footer_static_footer__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_about_about__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_acknowledgements_acknowledgements__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_audio_audio__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_class_class__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_class_schedule_class_schedule__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_community_discussion_community_discussion__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_community_user_community_user__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_community_community__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_dashboard_dashboard__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_feedback_feedback__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_forgot_password_forgot_password__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_general_general__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_homework_detail_homework_detail__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_homework_reading_detail_homework_reading_detail__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_homework_homework__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_intention_intention__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_meditation_timer_meditation_timer__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_numbered_general_numbered_general__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_overview_overview__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_privacy_policy_privacy_policy__ = __webpack_require__(521);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_question_question__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_reset_password_reset_password__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_review_detail_review_detail__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_review_review__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_signin_signin__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_signup_signup__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_tabs_tabs__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_terms_conditions_terms_conditions__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_testimonial_testimonial__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_topic_topic__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_video_intro_video_intro__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_video_video__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__providers_community_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__providers_data_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__providers_homework_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__providers_review_service__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__app_component__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_notification_notification__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







//import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup

// Social login
// import { AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule } from 'angular4-social-login';
































// Import Pages






 //import { ReviewDetailPage } from '../pages/review-detail/review-detail';









// Import Provider








var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_62__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_33__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_signin_signin__["a" /* SigninPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_49__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_49__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_class_class__["a" /* ClassPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_class_schedule_class_schedule__["a" /* ClassSchedulePage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_review_review__["a" /* ReviewPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_homework_homework__["a" /* HomeworkPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_meditation_timer_meditation_timer__["a" /* MeditationTimerPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_community_community__["a" /* CommunityPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_feedback_feedback__["a" /* FeedbackPage */],
            __WEBPACK_IMPORTED_MODULE_63__pages_notification_notification__["a" /* NotificationPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_overview_overview__["a" /* OverviewPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_acknowledgements_acknowledgements__["a" /* AcknowledgementsPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_privacy_policy_privacy_policy__["a" /* PrivacyPolicyPage */],
            __WEBPACK_IMPORTED_MODULE_50__pages_terms_conditions_terms_conditions__["a" /* TermsConditionsPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_general_general__["a" /* GeneralPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_audio_audio__["a" /* AudioPage */],
            __WEBPACK_IMPORTED_MODULE_53__pages_video_intro_video_intro__["a" /* VideoIntroPage */],
            __WEBPACK_IMPORTED_MODULE_54__pages_video_video__["a" /* VideoPage */],
            __WEBPACK_IMPORTED_MODULE_51__pages_testimonial_testimonial__["a" /* TestimonialPage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_numbered_general_numbered_general__["a" /* NumberedGeneralPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_topic_topic__["a" /* TopicPage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_question_question__["a" /* QuestionPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_intention_intention__["a" /* IntentionPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_community_discussion_community_discussion__["a" /* CommunityDiscussionPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_community_user_community_user__["a" /* CommunityUserPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_review_detail_review_detail__["a" /* ReviewDetailPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_homework_detail_homework_detail__["a" /* HomeworkDetailPage */],
            __WEBPACK_IMPORTED_MODULE_15__components_back_static_header_back_static_header__["a" /* BackStaticHeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_modal_modal__["a" /* ModalComponent */],
            __WEBPACK_IMPORTED_MODULE_14__components_app_header_app_header__["a" /* AppHeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__components_progress_bar_progress_bar__["a" /* ProgressBarComponent */],
            __WEBPACK_IMPORTED_MODULE_16__components_breadcrumb_breadcrumb__["a" /* BreadcrumbComponent */],
            __WEBPACK_IMPORTED_MODULE_19__components_static_footer_static_footer__["a" /* StaticFooterComponent */],
            __WEBPACK_IMPORTED_MODULE_35__pages_homework_reading_detail_homework_reading_detail__["a" /* HomeworkReadingDetailPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_7_angular2_moment__["MomentModule"],
            //NgIdleKeepaliveModule.forRoot(),
            __WEBPACK_IMPORTED_MODULE_6__ng_idle_core__["c" /* NgIdleModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_62__app_component__["a" /* MyApp */], {
                backButtonText: '',
            }, {
                links: [
                    // Root for dashboard
                    { component: __WEBPACK_IMPORTED_MODULE_29__pages_dashboard_dashboard__["a" /* DashboardPage */], name: 'DashboardPage', segment: '' },
                    // Root for classes
                    { component: __WEBPACK_IMPORTED_MODULE_24__pages_class_schedule_class_schedule__["a" /* ClassSchedulePage */], name: 'ClassSchedulePage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_23__pages_class_class__["a" /* ClassPage */], name: 'ClassPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_32__pages_general_general__["a" /* GeneralPage */], name: 'GeneralPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_22__pages_audio_audio__["a" /* AudioPage */], name: 'AudioPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_53__pages_video_intro_video_intro__["a" /* VideoIntroPage */], name: 'VideoIntroPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_54__pages_video_video__["a" /* VideoPage */], name: 'VideoPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_51__pages_testimonial_testimonial__["a" /* TestimonialPage */], name: 'TestimonialPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_39__pages_numbered_general_numbered_general__["a" /* NumberedGeneralPage */], name: 'NumberedGeneralPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_52__pages_topic_topic__["a" /* TopicPage */], name: 'TopicPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_43__pages_question_question__["a" /* QuestionPage */], name: 'QuestionPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_37__pages_intention_intention__["a" /* IntentionPage */], name: 'IntentionPage', segment: '' },
                    // Root for review
                    { component: __WEBPACK_IMPORTED_MODULE_46__pages_review_review__["a" /* ReviewPage */], name: 'ReviewPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_45__pages_review_detail_review_detail__["a" /* ReviewDetailPage */], name: 'ReviewDetailPage', segment: '' },
                    // Root for homework
                    { component: __WEBPACK_IMPORTED_MODULE_36__pages_homework_homework__["a" /* HomeworkPage */], name: 'HomeworkPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_38__pages_meditation_timer_meditation_timer__["a" /* MeditationTimerPage */], name: 'MeditationTimerPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_34__pages_homework_detail_homework_detail__["a" /* HomeworkDetailPage */], name: 'HomeworkDetailPage', segment: '' },
                    // Root for community
                    { component: __WEBPACK_IMPORTED_MODULE_27__pages_community_community__["a" /* CommunityPage */], name: 'CommunityPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_26__pages_community_user_community_user__["a" /* CommunityUserPage */], name: 'CommunityUserPage', segment: '' },
                    // Root for Other pages
                    { component: __WEBPACK_IMPORTED_MODULE_48__pages_signup_signup__["a" /* SignupPage */], name: 'SignupPage', segment: 'sign-up/:accesscode' },
                    { component: __WEBPACK_IMPORTED_MODULE_33__pages_home_home__["a" /* HomePage */], name: 'HomePage', segment: 'home' },
                    { component: __WEBPACK_IMPORTED_MODULE_47__pages_signin_signin__["a" /* SigninPage */], name: 'SigninPage', segment: 'sign-in/:course' },
                    { component: __WEBPACK_IMPORTED_MODULE_47__pages_signin_signin__["a" /* SigninPage */], name: 'SigninPage', segment: 'sign-in' },
                    { component: __WEBPACK_IMPORTED_MODULE_31__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */], name: 'ForgotPasswordPage', segment: 'forgot-password' },
                    //{ component: HomePage, name: 'HomePage', segment:''},
                    { component: __WEBPACK_IMPORTED_MODULE_20__pages_about_about__["a" /* AboutPage */], name: 'AboutPage', segment: 'about-us' },
                    { component: __WEBPACK_IMPORTED_MODULE_21__pages_acknowledgements_acknowledgements__["a" /* AcknowledgementsPage */], name: 'AcknowledgementsPage', segment: 'acknowledgements' },
                    { component: __WEBPACK_IMPORTED_MODULE_41__pages_privacy_policy_privacy_policy__["a" /* PrivacyPolicyPage */], name: 'PrivacyPolicyPage', segment: 'privacy-policy' },
                    { component: __WEBPACK_IMPORTED_MODULE_50__pages_terms_conditions_terms_conditions__["a" /* TermsConditionsPage */], name: 'TermsConditionsPage', segment: 'terms-conditions' },
                    { component: __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact__["a" /* ContactPage */], name: 'ContactPage', segment: 'contact' },
                    { component: __WEBPACK_IMPORTED_MODULE_63__pages_notification_notification__["a" /* NotificationPage */], name: 'NotificationPage', segment: 'notification' },
                    { component: __WEBPACK_IMPORTED_MODULE_25__pages_community_discussion_community_discussion__["a" /* CommunityDiscussionPage */], name: 'CommunityDiscussionPage', segment: '' },
                    { component: __WEBPACK_IMPORTED_MODULE_30__pages_feedback_feedback__["a" /* FeedbackPage */], name: 'FeedbackPage', segment: 'feedback' },
                    { component: __WEBPACK_IMPORTED_MODULE_40__pages_overview_overview__["a" /* OverviewPage */], name: 'OverviewPage', segment: 'faqs' },
                    { component: __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__["a" /* ProfilePage */], name: 'ProfilePage', segment: 'profile' },
                ],
            }),
            __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_32__pages_general_general__["a" /* GeneralPage */]),
            //IonicPageModule.forChild(AudioPage),
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            //AudioPageModule,
            //DashboardPageModule,
            //GeneralPageModule,
            /*QuestionPageModule,
            VideoIntroPageModule,
            VideoPageModule,
            TopicPageModule,
            TestimonialPageModule,
            IntentionPageModule,
            ReviewDetailPageModule,
            HomeworkDetailPageModule,
            CommonModule,
            ForgotPasswordPageModule,
            ResetPasswordPageModule,
            CommunityDiscussionPageModule,
            CommunityUserPageModule,*/
            //ComponentsModule,
            //ProgressBarComponent,
            // SocialLoginModule.initialize(config),
            __WEBPACK_IMPORTED_MODULE_12_videogular2_core__["VgCoreModule"],
            __WEBPACK_IMPORTED_MODULE_11_videogular2_controls__["VgControlsModule"],
            __WEBPACK_IMPORTED_MODULE_13_videogular2_overlay_play__["VgOverlayPlayModule"],
            __WEBPACK_IMPORTED_MODULE_10_videogular2_buffering__["VgBufferingModule"],
            __WEBPACK_IMPORTED_MODULE_9_ng2_trim_directive__["InputTrimModule"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_62__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_33__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_signin_signin__["a" /* SigninPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_49__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_class_class__["a" /* ClassPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_class_schedule_class_schedule__["a" /* ClassSchedulePage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_review_review__["a" /* ReviewPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_homework_homework__["a" /* HomeworkPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_meditation_timer_meditation_timer__["a" /* MeditationTimerPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_community_community__["a" /* CommunityPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_acknowledgements_acknowledgements__["a" /* AcknowledgementsPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_privacy_policy_privacy_policy__["a" /* PrivacyPolicyPage */],
            __WEBPACK_IMPORTED_MODULE_50__pages_terms_conditions_terms_conditions__["a" /* TermsConditionsPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_feedback_feedback__["a" /* FeedbackPage */],
            __WEBPACK_IMPORTED_MODULE_63__pages_notification_notification__["a" /* NotificationPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_overview_overview__["a" /* OverviewPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_general_general__["a" /* GeneralPage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_numbered_general_numbered_general__["a" /* NumberedGeneralPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_audio_audio__["a" /* AudioPage */],
            __WEBPACK_IMPORTED_MODULE_53__pages_video_intro_video_intro__["a" /* VideoIntroPage */],
            __WEBPACK_IMPORTED_MODULE_54__pages_video_video__["a" /* VideoPage */],
            __WEBPACK_IMPORTED_MODULE_51__pages_testimonial_testimonial__["a" /* TestimonialPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_topic_topic__["a" /* TopicPage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_question_question__["a" /* QuestionPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_intention_intention__["a" /* IntentionPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_community_discussion_community_discussion__["a" /* CommunityDiscussionPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_community_user_community_user__["a" /* CommunityUserPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_review_detail_review_detail__["a" /* ReviewDetailPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_homework_detail_homework_detail__["a" /* HomeworkDetailPage */],
            __WEBPACK_IMPORTED_MODULE_17__components_modal_modal__["a" /* ModalComponent */],
            __WEBPACK_IMPORTED_MODULE_14__components_app_header_app_header__["a" /* AppHeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__components_progress_bar_progress_bar__["a" /* ProgressBarComponent */],
            __WEBPACK_IMPORTED_MODULE_16__components_breadcrumb_breadcrumb__["a" /* BreadcrumbComponent */],
            __WEBPACK_IMPORTED_MODULE_19__components_static_footer_static_footer__["a" /* StaticFooterComponent */],
            __WEBPACK_IMPORTED_MODULE_35__pages_homework_reading_detail_homework_reading_detail__["a" /* HomeworkReadingDetailPage */],
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_55__providers_auth_service__["a" /* AuthServiceProvider */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["e" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_59__providers_helper__["a" /* Helper */],
            __WEBPACK_IMPORTED_MODULE_56__providers_class_service__["a" /* ClassServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_58__providers_data_service__["a" /* DataServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_61__providers_review_service__["a" /* ReviewServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_60__providers_homework_service__["a" /* HomeworkServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_57__providers_community_service__["a" /* CommunityServiceProvider */],
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_18__components_progress_bar_progress_bar__["a" /* ProgressBarComponent */]],
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signin_signin__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabs_tabs__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { CONSTANTS } from '../../config/constants';
var HomePage = HomePage_1 = (function () {
    function HomePage(navCtrl, storage, platform, classService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.platform = platform;
        this.classService = classService;
        this.title = '';
        this.bg_image = '';
        this.signInPage = __WEBPACK_IMPORTED_MODULE_4__signin_signin__["a" /* SigninPage */];
        this.signUpPage = __WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */];
        this.storage.get('token').then(function (token) {
            if (token) {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__tabs_tabs__["a" /* TabsPage */];
            }
            else {
                _this.rootPage = HomePage_1;
            }
        });
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (res.hasOwnProperty('main_page')) {
                _this.bg_image = data.main_page;
            }
        });
    };
    HomePage.prototype.signIn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signin_signin__["a" /* SigninPage */], { course: 'course' });
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slides'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Slides */])
], HomePage.prototype, "slides", void 0);
HomePage = HomePage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\home\home.html"*/'<ion-content>\n	<section class="section-0">\n		<div class="">\n			<ion-navbar>\n				<span text-center float-left>\n					<img class="center-logo" src=\'assets/images/full_logo.png\' alt="Wakeful" />\n				</span>\n				<a menuToggle end class="menu-btn">\n					<ion-icon name="im-menu"></ion-icon>\n				</a>\n			</ion-navbar>\n			<!-- <div col-lg-6 col-12 class="m-0-auto" text-center>\n				<h1 class="home-heading">Welcome to Wakeful</h1>\n				<p class="gray-text home-text">Learn to be wakeful and treat the moments of your life as if they really mattered</p>\n				<div text-center>\n					<button ion-button col-9 round class="submit-btn btn-bg-color" color="dark" (click)="signIn()" >SIGN IN</button>\n				</div>\n			</div> -->\n		</div>\n	</section>\n	<section class="section-1">\n		<div class="w-1024">			\n			<div col-lg-6 col-12 class="m-0-auto" text-center>\n				<h1 class="home-heading">Welcome to Wakeful</h1>\n				<p class="gray-text home-text">Learn to be wakeful and treat the moments of your life as if they really mattered</p>\n				<!-- <div text-center padding-top>\n					<button ion-button col-9 round type="button" color="primary" [navPush]="signUpPage">SIGN UP</button>\n				</div> -->\n				<div text-center>\n					<button ion-button col-9 round class="submit-btn btn-bg-color" color="dark" (click)="signIn()" >SIGN IN</button>\n				</div>\n			</div>\n		</div>\n	</section>\n	<section class="section-2">\n		<div class="w-1024">\n			<ion-row padding-top>\n				<div col-lg-6 col-md-6 col-12 class="left">\n					<h3>Mindfully developed to develop your mind...fully!</h3>\n					<ul>\n						<li>Created by mindfulness researchers, teachers and practitioners</li>\n						<li>Self-directed, interative content</li>\n						<li>Evidence-based exercises and teachings</li>\n						<li>Tailored content delivery for different populations and needs</li>\n					</ul>\n				</div>\n				<div col-lg-6 col-md-6 col-12 class="right">\n					<div class="right-mobile-img">\n						<img alt="section right mobile" src="assets/images/section_right_mobile.png">\n					</div>\n				</div>\n			</ion-row>\n		</div>\n	</section>\n	<section class="section-3">\n		<div class="w-1024 section-center">\n			<ion-row text-center justify-content-center>\n				<div col-lg-12 col-md-12 col-12>\n					<p>\n						<strong>“Waking up this morning, I smile. Twenty-four brand new hours are before me. I vow to live fully in each moment and\n							to look at all beings with eyes of compassion.” - Thich Nhat Hanh</strong>\n					</p>\n					<h2>Wanna Wake Up?</h2>\n					<h4 padding-bottom>Let’s set in motion a whole new way of living...wakefully.</h4>\n					<button ion-button round outline class="btn-outline-dark" [navPush]="signUpPage">let’s get started</button>\n				</div>\n			</ion-row>\n		</div>\n	</section>\n	<section class="section-4">\n		<div class="w-1024 section-center">\n			<ion-row text-center justify-content-center>\n				<div col-lg-12 col-md-12 col-12>\n					<h2>Don’t have time?</h2>\n					<h4 padding-bottom>\n						Don’t worry, you’re not alone. Nobody does. The good news is that\n						<i>Wakeful</i> can be easily integrated into your day and schedule, wherever you are, whenever you want.\n					</h4>\n					<button ion-button round outline class="btn-outline-dark" [navPush]="signUpPage">let’s make time</button>\n				</div>\n			</ion-row>\n		</div>\n	</section>\n	<section class="section-5">\n		<div class="w-1024 section-center">\n			<ion-row text-center justify-content-center>\n				<div col-lg-12 col-md-12 col-12>\n					<h2 class="c-white">Not the meditating type?</h2>\n					<h4 padding-bottom class="c-white">\n						We’re not saying it’s easy, but if you give this a try we bet you’ll come to agree that meditation is not what you think!\n					</h4>\n					<button ion-button round outline class="btn-outline-white" [navPush]="signUpPage">Let’s give it a try</button>\n				</div>\n			</ion-row>\n		</div>\n	</section>\n	<section class="section-6">\n		<div class="w-1024 section-center">\n			<ion-row text-center justify-content-center>\n				<div col-lg-8 col-md-9 col-12 class="slider-container" padding-top>\n					<ion-slides pager="true" #slides>\n						<ion-slide>\n							<img class="round-image" src="assets/images/mindful.png" />\n							<p class="slide-text text-center f-22">\n								<ion-icon style="color:#af96c1" class="icon-flipped" ios="ios-quote" md="md-quote" float-left></ion-icon>\n								What a gift it is to really notice and savor the moments of my life. Even the unpleasant ones. I can’t help but feel grateful\n								for everything.\n								<ion-icon style="color:#af96c1" ios="ios-quote" md="md-quote" float-right></ion-icon>\n							</p>\n\n							<p class="t-uper testi-name f-16" padding-bottom padding-top text-center>- MINDFUL2273</p>\n						</ion-slide>\n						<ion-slide>\n							<img class="round-image" src="assets/images/john.png" />\n							<p class="slide-text text-center f-22">\n								<ion-icon style="color:#af96c1" class="icon-flipped" ios="ios-quote" md="md-quote" float-left></ion-icon>\n								I know being mindful is more than feeling relaxed, but when I pay attention to my life in this way, I slow down, and I can’t help feeling calmer and more relaxed as a result.\n								<ion-icon style="color:#af96c1" ios="ios-quote" md="md-quote" float-right></ion-icon>\n							</p>\n\n							<p class="t-uper testi-name f-16" padding-bottom padding-top text-center>- John Thompson</p>\n						</ion-slide>\n						<ion-slide>\n							<img class="round-image" src="assets/images/jennifer.png" />\n							<p class="slide-text text-center f-22">\n								<ion-icon style="color:#af96c1" class="icon-flipped" ios="ios-quote" md="md-quote" float-left></ion-icon>\n								I learn best by doing, and this class is all about that! Now I can’t not be mindful to my life!\n								<ion-icon style="color:#af96c1" ios="ios-quote" md="md-quote" float-right></ion-icon>\n							</p>\n\n							<p class="t-uper testi-name f-16" padding-bottom padding-top text-center>- jennifer harris</p>\n						</ion-slide>\n					</ion-slides>\n					<div class="swiper-button-prev" (click)="slides.slidePrev()"></div>\n					<div class="swiper-button-next" (click)="slides.slideNext()"></div>\n				</div>\n			</ion-row>\n		</div>\n	</section>\n	<static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\home\home.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_3__providers_class_service__["a" /* ClassServiceProvider */]])
], HomePage);

var HomePage_1;
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ModalComponent = (function () {
    function ModalComponent(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.bgColor = '#DC78AE !important';
        this.height = 'auto';
        this.body = params.get('body').replace(/(?:\r\n|\r|\n)/g, '<br />');
        if (params.get('bgColor')) {
            this.bgColor = params.get('bgColor');
        }
        if (params.get('type') && params.get('type') == 'topic') {
            this.height = '100%';
        }
        this.title = params.get('title');
    }
    ModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return ModalComponent;
}());
ModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modal',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\modal\modal.html"*/'<!-- Generated template for the ModalComponent component -->\n<ion-content>\n   \n    <section class="modal-content" [ngStyle]="{\'background\': bgColor, \'height\': height}" text-center>\n        <ion-buttons end>\n            <a href="javascript:void(0);" class="modal-close" (click)="dismiss()">\n                <ion-icon ios="ios-close" md="md-close"></ion-icon>\n            </a>\n        </ion-buttons>\n        <div class="main-model">\n            <div>\n                <h1>{{title}}</h1>\n            </div>\n            <h5>\n                <div [innerHTML]="body" class="f-26"></div>\n            </h5>\n        </div>\n    </section>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\modal\modal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], ModalComponent);

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 440:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 163,
	"./af.js": 163,
	"./ar": 164,
	"./ar-dz": 165,
	"./ar-dz.js": 165,
	"./ar-kw": 166,
	"./ar-kw.js": 166,
	"./ar-ly": 167,
	"./ar-ly.js": 167,
	"./ar-ma": 168,
	"./ar-ma.js": 168,
	"./ar-sa": 169,
	"./ar-sa.js": 169,
	"./ar-tn": 170,
	"./ar-tn.js": 170,
	"./ar.js": 164,
	"./az": 171,
	"./az.js": 171,
	"./be": 172,
	"./be.js": 172,
	"./bg": 173,
	"./bg.js": 173,
	"./bm": 174,
	"./bm.js": 174,
	"./bn": 175,
	"./bn.js": 175,
	"./bo": 176,
	"./bo.js": 176,
	"./br": 177,
	"./br.js": 177,
	"./bs": 178,
	"./bs.js": 178,
	"./ca": 179,
	"./ca.js": 179,
	"./cs": 180,
	"./cs.js": 180,
	"./cv": 181,
	"./cv.js": 181,
	"./cy": 182,
	"./cy.js": 182,
	"./da": 183,
	"./da.js": 183,
	"./de": 184,
	"./de-at": 185,
	"./de-at.js": 185,
	"./de-ch": 186,
	"./de-ch.js": 186,
	"./de.js": 184,
	"./dv": 187,
	"./dv.js": 187,
	"./el": 188,
	"./el.js": 188,
	"./en-au": 189,
	"./en-au.js": 189,
	"./en-ca": 190,
	"./en-ca.js": 190,
	"./en-gb": 191,
	"./en-gb.js": 191,
	"./en-ie": 192,
	"./en-ie.js": 192,
	"./en-il": 193,
	"./en-il.js": 193,
	"./en-nz": 194,
	"./en-nz.js": 194,
	"./eo": 195,
	"./eo.js": 195,
	"./es": 196,
	"./es-do": 197,
	"./es-do.js": 197,
	"./es-us": 198,
	"./es-us.js": 198,
	"./es.js": 196,
	"./et": 199,
	"./et.js": 199,
	"./eu": 200,
	"./eu.js": 200,
	"./fa": 201,
	"./fa.js": 201,
	"./fi": 202,
	"./fi.js": 202,
	"./fo": 203,
	"./fo.js": 203,
	"./fr": 204,
	"./fr-ca": 205,
	"./fr-ca.js": 205,
	"./fr-ch": 206,
	"./fr-ch.js": 206,
	"./fr.js": 204,
	"./fy": 207,
	"./fy.js": 207,
	"./gd": 208,
	"./gd.js": 208,
	"./gl": 209,
	"./gl.js": 209,
	"./gom-latn": 210,
	"./gom-latn.js": 210,
	"./gu": 211,
	"./gu.js": 211,
	"./he": 212,
	"./he.js": 212,
	"./hi": 213,
	"./hi.js": 213,
	"./hr": 214,
	"./hr.js": 214,
	"./hu": 215,
	"./hu.js": 215,
	"./hy-am": 216,
	"./hy-am.js": 216,
	"./id": 217,
	"./id.js": 217,
	"./is": 218,
	"./is.js": 218,
	"./it": 219,
	"./it.js": 219,
	"./ja": 220,
	"./ja.js": 220,
	"./jv": 221,
	"./jv.js": 221,
	"./ka": 222,
	"./ka.js": 222,
	"./kk": 223,
	"./kk.js": 223,
	"./km": 224,
	"./km.js": 224,
	"./kn": 225,
	"./kn.js": 225,
	"./ko": 226,
	"./ko.js": 226,
	"./ky": 227,
	"./ky.js": 227,
	"./lb": 228,
	"./lb.js": 228,
	"./lo": 229,
	"./lo.js": 229,
	"./lt": 230,
	"./lt.js": 230,
	"./lv": 231,
	"./lv.js": 231,
	"./me": 232,
	"./me.js": 232,
	"./mi": 233,
	"./mi.js": 233,
	"./mk": 234,
	"./mk.js": 234,
	"./ml": 235,
	"./ml.js": 235,
	"./mn": 236,
	"./mn.js": 236,
	"./mr": 237,
	"./mr.js": 237,
	"./ms": 238,
	"./ms-my": 239,
	"./ms-my.js": 239,
	"./ms.js": 238,
	"./mt": 240,
	"./mt.js": 240,
	"./my": 241,
	"./my.js": 241,
	"./nb": 242,
	"./nb.js": 242,
	"./ne": 243,
	"./ne.js": 243,
	"./nl": 244,
	"./nl-be": 245,
	"./nl-be.js": 245,
	"./nl.js": 244,
	"./nn": 246,
	"./nn.js": 246,
	"./pa-in": 247,
	"./pa-in.js": 247,
	"./pl": 248,
	"./pl.js": 248,
	"./pt": 249,
	"./pt-br": 250,
	"./pt-br.js": 250,
	"./pt.js": 249,
	"./ro": 251,
	"./ro.js": 251,
	"./ru": 252,
	"./ru.js": 252,
	"./sd": 253,
	"./sd.js": 253,
	"./se": 254,
	"./se.js": 254,
	"./si": 255,
	"./si.js": 255,
	"./sk": 256,
	"./sk.js": 256,
	"./sl": 257,
	"./sl.js": 257,
	"./sq": 258,
	"./sq.js": 258,
	"./sr": 259,
	"./sr-cyrl": 260,
	"./sr-cyrl.js": 260,
	"./sr.js": 259,
	"./ss": 261,
	"./ss.js": 261,
	"./sv": 262,
	"./sv.js": 262,
	"./sw": 263,
	"./sw.js": 263,
	"./ta": 264,
	"./ta.js": 264,
	"./te": 265,
	"./te.js": 265,
	"./tet": 266,
	"./tet.js": 266,
	"./tg": 267,
	"./tg.js": 267,
	"./th": 268,
	"./th.js": 268,
	"./tl-ph": 269,
	"./tl-ph.js": 269,
	"./tlh": 270,
	"./tlh.js": 270,
	"./tr": 271,
	"./tr.js": 271,
	"./tzl": 272,
	"./tzl.js": 272,
	"./tzm": 273,
	"./tzm-latn": 274,
	"./tzm-latn.js": 274,
	"./tzm.js": 273,
	"./ug-cn": 275,
	"./ug-cn.js": 275,
	"./uk": 276,
	"./uk.js": 276,
	"./ur": 277,
	"./ur.js": 277,
	"./uz": 278,
	"./uz-latn": 279,
	"./uz-latn.js": 279,
	"./uz.js": 278,
	"./vi": 280,
	"./vi.js": 280,
	"./x-pseudo": 281,
	"./x-pseudo.js": 281,
	"./yo": 282,
	"./yo.js": 282,
	"./zh-cn": 283,
	"./zh-cn.js": 283,
	"./zh-hk": 284,
	"./zh-hk.js": 284,
	"./zh-tw": 285,
	"./zh-tw.js": 285
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 440;

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Title } from '@angular/platform-browser/src/browser/title';
/**
 * Generated class for the AppHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var AppHeaderComponent = (function () {
    function AppHeaderComponent(params, storage, events) {
        var _this = this;
        this.storage = storage;
        this.events = events;
        this.isShownImage = true;
        this.notificationCount = 0;
        this.events.subscribe('user:notification', function () {
            // notification count subscribe
            _this.storage.get('notification_count').then(function (notificationCount) {
                _this.notificationCount = notificationCount;
            });
        });
    }
    return AppHeaderComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AppHeaderComponent.prototype, "title", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AppHeaderComponent.prototype, "isShownImage", void 0);
AppHeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-header',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\app-header\app-header.html"*/'<!-- Generated template for the AppHeaderComponent component -->\n<ion-navbar>\n    <span text-center float-left *ngIf="isShownImage">\n		<img class="center-logo" src=\'assets/images/logo.png\' alt="Wakeful" />\n	</span>\n    <ion-title text-center> {{title}} </ion-title>\n    <a menuToggle end class="menu-btn">\n        <ion-icon name="im-menu" ></ion-icon>\n        <span class="{{notificationCount > 0 ? \'red-dot\' : \'\'}}"></span>\n    </a>\n</ion-navbar>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\app-header\app-header.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], AppHeaderComponent);

//# sourceMappingURL=app-header.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackStaticHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the BackStaticHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BackStaticHeaderComponent = (function () {
    function BackStaticHeaderComponent(navCtrl, helper, storage, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.storage = storage;
        this.events = events;
        this.notificationCount = 0;
        this.APPTITLE = __WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* CONSTANTS */].APP_TITLE;
        this.events.subscribe('user:notification', function () {
            // notification count subscribe
            _this.storage.get('notification_count').then(function (notificationCount) {
                _this.notificationCount = notificationCount;
            });
        });
    }
    BackStaticHeaderComponent.prototype.goBack = function () {
        var _this = this;
        this.helper.authenticated(false).then(function (response) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_tabs_tabs__["a" /* TabsPage */], { selectIndex: 0 });
        }, function (err) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
        });
    };
    return BackStaticHeaderComponent;
}());
BackStaticHeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'back-static-header',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\back-static-header\back-static-header.html"*/'<ion-navbar>\n	<ion-buttons left>\n		<button ion-button icon-only (click)="goBack()">\n			<ion-icon name="arrow-back"></ion-icon>\n		</button>\n	</ion-buttons>\n	<div text-center>\n		<img src="assets/images/logo.png" class="center-logo" alt="{{APPTITLE}}" />\n	</div>\n	<a menuToggle end class="menu-btn">\n		<ion-icon name="im-menu"></ion-icon>\n		<span *ngIf="notificationCount > 0"  class="red-dot"></span>\n	</a>\n</ion-navbar>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\back-static-header\back-static-header.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */]])
], BackStaticHeaderComponent);

//# sourceMappingURL=back-static-header.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the BreadcrumbComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BreadcrumbComponent = (function () {
    function BreadcrumbComponent() {
        this.data = [];
    }
    return BreadcrumbComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('data'),
    __metadata("design:type", Object)
], BreadcrumbComponent.prototype, "data", void 0);
BreadcrumbComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'breadcrumb',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\breadcrumb\breadcrumb.html"*/'<!-- Generated template for the BreadcrumbComponent component -->\n<div margin style="border-bottom:1px solid #DCDCDC">\n    <ul>\n        <li *ngFor="let d of data">{{d}}</li>\n    </ul>\n</div>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\breadcrumb\breadcrumb.html"*/,
    }),
    __metadata("design:paramtypes", [])
], BreadcrumbComponent);

//# sourceMappingURL=breadcrumb.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ProgressBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ProgressBarComponent = (function () {
    function ProgressBarComponent() {
        this.text = 'Hello World';
    }
    return ProgressBarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('progress'),
    __metadata("design:type", Object)
], ProgressBarComponent.prototype, "progress", void 0);
ProgressBarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'progress-bar',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\progress-bar\progress-bar.html"*/'<!-- Generated template for the ProgressBarComponent component -->\n<section class="w-1024">\n	<ion-row justify-content-center>\n		<div class="progress-container">\n			<span class="progress-pretext t-uper gray-text text-left">Class Progress</span>\n			<div class="progress-outer">\n				<div class="progress-inner" [style.width]="progress + \'%\'"></div>\n			</div>\n			<span class="progress-posttext gray-text bg-pink">{{progress}}%</span>\n		</div>\n	</ion-row>\n</section>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\progress-bar\progress-bar.html"*/,
    }),
    __metadata("design:paramtypes", [])
], ProgressBarComponent);

//# sourceMappingURL=progress-bar.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaticFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_about_about__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_contact_contact__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_overview_overview__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the StaticFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var StaticFooterComponent = (function () {
    function StaticFooterComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    StaticFooterComponent.prototype.overview = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_overview_overview__["a" /* OverviewPage */]);
    };
    StaticFooterComponent.prototype.about = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_about_about__["a" /* AboutPage */]);
    };
    StaticFooterComponent.prototype.contact = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_contact_contact__["a" /* ContactPage */]);
    };
    return StaticFooterComponent;
}());
StaticFooterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'static-footer',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\components\static-footer\static-footer.html"*/'<!-- Generated template for the StaticFooterComponent component -->\n<section class="new-footer">\n	<div class="w-1024">\n		<ion-row class="first-row">\n			<div col-lg-4 col-md-4 col-12 class="left-logo">\n				<a href="#">\n					<img src="assets/images/full_logo.png" />\n				</a>\n			</div>\n			<div col-lg-4 col-md-4 col-12 padding-bottom margin-bottom class="left">\n				<ul>\n					<li>\n						<a (click)="about()">About Wakeful</a>\n					</li>\n					<li>\n						<a (click)="overview()">FAQs</a>\n					</li>\n					<li>\n						<a (click)="contact()">Contact us</a>\n					</li>\n				</ul>\n			</div>\n			<div col-lg-4 col-md-4 col-12 class="left">\n				<ul>\n					<li>\n						<a href="#/terms-conditions">Terms & Conditions</a>\n					</li>\n					<li>\n						<a href="#/privacy-policy">Privacy Policy</a>\n					</li>\n					<li>\n						<a href="#/acknowledgements">Acknowledgements</a>\n					</li>\n				</ul>\n			</div>\n			\n			<!-- Start Download app button -->\n			<!-- <div col-lg-3 col-md-3 col-12 class="right">\n				<h5>\n					<strong>Download our app</strong>\n				</h5>\n				<div class="store-btn">\n					<a href="#">\n						<img src="assets/images/btn_appstore.png">\n					</a>\n					<a href="#">\n						<img src="assets/images/btn_playstore.png">\n					</a>\n				</div>\n			</div> -->\n			<!-- End Download app button -->\n\n		</ion-row>\n		<!-- <ion-row text-center justify-content-center>\n            <div col-12 class="social-btn">\n                <a href="#">\n                    <ion-icon name="logo-facebook"></ion-icon>\n                </a>\n                <a href="#">\n                    <ion-icon name="logo-twitter"></ion-icon>\n                </a>\n                <a href="#">\n                    <ion-icon name="logo-youtube"></ion-icon>\n                </a>\n                <a href="#">\n                    <ion-icon name="logo-instagram"></ion-icon>\n                </a>\n            </div>\n        </ion-row> -->\n		<ion-row text-center justify-content-center>\n			<div col-12 class="bottom-link">\n				<ul>\n					<li>© 2018 wakeful INC.</li>\n				</ul>\n			</div>\n		</ion-row>\n	</div>\n</section>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\components\static-footer\static-footer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavController */]])
], StaticFooterComponent);

//# sourceMappingURL=static-footer.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcknowledgementsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AcknowledgementsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AcknowledgementsPage = (function () {
    function AcknowledgementsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AcknowledgementsPage.prototype.ionViewDidLoad = function () {
    };
    return AcknowledgementsPage;
}());
AcknowledgementsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-acknowledgements',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\acknowledgements\acknowledgements.html"*/'<!--\n  Generated template for the AcknowledgementsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n	<back-static-header></back-static-header>\n</ion-header>\n\n \n<ion-content class="about-section">\n	<!-- <section class="bg-pink">\n		<ion-row justify-content-center>\n			<ion-col>\n				<h1 class="static-headings">Acknowledgements</h1>\n			</ion-col>\n		</ion-row>\n	</section> -->\n	<ion-row>\n        <div class="head">\n            <h1>Acknowledgements</h1>\n        </div>\n    </ion-row>\n	<section class="w-900">\n		<ion-grid col-12 class="grid">\n			<ion-row class="cont-main">\n				<ion-col col-12 class="sub-heading">\n						<strong>Wakeful was made possible through a grant to Dr. David Victorson from the Osher Center for Integrative Medicine at Northwestern University.</strong>\n				</ion-col>\n			</ion-row>\n			<ion-row class="cont-main">\n				<ion-col padding col-12 class="acknowledge-cont">\n					<p class="gray-text"><strong>Dr. Victorson</strong> served as the Principal Lead on this effort and was responsible for overall app conceptualization and development. </p>\n					<p class="gray-text">He wishes to offer heartfelt thanks and acknowledgment to the following “village” of talented individuals who together made this tool possible:</p>\n					<p class="gray-text"><strong>Dr. Dershung Yang</strong> and <strong>Ms. Niina Haas</strong> at Bright Outcome for laying the technological “railroad tracks” of this entire effort;</p>\n					<p class="gray-text"><strong>Ms. Kelly Ann</strong> for overall art direction support;</p>\n					<p class="gray-text"><strong>Ms. Carly Maletich</strong> for massive amounts of content creation and project management;</p>\n					<p class="gray-text"><strong>Ms. Traci Brinling</strong> for incredible motion graphics animation support;</p>\n					<p class="gray-text"><strong>Ms. Ewa Niedbala</strong> for amazing original artwork support;</p>\n					<p class="gray-text"><strong>Mr. Kai Lukoff</strong> for thoughtful conceptual support on human-centered design principles;</p>\n					<p class="gray-text"><strong>Ms. Sarina Kao, Ms. Sydney Glickson, Ms. Rayann Yao,</strong> and <strong>Ms. Shruti Parthasarathy</strong> for loads of transcription assistance;</p>\n					<p class="gray-text"><strong>Dr. Melinda Ring</strong> for beta-testing support and feedback;</p>\n					<p class="gray-text"><strong>Dr. Karly Murphy</strong> for beta-testing support and feedback;</p>\n					<p class="gray-text"><strong>Ms. Bruriah Horowitz</strong> for beta-testing and never-ending project management support;</p>\n					<p class="gray-text"><strong>Ms. Christina Sauer</strong> for beta-testing and postproduction user implementation support; </p>\n					<p class="gray-text">And finally, a bow of deep gratitude to the countless mindfulness meditation practitioners, teachers, and researchers who informed the heart and soul of this tool.</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</section>\n	<static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\acknowledgements\acknowledgements.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], AcknowledgementsPage);

//# sourceMappingURL=acknowledgements.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommunityPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_community_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__community_discussion_community_discussion__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the HomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CommunityPage = (function () {
    function CommunityPage(communityService, loadCtrl, navCtrl, menu, helper, storage, authService, classService, navParams) {
        var _this = this;
        this.communityService = communityService;
        this.loadCtrl = loadCtrl;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.helper = helper;
        this.storage = storage;
        this.authService = authService;
        this.classService = classService;
        this.navParams = navParams;
        this.REVISIT_CLASS = false;
        this.shownGroup = null;
        this.title = "Community";
        this.breadcrumb = [];
        this.bg_image = '';
        this.course_id = '';
        this.menu.enable(true);
        this.REVISIT_CLASS = false;
        this.breadcrumb = ['Community', 'Discussion'];
        this.course_id = '';
        this.authService.get_course_id().then(function (id) {
            _this.course_id = id;
        });
        this.class_id = this.navParams.get('class_id');
    }
    CommunityPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.classService.get_background_images().then(function (res) {
            var data = res;
            if (data.hasOwnProperty('inner_page')) {
                _this.bg_image = data.inner_page;
            }
        });
        this.initClassPage();
    };
    // getting community list
    CommunityPage.prototype.getcommunityList = function () {
        var _this = this;
        this.loading = this.loadCtrl.create({ spinner: 'dots', content: 'Please wait...' });
        this.loading.present();
        this.communityService.communities(10).then(function (response) {
            _this.loading.dismiss();
            _this.result = response;
            if (_this.result.status == 'success') {
                _this.communityList = _this.result.data;
            }
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    // Community discussion page
    CommunityPage.prototype.getCommunityTopic = function (question_detail) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__community_discussion_community_discussion__["a" /* CommunityDiscussionPage */], { 'question_detail': question_detail });
    };
    // open close accordion
    CommunityPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    // check for current community
    CommunityPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    // Check to show class list or not
    CommunityPage.prototype.initClassPage = function () {
        var _this = this;
        this.storage.get('course_settings').then(function (settings) {
            if (_this.course_id) {
                var is_revisit = (settings && settings[_this.course_id]) ? settings[_this.course_id]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getcommunityList();
                }, 100);
            }
            else {
                var is_revisit = (settings && settings[__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['CLASSES_RE-ENTERABLE'] == 1 : !1;
                _this.REVISIT_CLASS = is_revisit;
                setTimeout(function () {
                    _this.getcommunityList();
                }, 100);
            }
        });
    };
    return CommunityPage;
}());
CommunityPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-community',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\community\community.html"*/'<ion-header>\n    <app-header [title]="title"></app-header>\n  </ion-header>\n  \n  <ion-content [style.backgroundImage]="\'url(\'+bg_image+\')\'">\n    <section class="w-1024 p-0-mob m-t-10">\n        <div padding-top>\n            <breadcrumb [data]="breadcrumb"></breadcrumb>\n        </div>\n        <ion-list inset class="btn-list">\n            <p class="text-light-gray  mob-p-l-r t-c-mob" text-center padding-bottom>Here are some popular class topic that other users are currently discussing.</p>\n            <div *ngIf="!REVISIT_CLASS">\n                <ion-item *ngFor="let community of communityList; let i=index" text-wrap (click)="(community.status && toggleGroup(i))" [class.open]="isGroupShown(i)"\n                 [class.active]="community.status">\n                    <div>\n                        <h2>\n                            <strong>{{ community.title }}</strong>\n                        </h2>\n                    </div>\n                    <span class="accordian-plus-minus" *ngIf="community.status==1">\n                        <ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'remove\' : \'add\'" class="icon-add-min"></ion-icon>\n                    </span>\n                    <ion-icon padding-right color="gray" name="ios-lock-outline" item-right *ngIf="community.status==0"></ion-icon>\n                    <!-- <ion-icon class="icon-gray" [name]="isGroupShown(i) ? \'remove\' : \'add\'" name="add" item-right *ngIf="homework.status==1"></ion-icon> -->\n  \n                    <div *ngIf="isGroupShown(i)" class="accordian-oppen-bg">\n                        <ion-list inset>\n                            <ion-item style="padding:0" detail-push ion-item *ngFor="let pages of community.list" (click)="getCommunityTopic(pages.page_data)">\n                                <!-- <div class="accordian-text">\n                                    <h2>\n                                        <strong>{{ pages.title }}</strong>\n                                    </h2>\n                                </div> -->\n                                <div class="accordian-text">\n                                    <h2>\n                                        <strong>{{ pages.page_data[\'question_text\'] }}</strong>\n                                    </h2>\n                                </div>\n                                <!-- <p class="overflow-text">{{ pages.page_data[\'question_text\'] }}</p> -->\n                                <ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n                            </ion-item>\n                        </ion-list>\n                    </div>\n                </ion-item>\n            </div>\n  \n  \n            <div *ngIf="REVISIT_CLASS">\n                <ion-item *ngFor="let community of communityList; let i=index" text-wrap (click)="(toggleGroup(i))" [class.open]="isGroupShown(i)"\n                 [class.active]="community.status">\n                    <div>\n                        <h2>\n                            <strong>{{ community.title }}</strong>\n                        </h2>\n                    </div>\n                    <span class="accordian-plus-minus">\n                        <ion-icon padding-right item-right color="primary" [name]="isGroupShown(i) ? \'remove\' : \'add\'" class="icon-add-min"></ion-icon>\n                    </span>\n                    <div *ngIf="isGroupShown(i)" class="accordian-oppen-bg">\n                        <ion-list inset>\n                            <ion-item style="padding:0" detail-push ion-item *ngFor="let pages of community.list" (click)="getCommunityTopic(pages.page_data)">\n                                <!-- <div class="accordian-text">\n                                    <h2>\n                                        <strong>{{ pages.title }}</strong>\n                                    </h2>\n                                </div> -->\n                                <div class="accordian-text">\n                                    <h2>\n                                        <strong>{{ pages.page_data[\'question_text\'] }}</strong>\n                                    </h2>\n                                </div>\n                                <!-- <p class="overflow-text">{{ pages.page_data[\'question_text\'] }}</p> -->\n                                <ion-icon padding-right color="primary" name="ios-arrow-forward" item-right></ion-icon>\n                            </ion-item>\n                        </ion-list>\n                    </div>\n                </ion-item>\n            </div>\n  \n        </ion-list>\n  \n    </section>\n  </ion-content>\n  '/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\community\community.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_community_service__["a" /* CommunityServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_app_menu_controller__["a" /* MenuController */], __WEBPACK_IMPORTED_MODULE_8__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_class_service__["a" /* ClassServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */]])
], CommunityPage);

//# sourceMappingURL=community.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyPolicyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PrivacyPolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrivacyPolicyPage = (function () {
    function PrivacyPolicyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PrivacyPolicyPage.prototype.ionViewDidLoad = function () {
    };
    return PrivacyPolicyPage;
}());
PrivacyPolicyPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-privacy-policy',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\privacy-policy\privacy-policy.html"*/'<!--\n  Generated template for the PrivacyPolicyPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n<ion-content> \n   <!--  <section class="bg-pink">\n        <ion-row justify-content-center>\n            <ion-col>\n                <h1 class="static-headings">Privacy Policy</h1>\n            </ion-col>\n        </ion-row>\n    </section> -->\n    <ion-row>\n        <div class="head">\n            <h1>Privacy Policy</h1>   \n        </div>\n    </ion-row>\n    <section class="w-900 privacy-policy">\n        <ion-row>\n            <ion-grid col-lg-11 col-md-12 col-12>\n                <div>\n                    <p class="gray-text">\n                        This Privacy Policy governs the manner in which BrightOutcome, Inc. (“BrightOutcome”, “we”, “us”, “our”) collects, uses, maintains and discloses information collected from users (each, a "User") of the wakeful (wakeful.com) website ("Site") and associated\n                        mobile device applications (“Mobile Apps”). This Privacy Policy applies to your use of all products and services offered by BrightOutcome. Privacy is of great importance to us. Because we may gather important information from Users,\n                        we have established this Privacy Policy as a means to communicate our information gathering and dissemination practices and to demonstrate our commitment to User privacy.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Personal Identification Information</strong></h2>\n                    <p class="gray-text">\n                        We may collect Personal Identification Information from Users in a variety of ways, including, but not limited to, when Users access Mobile Apps or visit the Site, register on the Site, subscribe to a newsletter, fill out a form, or in connection with\n                        other activities, services, features or resources we make available. Users may be asked for, as appropriate, name and email address. We will collect Personal Identification Information from Users only if they voluntarily submit\n                        such information to us. Users always have the option to refuse to supply Personal Identification Information, except that it may prevent them from engaging in certain activities and communicating with us.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Non-Personal Identification Information</strong></h2>\n\n                    <p class="gray-text">\n                        When you visit any website, certain non-personal information may be automatically and temporarily stored, including, but not limited to the following:\n                    </p>\n                    <ul class="left">\n                        <li>Domain name you use to access the internet.</li>\n                        <li>IP address (series of numbers assigned to a computer when accessing the internet).</li>\n                        <li>Information about the operating system of the computer and the web browser used when surfing the web generally and when accessing the website.</li>\n                        <li>Date and time of your visit to the website.</li>\n                        <li>Pages within the website that you visited.</li>\n                        <li>If applicable, the IP address of the website that linked you to the current website (e.g., google.com or bing.com).</li>\n                        <li>Demographic and interest information.</li>\n                    </ul>\n                    <p class="gray-text">We may collect this type of Non-Personal Identification Information about Users whenever they interact with the Site or Mobile Apps.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Web Browser Cookies</strong></h2>\n                    <p class="gray-text">\n                        When you visit any website, it may use "cookies" to enhance User experience. The websites place cookies on the Users’ computer hard drives for record-keeping purposes and sometimes to track information about the Users. Users may choose to set their web\n                        browsers to refuse cookies, or to alert them when cookies are being sent. If they do so, note that some parts of the websites may not function properly.\n                    </p>\n                    <p class="gray-text">\n                        We do not use cookies at this time, and if we do, this Privacy Policy will be updated accordingly to keep Users informed.\n                    </p>\n                </div>\n\n                <div>\n                    <h2 class="sub-heading"><strong>How We Use Collected Information</strong></h2>\n                    <p class="gray-text">\n                        We use the information that we collect to provide and maintain the Site and Mobile Apps as we deem appropriate in our sole discretion, such as to improve customer service, to personalize User experience, and to provide any other services that you and\n                        BrightOutcome agree to. We may also use the information to contact you to further discuss interest in our company, the goods and services that we provide, and to send information regarding our company or partners, such as promotions\n                        and events, as well as to respond to Users’ requests for information. You may be invited to receive an email newsletter or other correspondence by providing an email address.\n                    </p>\n                    <p class="gray-text">\n                        We may use email addresses to respond to User inquiries, questions, and/or other requests. Users that elect to opt-in to our mailing list may receive emails that include company news, updates, related product or service information, etc. If at any time\n                        any User elects to unsubscribe from receiving future emails or contacts, the User may do so by contacting us.\n                    </p>\n                </div>\n\n\n                <div>\n                    <h2 class="sub-heading"><strong>How We Protect Your Information</strong></h2>\n                    <p class="gray-text">\n                        We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data.\n                        Sensitive and private data exchange occurs over an SSL secured communication channel and is encrypted and protected with digital signatures.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Security</strong></h2>\n                    <p class="gray-text">\n                        Please note that data that is transported over an open network, such as the Internet or e-mail, may be accessible to anybody. BrightOutcome cannot guarantee the confidentiality of any communication or material transmitted via such open networks. When\n                        disclosing any personal information via an open network, you should be aware that it is potentially accessible to others, and consequently, can be collected and used by others without your consent. In particular, while individual\n                        data packets are often encrypted, the names of the sender and recipient are not. A third party may therefore be able to trace an existing bank account or relationship or one that is subsequently created. Even if both the sender\n                        and recipient are located in the same country data may also be transmitted via such networks to other countries regularly and without controls, including to countries that do not afford the same level of data protection as your\n                        country. Your data may be lost during transmission or may be accessed by unauthorized parties. We do not accept any liability for direct or indirect losses as regards the security of your personal information or data during its\n                        transfer via Internet. Please use other means of communication if you think this is necessary or prudent for security reasons.\n\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Changing Your Personal Information</strong></h2>\n                    <p class="gray-text">\n                        Users can update or remove personal information at any time by contacting us, but Users should be aware that archival or back-up copies of said information will not cease to exist and BrightOutcome shall not be liable for any problems in this editing\n                        process.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Sharing Your Personal Information</strong></h2>\n                    <p class="gray-text">\n                        We do not sell, trade, or rent Users’ Personal Identification Information to others unless it is to transact such business as you have contracted us to do, to comply with any legal processes and/or law enforcement requests, or in order to conduct any\n                        business as we, in our sole subjective discretion, deem reasonable. We may share generic aggregated demographic information not linked to any Personal Identification Information regarding Users with our business partners, trusted\n                        affiliates and advertisers for the purposes outlined above. Users have the right to opt-out of any sharing of information described above (unless required by law), by contacting us, which is not deemed valid until processed by\n                        BrightOutcome. It is your obligation to verify that you have been opted-out. BrightOutcome shall not be liable for problems with the opt-out procedures.\n                    </p>\n                </div>\n\n                <div>\n                    <h2 class="sub-heading"><strong>Third Party Websites</strong></h2>\n                    <p class="gray-text">\n                        Users may find advertising or other content that link to the websites and services of our partners, suppliers, advertisers, sponsors, licensors, or other third parties. We do not control the content or links that appear on these sites and are not responsible\n                        for the practices employed by other websites. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service\n                        policies. Browsing and interaction on any other website, including linked websites, is subject to that website\'s own terms and policies. When you access a linked site you may be disclosing private information. It is your responsibility\n                        to keep such information private and confidential.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Compliance With Children\'s Online Privacy Protection Act</strong></h2>\n                    <p class="gray-text">\n                        Protecting the privacy of the very young is especially important. For that reason, Users must be at least 13 years old; we never collect or maintain information from persons we actually know are under 13 years old, and no part of our Site is structured\n                        to attract anyone under 13. If you are under 13 years old, you may not use the Site.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Changes To This Privacy Policy</strong></h2>\n                    <p class="gray-text">\n                        BrightOutcome has the discretion to update this Privacy Policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping\n                        to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this Privacy Policy periodically and become aware of modifications.\n                    </p>\n                </div>\n\n                <div>\n                    <h2 class="sub-heading"><strong>Your Acceptance of These Terms</strong></h2>\n                    <p class="gray-text">\n                        By using the Site, you signify your acceptance of this policy and also agree to the BrightOutcome <a href="#/terms-conditions">Terms of Use</a>. If you do not agree to this Privacy Policy and the Terms of Use, then you do not have\n                        permission to use our Site or access Mobile Apps. After accepting this Privacy Policy and Terms of Use, your continued use of the Site following the posting of changes to this Privacy Policy or Terms of Use will be deemed your\n                        acceptance of those changes.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Contacting Us</strong></h2>\n                    <p class="gray-text">\n                        This Privacy Policy is part of the BrightOutcome Terms of Use applicable to your use of the Site and Mobile Apps and is governed by the Terms of Use. Questions regarding this Privacy Policy, the Terms of Use should be sent to BrightOutcome by email\n                        <a href="#">info@brighoutcome.com</a> or by certified mail addressed to: BrightOutcome Inc., 1110 Lake Cook Road, Suite 167, Buffalo Grove, Il 60089\n                    </p>\n                </div>\n            </ion-grid>\n        </ion-row>\n    </section>\n    <static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\privacy-policy\privacy-policy.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], PrivacyPolicyPage);

//# sourceMappingURL=privacy-policy.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsConditionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TermsConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TermsConditionsPage = (function () {
    function TermsConditionsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TermsConditionsPage.prototype.ionViewDidLoad = function () {
    };
    return TermsConditionsPage;
}());
TermsConditionsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-terms-conditions',template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\terms-conditions\terms-conditions.html"*/'<!--\n  Generated template for the TermsConditionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the ProfilePage page. \n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <back-static-header></back-static-header>\n</ion-header>\n\n\n<ion-content>\n    <!-- <section class="bg-pink">\n        <ion-row justify-content-center>\n            <ion-col>\n                <h1 class="static-headings">TERMS OF USE</h1>\n            </ion-col>\n        </ion-row>\n    </section> -->\n    <ion-row>\n        <div class="head">\n            <h1>Terms & Conditions</h1>   \n        </div>\n    </ion-row>\n    <section class="w-900 terms">\n        <ion-row>\n            <ion-grid col-lg-11 col-md-12 col-12>\n                <div>\n                    <p class="gray-text">\n                        These Terms of Use govern the manner in which the BrightOutcome, Inc. (“BrightOutcome”, “we”, “us”, “our”) <strong>Wakeful</strong> website ("Site") and associated <strong>Wakeful</strong> mobile device applications (“Mobile Apps”) may be accessed and used by properly licensed users (each, a “User”).\n                    </p>\n                    <p class="gray-text">\n                        By accessing, using and/or downloading any <strong>Wakeful</strong> materials or content from the Site, Apple, Inc. App Store, or Google, including. Google Play, you agree to follow and be bound by these Terms. if you do not agree with these Terms, you may not use the Site or Mobile Apps.\n                    </p>\n                    <p class="gray-text"> \n                        Any User under the age of 18 must have their parent or guardian create their <strong>Wakeful</strong> User account on the Website, and have their parent or guardian’s consent with regard to use of such User account.\n                    </p>\n                    <p class="gray-text"> \n                        BrightOutcome provides the <strong>Wakeful</strong> products and services via the Site and Mobile Apps as a forum and associated service, but is not a medical provider and does not provide, review the accuracy of, endorse or otherwise affect the substance of any medical information. BrightOutcome has no control over the information provided by Users of the Site or Mobile Apps.\n                    </p>\n                    <p class="gray-text"> \n                        Users should consult directly with their physicians or other medical representatives for any health questions or concerns. Your use of the Website, and any all information and material contained therein, including but not limited to the materials, content and submissions, is solely at your own risk. You should always seek the advice of qualified, professional medical providers with regard to any medical problem, condition, or situation you may have.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>General Provisions</strong></h2>\n                    <p class="gray-text">\n                        <u>Content, Specifications and Accuracy of Information</u>: The information on the Site and in Mobile Apps may be inaccurate, incomplete or out of date. We make no representation that any such information is complete, accurate,\n                        or current.\n                    </p><p class="gray-text"> All features, content, specifications, products, and services described or depicted are subject to change at any time without notice. </p><p class="gray-text">\n                        <u>Use of Content</u>: All products, materials or content accessed or provided via the Site or Mobile Apps, including but not limited to information, documents, products, logos, graphics, sounds, images, compilations, content and services ("Materials”), are provided either by BrightOutcome or by respective third party authors, developers or vendors ("Third Party Providers") and are the copyrighted works of BrightOutcome and/or its Third Party Providers\n                        (or is permitted/licensed to be used by Third Party Providers), unless specifically provided otherwise. Except as stated herein, none of the Materials may be modified, copied, printed, reproduced, distributed, republished, performed,\n                        downloaded, displayed, posted, transmitted and/or otherwise used in any form or by any means, including but not limited to electronic, mechanical, photocopying, recording, or other means, without the prior express written permission\n                        of BrightOutcome and/or the applicable Third Party Provider. Also, you may not "mirror" or “archive” any Materials on any other server without BrightOutcome’s prior express written permission.\n                    </p><p class="gray-text"> Except where expressly provided otherwise by BrightOutcome, nothing shall be construed to confer any license or ownership right in or to the Materials, under any of BrightOutcome’s intellectual property rights, whether by\n                        estoppel, implication, or otherwise. Materials provided by Third Party Providers have not been independently reviewed, tested, certified, or authenticated in whole or in part by BrightOutcome. BrightOutcome does not provide, sell,\n                        license, or lease any of the Materials other than those specifically identified as being provided by BrightOutcome.\n                    </p><p class="gray-text"> Unauthorized use of any Materials may violate copyright laws, trademark laws, the laws of privacy and publicity, and/or communications regulations and statutes. It is your obligation to comply with all applicable state, federal\n                        and international laws. You are responsible for maintaining the confidentiality of your account information and password and for restricting access to such information and to your computer. You agree to accept responsibility for\n                        all activities that occur under your account or password.\n                    </p><p class="gray-text">\n                        <u>Termination</u>: Either BrightOutcome or you may suspend or terminate your account or your use of the Site or Mobile Apps at any time, for any reason or for no reason. We reserve the right to change, suspend, or discontinue\n                        all or any aspect of the Site and Mobile Apps at any time without notice.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Authorized Agents</strong></h2>\n                    <p class="gray-text">\n                        If you are using the Site or Mobile Apps on behalf of another person (“Person”), you (“Authorized Agent”) are subject to these Terms, and you agree: (1) that the information you provide on the Person’s behalf will be accurate and thorough, (2) that the\n                        Person has agreed to allow you to use the Site and Mobile Apps on the Person’s behalf and has provided you with all the necessary verbal or written consent to permit you to use the same on his or her behalf, (3) that you will use\n                        the Site and Mobile Apps solely on behalf of, and for the benefit of the Person, (4) that you will periodically review and update the information as necessary to provide complete and accurate information on behalf of the Person,\n                        (5) that you will immediately remove any information entered on the Person’s behalf when instructed to do so by the Person, and (6) that the Person understands these Terms and agrees to them.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>HIPAA not Applicable - No Protected Health Information</strong></h2>\n                    <p class="gray-text">The Health Insurance Portability and Accountability Act of 1996 (“HIPAA”) as modified and amended by the Health Information Technology for Economic and Clinical Health (“HITECH”) Act of 2009 contains provisions regarding the use and\n                        disclosure of Protected Health Information (“PHI”). PHI includes any information about health status, provision of health care, or payment for health care that can be linked to a specific individual and any part of a patient’s\n                        medical record or payment history. You are prohibited from submitting PHI via the Site and Mobile Apps; BrightOutcome hereby expressly disclaims any responsibility or liability for any such action by you.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Links to Third Party Sites</strong></h2>\n                    <p class="gray-text">The Site and Mobile Apps may contain links or have references to websites controlled by parties other than BrightOutcome, whether for clinical content or specifically for advertising purposes. BrightOutcome is not responsible for and\n                        does not endorse or accept any responsibility for the contents or use of these third party websites. BrightOutcome provides these links to you only as a convenience, and the inclusion of any link does not imply endorsement by BrightOutcome\n                        of the linked website and/or the content and materials found at the linked website, except as specifically stated otherwise by BrightOutcome. It is your responsibility to take precautions to ensure that whatever you select for\n                        your use is free of viruses or other items of an intrusive nature.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Submissions</strong></h2>\n                    <p class="gray-text">Except where expressly provided otherwise by BrightOutcome, all comments, feedback, information and data submitted by Users through, in association with or in regard to the Site or Mobile Apps and/or any other BrightOutcome goods or\n                        services ("Submissions") shall be considered non-confidential and BrightOutcome property. This may not include copyright ownership of images, including but not limited to photographs and videos which you may upload, but does include\n                        an express license to use said images in any method BrightOutcome sees fit and make compilations and derivative works thereof in all media now known or hereafter devised. Except as expressly enumerated in the preceding sentence,\n                        by providing such Submissions to BrightOutcome, you agree to assign to BrightOutcome, as consideration in exchange for the use of the Site or Mobile Apps, all worldwide rights, title and interest in copyrights and other intellectual\n                        property rights to the Submissions. You represent that you have the right to grant BrightOutcome these rights. BrightOutcome shall be free to use and/or disseminate such Submissions on an unrestricted basis for any purpose. You\n                        acknowledge that you are responsible for the Submissions that you provide, and that you, not BrightOutcome, have full responsibility for the Submissions, including their legality, reliability, appropriateness, originality and copyright.</p><p class="gray-text">Users\n                        shall not post any Submission that (a) is defamatory, abusive, harassing, threatening, or an invasion of a right of privacy of another person; (b) bigoted, hateful, or racially or otherwise offensive; (c) is violent, vulgar, obscene,\n                        pornographic or otherwise sexually explicit; (d) otherwise harms or can reasonably be expected to harm any person or entity; (e) contains PHI, (f) contains vulgar language, personal insults, or offensive comments about others (based\n                        on ethnicity, race, religion, sexual orientation, intelligence, or any other personal attacks), (g) promotes any services or products, is clearly off topic, or makes unsupported assertions or accusations (links to personal or commercial\n                        web sites will not be posted), or, (h) voices support for drug use or any illegal activity. Submissions may be reviewed or moderated by BrightOutcome prior to or after posting. While debate and disagreement are permitted, mutual\n                        respect and professionalism must be maintained. </p><p class="gray-text"> BrightOutcome reserves the right, but disclaims any obligation or responsibility, to (a) refuse to post or communicate or remove any Submission that violates these Terms of\n                        Use and (b) identify any User to third parties, and/or disclose to third parties any Submission or personally identifiable information, when we believe in good faith that such identification or disclosure will either (i) facilitate\n                        compliance with laws, including, for example, compliance with a court order or subpoena, or (ii) help to enforce these Terms of Use and/or protect the safety or security of any person or property, including BrightOutcome. Moreover,\n                        we retain all rights to remove Submissions at any time for any reason or no reason whatsoever. </p><p class="gray-text"> All Submissions must be true, and in accordance with the rights of privacy and publicity and all federal, state and international\n                        law. You may not upload an image or any likeness of another person without their consent (or the consent of their parent or guardian if they are under the age of 18). If you do so, BrightOutcome reserves the right to cancel or\n                        suspend your account. Furthermore, BrightOutcome reserves the right to cancel or suspend your account, if in its sole discretion, it believes you are using your account for improper purposes, or any purpose inconsistent with its\n                        business.\n                    </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Limitation of Liability</strong></h2>\n\n                    <p class="gray-text">To the extent allowed by law, in no event shall the aggregate liability of BrightOutcome exceed the lesser of $100 or the amount paid to BrightOutcome in the immediately preceding twelve months. In no event shall BrightOutcome or the\n                        third party providers be liable to anyone for any indirect, punitive, special, exemplary, incidental, or consequential damages, or for any damages to your computer, telecommunication equipment, or other property and/or for loss\n                        of data, content, images, revenue, profits, use or other economic advantage, arising out of, or in any way connected with these Terms, including but not limited to the accessing or use of, or inability to use, the Site or Mobile\n                        Apps and the services associated therewith including but not limited to the use or downloading of any Materials, regardless of cause, whether in an action in contract or negligence or other tortious action, even if the party from\n                        which damages are being sought or the third party provider have been previously advised of the possibility of such damages. The limitation of liability set forth in this section shall not apply in the event of User breach, or related\n                        to its indemnity obligations. This paragraph shall not affect the rights listed below in the section titled “indemnities”.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Licenses from BRIGHTOUTCOME</strong></h2>\n                    <p class="gray-text">You are being granted only a revocable, limited license, in compliance with these Terms.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Licenses from YOU</strong></h2>\n                    <p class="gray-text">You grant to BrightOutcome and its Third Party Providers the non-exclusive, worldwide right to use, copy, transmit and display any data, information, content or other materials, provided by you via the Site or Mobile Apps. Notwithstanding\n                        the foregoing, BrightOutcome obligations regarding identification and other information concerning your personal information shall be governed by the terms of the <a href="#/privacy-policy">Privacy Policy</a>. The terms of the\n                        Privacy Policy are expressly incorporated herein as though set forth in full.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Representations and Warranties</strong></h2>\n                    <p class="gray-text">Each party represents and warrants that it has the power and authority to enter into these Terms. BrightOutcome warrants that it will provide all goods and services in a manner consistent with its business practices, as BrightOutcome,\n                        in its sole and absolute discretion, deems fit. To the extent that you represent an entity of any type or any individual other than yourself, you represent and warrant that you have the proper authority to enter into these Terms\n                        their behalf.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Disclaimer of Warranties</strong></h2>\n                    <p class="gray-text">Except where expressly provided otherwise by BrightOutcome, the Materials are provided "as is," and are for use as stated herein. Except for the express warranties set forth herein, BrightOutcome and its third party providers hereby\n                        disclaim all express or implied representations, warranties, guaranties, and conditions with regard to the Site, Mobile Apps, Materials, and the goods and services associated therewith including but not limited to any implied representations,\n                        warranties, guaranties, and conditions of merchantability, fitness for a particular purpose, title and non-infringement, and quality of goods and services except to the extent that such disclaimers are held to be legally invalid.\n                        BrightOutcome and its third party providers make no representations, guaranties or warranties regarding the reliability, availability, timeliness, quality, suitability, truth, accuracy or completeness of the Site, Mobile Apps,\n                        Materials, and goods and services associated therewith, or the results you may obtain by accessing or using the Site, Mobile Apps, Materials, and goods and services associated therewith. Without limiting the generality of the foregoing,\n                        BrightOutcome and its third party providers do not represent or warrant that (a) the operation or use of the Site, Mobile Apps, or Materials will be timely, secure, uninterrupted or error-free; (b) the quality of any products,\n                        services, information, or other material you purchase or obtain from BrightOutcome will meet your requirements; or (c) the Site, Mobile Apps, or Materials or the systems that make these available are free of viruses or other harmful\n                        components. You acknowledge that neither BrightOutcome nor its third party providers control the transfer of data over communications facilities, including the internet, and that the Site, Mobile Apps, or Materials may be subject\n                        to limitations, delays, and other problems inherent in the use of such communications facilities. BrightOutcome is not responsible for any delays, delivery failures, or other damage resulting from such problems. Except where expressly\n                        provided otherwise by BrightOutcome, the Site, Mobile Apps, Materials, and the goods and services associated therewith are provided to you on an "as is" basis. BrightOutcome expressly disclaims any warranty related to the quality\n                        of third party goods and/or services referenced on the website. You use third party providers and affiliated services at your peril and assume all risks related to use of said third party providers and services.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Indemnities</strong></h2>\n                    <p class="gray-text">You shall defend and indemnify BrightOutcome and its Third Party Providers against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys\' fees and costs) finally awarded against BrightOutcome or\n                        its Third Party Providers by a court of competent jurisdiction arising out of or in connection with a claim by a third party related to you and your use of the Site, Mobile Apps, Materials, and/or the goods and services associated\n                        therewith and any Submissions made by you. BrightOutcome shall have no indemnification obligation or other liability for any claim of infringement arising from (a) use of the Site, Mobile Apps, Materials, and/or the goods and services\n                        associated therewith, other than in accordance with these Terms; (b) the combination of the Site, Mobile Apps, Materials, and/or the goods and services associated therewith with any other products, services, or materials; or (c)\n                        any third party products, services, or materials. </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Your responsibilities</strong></h2>\n                    <p class="gray-text">You will comply with all applicable local, state, national and foreign laws, treaties, regulations and conventions in connection with your use of the Site, Mobile Apps, Materials, and/or the goods and services associated therewith,\n                        including without limitation those related to data privacy, international communications, and the exportation of technical or personal data from locations other than the location from which BrightOutcome controls and operates the\n                        Site, Mobile Apps, Materials, and/or the goods and services associated therewith. Furthermore, you expressly agree not to violate any rights of publicity or privacy of any person, nor defame any person or entity.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Notices</strong></h2>\n                    <p class="gray-text">BrightOutcome may give notice by means of a general notice on the Site or Mobile Apps, or by electronic mail to your e-mail address on record in your User account. You may give notice to BrightOutcome at any time by letter sent by\n                        confirmed facsimile to BrightOutcome, fax number 847.232.3361 or by letter delivered by registered mail with return receipt to: BrightOutcome Inc., 1110 Lake Cook Road, Suite 167, Buffalo Grove, IL 60089. All notices shall be deemed\n                        to have been given four days after mailing or 36 hours after sending by confirmed facsimile, email or posting to the Site or Mobile Apps.</p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Miscellaneous Provisions</strong></h2>\n                    <p class="gray-text">Any action related to these Terms will be governed by Illinois law and controlling U.S. Federal law. No choice of law rules of any jurisdiction will apply. Any disputes, actions, claims or causes of action arising out of or in connection\n                        with these Terms, the Site, Mobile Apps, or Materials shall be subject to the exclusive jurisdiction of the state and federal courts located in Chicago, Illinois, and to arbitration as stated herein. At the request of BrightOutcome,\n                        any controversy or claim related to these Terms ("Claim") may be resolved by arbitration in accordance with the Federal Arbitration Act (Title 9, U. S. Code) (the "Act"). In the case of arbitration, the Act will apply even though\n                        these Terms provide that they are governed by the laws of Illinois. Arbitration proceedings will be determined in accordance with the Act. This paragraph does not limit the right of BrightOutcome to exercise any and all legal,\n                        judicial, or self-help remedies, including without limitation, injunctive relief or additional or supplementary remedies.</p><p class="gray-text">These Terms, together with the Privacy Policy, represents the parties\' entire understanding relating\n                        to the use of the Site, Mobile Apps, Materials, and/or the goods and services associated therewith and supersedes any prior or contemporaneous, conflicting or additional, communications. BrightOutcome reserves the right to change\n                        these Terms or its policies at any time and from time to time, and such changes will be effective upon notice as recited above. You should regularly review the then current Terms because they are binding on you. Your continued\n                        use of the Site or Mobile Apps after any such changes and/or postings shall constitute your consent to such changes. If any provision of these Terms is held by a court of competent jurisdiction to be invalid or unenforceable, then\n                        such provision(s) shall be construed to reflect the intentions of the invalid or unenforceable provision(s), with all other provisions remaining in full force and effect. No joint venture, partnership, employment, or agency relationship\n                        exists between BrightOutcome and you as a result of these Terms or use of the Site, Mobile Apps, Materials, and/or the goods and services associated therewith. You may not assign these Terms or your User account without the prior\n                        written approval of BrightOutcome. Any purported assignment in violation of this section shall be void. The failure of either party to enforce any right or provision in these Terms shall not constitute a waiver of such right or\n                        provision unless acknowledged and agreed to by such party in writing. In the event of any litigation of any controversy or dispute arising out of or in connection with these Terms, its interpretation, its performance, or the like,\n                        the prevailing party shall be awarded reasonable attorneys\' fees and expenses, court costs, and reasonable costs for expert and other witnesses attributable to the prosecution or defense of that controversy or dispute. Any and\n                        all rights not expressly granted herein are reserved by BrightOutcome. </p>\n                </div>\n                <div>\n                    <h2 class="sub-heading"><strong>Intellectual Property Notices</strong></h2>\n                    <p class="gray-text">Elements of the Site, Mobile Apps, and Materials are protected by copyright, trademark, trade dress and other laws and may not be copied or imitated in whole or in part. No logo, graphic, sound or image may be copied or retransmitted\n                        unless expressly permitted by BrightOutcome.<br/><br/> Thank you. </p>\n                </div>\n\n            </ion-grid>\n        </ion-row>\n    </section>\n\n    <static-footer></static-footer>\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\terms-conditions\terms-conditions.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], TermsConditionsPage);

//# sourceMappingURL=terms-conditions.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_idle_core__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_app_menu_controller__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about_about__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_contact_contact__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_feedback_feedback__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_overview_overview__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_reset_password_reset_password__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_class_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_notification_notification__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




















var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, storage, authService, classService, alertCtrl, menu, events, 
        // public socialService: AuthService,
        helper, idle) {
        var _this = this;
        this.authService = authService;
        this.classService = classService;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.events = events;
        this.helper = helper;
        this.idle = idle;
        this.user = false;
        this.isLoggedIn = false;
        this.notificationCount = 0;
        this.storage = null;
        this.code = '';
        this.bg_image = '';
        this.idleState = 'Not started.';
        this.timedOut = false;
        this.lastPing = null;
        this.storage = storage;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            _this.events.subscribe('user:notification', function () {
                // notification count subscribe
                _this.storage.get('notification_count').then(function (notificationCount) {
                    _this.notificationCount = notificationCount;
                });
            });
            _this.storage.get('token').then(function (token) {
                _this.isLoggedIn = !!token;
                if (token) {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */];
                }
                else {
                    _this.code = location.search.split('code=')[1];
                    var url = document.URL.split('#')[1];
                    var slug = '', accesscode = '';
                    if (url != undefined) {
                        slug = url.split('/')[1];
                        accesscode = url.split('/')[2];
                    }
                    if (_this.code != undefined) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_reset_password_reset_password__["a" /* ResetPasswordPage */], { code: _this.code });
                    }
                    else if (slug == 'sign-up' && url != undefined) {
                        // Don't remove this condition. This is used for hold url to signup page for create password
                        // this.navCtrl.setRoot(SignupPage, { accesscode: accesscode });
                    }
                    else {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */];
                    }
                }
                clearInterval(_this.interval);
                _this.interval = setInterval(function () {
                    if (_this.isLoggedIn) {
                        _this.authService.getNotificationCount().then(function (response) {
                            _this.storage.set('notification_count', response['notification_count']);
                            events.publish('user:notification');
                        });
                    }
                }, 10000);
            });
            _this.classService.set_background_images();
        });
        events.subscribe('user:loggedin', function () {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.isLoggedIn = !0;
            _this.reset();
        });
        // sets an idle timeout of 5 seconds, for testing purposes.
        idle.setIdle(__WEBPACK_IMPORTED_MODULE_7__config_constants__["a" /* CONSTANTS */].SESSION_TIMEOUT); //
        // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(5);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(__WEBPACK_IMPORTED_MODULE_4__ng_idle_core__["a" /* DEFAULT_INTERRUPTSOURCES */]);
        //idle.onIdleEnd.subscribe(() => (this.idleState = 'No longer idle.'));
        idle.onTimeout.subscribe(function () {
            _this.idleState = 'Timed out!';
            _this.timedOut = true;
        });
        idle.onIdleStart.subscribe(function () {
            _this.idleState = "You've gone idle!";
            _this.isLoggedIn = false;
            _this.storage.clear();
            _this.timedOut = true;
            _this.helper.presentToast('Your session has been expired. Please log in to continue.', 'error');
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */]);
        });
    }
    MyApp.prototype.reset = function () {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    };
    MyApp.prototype.overview = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_overview_overview__["a" /* OverviewPage */]);
    };
    MyApp.prototype.notification = function () {
        this.authService.clearNotificationCount().then(function (response) {
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_19__pages_notification_notification__["a" /* NotificationPage */]);
    };
    MyApp.prototype.about = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */]);
    };
    MyApp.prototype.contact = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_contact_contact__["a" /* ContactPage */]);
    };
    MyApp.prototype.feedback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_feedback_feedback__["a" /* FeedbackPage */]);
    };
    MyApp.prototype.profile = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */]);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.authService.logout().then(function (result) {
            _this.data = result;
            if (_this.data.status == 'success') {
                _this.isLoggedIn = !1;
                _this.storage.clear();
                // this.socialService.authState.subscribe(user => {
                // 	if (user != null) {
                // 		this.socialService.signOut();
                // 	}
                // });
                _this.reset();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */]); // go to Home
            }
            else {
                _this.alerttitle = 'Error';
                _this.alertmessage = _this.data.msg;
                _this.doAlert();
            }
        }, function (err) { });
    };
    MyApp.prototype.doAlert = function () {
        var alert = this.alertCtrl.create({
            title: this.alerttitle,
            subTitle: this.alertmessage,
            buttons: ['Dismiss'],
        });
        alert.present();
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imhereNav'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavController */])
], MyApp.prototype, "navCtrl", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\app\app.html"*/'<ion-menu [content]="imhereNav" persistent="true" side="right">\n    <ion-header>\n        <ion-item>\n            <ion-icon item-right menuClose name="ios-close-outline" style="cursor:pointer"></ion-icon>\n        </ion-item>\n    </ion-header>\n\n    <ion-content>\n        <ion-list class="menu-list" padding-top>\n            <button menuClose ion-item *ngIf="isLoggedIn" (click)="notification()"><img src=\'assets/images/notification.png\' width="22px" style="margin-right: 5px;" /> Notification <span *ngIf=\'notificationCount > 0\' class="notification">{{notificationCount > 0 ? notificationCount : 0 }}</span></button>\n            <button menuClose ion-item (click)="overview()"><ion-icon name="ios-list-box-outline"></ion-icon>FAQs</button>\n            <button menuClose ion-item (click)="about()"><ion-icon name="ios-information-circle-outline"></ion-icon>About wakeful</button>\n\n            <button *ngIf="isLoggedIn" menuClose ion-item (click)="feedback()"><ion-icon name="ios-chatboxes-outline"></ion-icon>Feedback</button>\n            <button menuClose ion-item (click)="contact()"><ion-icon name="ios-call-outline"></ion-icon>Contact us</button>\n\n            <button *ngIf="isLoggedIn" menuClose ion-item (click)="profile()"><ion-icon name="ios-person-outline"></ion-icon>Profile</button>\n            <button *ngIf="isLoggedIn" menuClose ion-item (click)="logout()"><ion-icon name="ios-exit-outline"></ion-icon>Logout</button>\n        </ion-list>\n    </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #imhereNav></ion-nav>\n'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\app\app.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_16__providers_auth_service__["a" /* AuthServiceProvider */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_16__providers_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_17__providers_class_service__["a" /* ClassServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_18__providers_helper__["a" /* Helper */],
        __WEBPACK_IMPORTED_MODULE_4__ng_idle_core__["b" /* Idle */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__class_class__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__review_review__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__homework_homework__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__meditation_timer_meditation_timer__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__community_user_community_user__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_helper__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












//import { dashCaseToCamelCase } from '@angular/compiler/src/util';
var TabsPage = (function () {
    function TabsPage(authService, navCtrl, helper, events) {
        var _this = this;
        this.authService = authService;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.events = events;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__class_class__["a" /* ClassPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__review_review__["a" /* ReviewPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_8__community_user_community_user__["a" /* CommunityUserPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_6__homework_homework__["a" /* HomeworkPage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_7__meditation_timer_meditation_timer__["a" /* MeditationTimerPage */];
        this.tabshow = true;
        this.uriSegment = '';
        this.events.subscribe('homework:disableTabs', function (checkHidden) {
            _this.hiddenTabs = checkHidden;
        });
        this.events.subscribe('course:updateSettings', function (settings) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.initClassPage(settings);
        });
        // this.uriSegment =""
        if (!!window.location.href.split('#')[1]) {
            this.uriSegment = window.location.href.split('#')[1].replace(/\//g, '');
        }
        else {
            this.uriSegment = 'dashboard';
        }
    }
    // isClassPage(){
    // 	if(this.tabRef.getSelected()){
    // 		return this.tabRef.getSelected().index==1;
    // 	}
    // 	return false;
    // }
    // Check for valid token on every tab click
    TabsPage.prototype.check_login = function () {
        var _this = this;
        if (!!window.location.href.split('#')[1]) {
            this.uriSegment = window.location.href.split('#')[1].replace(/\//g, '');
        }
        else {
            this.uriSegment = 'dashboard';
        }
        this.authService.check_login().then(function (status) {
            if (!status) {
                _this.helper.presentToast('Your session has been expired. Please log in to continue.', 'error');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            }
            else {
                _this.events.publish('user:loggedin');
            }
        });
        switch (this.uriSegment) {
            case "journey":
                this.tabRef.select(1);
                break;
            case "review":
                this.tabRef.select(2);
                break;
            case "community":
                this.tabRef.select(3);
                break;
            case "homework":
                this.tabRef.select(4);
                break;
            case "meditation-timer":
                this.tabRef.select(5);
                break;
            case "dashboard":
                this.tabRef.select(0);
                break;
        }
        this.uriSegment = '';
    };
    TabsPage.prototype.initClassPage = function (settings) {
        var _this = this;
        this.authService.get_course_id().then(function (id) {
            var course_id = id;
            if (course_id) {
                var journey_enabled = (settings && settings[course_id]) ? settings[course_id]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
                _this.JOURNEY_ENABLED = journey_enabled;
                _this.authService.check_user_class_count({ 'course_id': course_id }).then(function (res) {
                    if (res['status'] == 'success') {
                        _this.tabshow = false;
                    }
                    else {
                        _this.tabshow = true;
                    }
                });
            }
            else {
                var journey_enabled = (settings && settings[__WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]) ? settings[__WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE]['JOURNEY_PAGE_VIEWABLE'] == 1 : 0;
                _this.JOURNEY_ENABLED = journey_enabled;
                _this.authService.check_user_class_count(__WEBPACK_IMPORTED_MODULE_1__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE).then(function (res) {
                    if (res['status'] == 'success') {
                        _this.tabshow = false;
                    }
                    else {
                        _this.tabshow = true;
                    }
                });
            }
        });
    };
    return TabsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imTabs'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_11_ionic_angular__["o" /* Tabs */])
], TabsPage.prototype, "tabRef", void 0);
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\xampp\htdocs\wakeful\app\src\pages\tabs\tabs.html"*/'<ion-tabs #imTabs tabsPlacement="bottom" [ngClass]="{\'hidden\': hiddenTabs}">\n    <ion-tab [root]="tab1Root" (ionSelect)="check_login()" tabTitle="Dashboard" tabIcon="im-dashboard"></ion-tab>\n    <ion-tab [root]="tab2Root" (ionSelect)="check_login()" tabTitle="Journey" tabIcon="im-class" [show]=\'JOURNEY_ENABLED\' [enabled]="tabshow"></ion-tab>\n    <ion-tab [root]="tab3Root" (ionSelect)="check_login()" tabTitle="Review" tabIcon="im-review"></ion-tab>\n    <ion-tab [root]="tab4Root" (ionSelect)="check_login()" tabTitle="Community" tabIcon="im-community" [enabled]="tabshow" ></ion-tab>\n    <!-- <ion-tab [root]="tab5Root" (ionSelect)="check_login()" tabTitle="Homework" tabIcon="im-homework" [enabled]="tabshow"></ion-tab> -->\n    <ion-tab [root]="tab6Root" (ionSelect)="check_login()" tabTitle="Practice" tabIcon="im-padmasana"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"C:\xampp\htdocs\wakeful\app\src\pages\tabs\tabs.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__providers_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_11_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_10__providers_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_11_ionic_angular__["c" /* Events */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeworkServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constants__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomeworkServiceProvider = (function () {
    function HomeworkServiceProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    /**
     * @desc Used to add headers for each API call, if a user is logged in then add token header also
     * @param isLoggedIn
     */
    HomeworkServiceProvider.prototype.getHeaders = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + __WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].AUTH
        });
    };
    /**
     * @desc Common Success Callback function used from all API calls
     * @param res
     * @param resolve
     * @param reject
     * @param status
     */
    HomeworkServiceProvider.prototype.successCallback = function (res, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (res.headers.get('Content-type').indexOf('application/json') !== -1) {
            resolve(res.json());
        }
        else {
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /**
     * @desc Common Error Callback function used from all API calls
     * @param err
     * @param resolve
     * @param reject
     * @param status
     */
    HomeworkServiceProvider.prototype.errorCallback = function (err, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (err.headers.get('Content-type') === 'application/json') {
            reject(err.json().join());
        }
        else {
            console.log(err);
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /** Calling api for get homework of class */
    HomeworkServiceProvider.prototype.homeworks = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/homeworks?course_id=' + course_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/homeworks?course_id=' + __WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for get class list */
    HomeworkServiceProvider.prototype.classList = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/class_list', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get homework detail */
    HomeworkServiceProvider.prototype.homework_detail = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/homework-detail', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add status of exercise audio/video track */
    HomeworkServiceProvider.prototype.exerciseTracking = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/exercise-tracking', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    HomeworkServiceProvider.prototype.updateReadingTime = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'homework/update-reading-time', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    return HomeworkServiceProvider;
}());
HomeworkServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], HomeworkServiceProvider);

//# sourceMappingURL=homework-service.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommunityServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CommunityServiceProvider = (function () {
    function CommunityServiceProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    /**
     * @desc Used to add headers for each API call, if a user is logged in then add token header also
     * @param isLoggedIn
     */
    CommunityServiceProvider.prototype.getHeaders = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].AUTH
        });
    };
    /**
     * @desc Common Success Callback function used from all API calls
     * @param res
     * @param resolve
     * @param reject
     * @param status
     */
    CommunityServiceProvider.prototype.successCallback = function (res, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (res.headers.get('Content-type').indexOf('application/json') !== -1) {
            resolve(res.json());
        }
        else {
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /**
     * @desc Common Error Callback function used from all API calls
     * @param err
     * @param resolve
     * @param reject
     * @param status
     */
    CommunityServiceProvider.prototype.errorCallback = function (err, resolve, reject, status) {
        if (status === void 0) { status = ''; }
        if (err.headers.get('Content-type') === 'application/json') {
            reject(err.json().join());
        }
        else {
            console.log(err);
            reject({ status: 'error', msg: 'Invalid response' });
        }
    };
    /** Calling api for get homework of class */
    CommunityServiceProvider.prototype.communities = function (page) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/communities?course_id=' + course_id + '&page=' + page, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/communities?course_id=' + __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE + '&page=' + page, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for get homework detail */
    CommunityServiceProvider.prototype.discussion = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/discussion', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get single community of notification */
    CommunityServiceProvider.prototype.community = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                if (_this.storage.get('course_id')) {
                    _this.storage.get('course_id').then(function (course_id) {
                        _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/notification_community?course_id=' + course_id + '&question_id=' + data.question_id + '&post_id=' + data.post_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                    });
                }
                else {
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/notification_community?course_id=' + __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].CURRENT_COURSE + '&question_id=' + data.question_id + '&post_id=' + data.post_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
                }
            });
        });
    };
    /** Calling api for add reflection answer */
    CommunityServiceProvider.prototype.add_comment = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/reply', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get homework detail */
    CommunityServiceProvider.prototype.get_reply = function (answer_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/reply?answer_id=' + answer_id, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get homework detail */
    CommunityServiceProvider.prototype.add_comment_status = function (obj) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/comment_status', obj, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get homework detail */
    CommunityServiceProvider.prototype.add_reply_status = function (obj) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/reply_status', obj, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for get communities notification */
    CommunityServiceProvider.prototype.notification = function (page) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/notification?page=' + page, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    /** Calling api for add reflection answer */
    CommunityServiceProvider.prototype.updateNotification = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                var headers = _this.getHeaders();
                headers.append('Token', token);
                _this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* CONSTANTS */].API_ENDPOINT + 'community/update_notification', data, { headers: headers }).subscribe(function (res) { return _this.successCallback(res, resolve, reject); }, function (err) { return _this.errorCallback(err, resolve, reject); });
            });
        });
    };
    return CommunityServiceProvider;
}());
CommunityServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], CommunityServiceProvider);

//# sourceMappingURL=community-service.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataServiceProvider = (function () {
    function DataServiceProvider() {
        this.messageSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](0);
        this.currentClass = this.messageSource.asObservable();
    }
    DataServiceProvider.prototype.changeClass = function (classId) {
        this.messageSource.next(classId);
    };
    return DataServiceProvider;
}());
DataServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], DataServiceProvider);

//# sourceMappingURL=data.service.js.map

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Helper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { NavController } from 'ionic-angular/navigation/nav-controller';
var Helper = (function () {
    function Helper(toastCtrl, storage, authService, app) {
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.authService = authService;
        this.app = app;
        this.navCtrl = app.getRootNavs();
    }
    // Helper function for show toast message 
    Helper.prototype.presentToast = function (msg, type) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top',
            cssClass: type,
            showCloseButton: true,
            closeButtonText: 'X',
        });
        toast.onDidDismiss(function () {
        });
        toast.present();
    };
    // Middleware function for check user is authenticate or not 
    Helper.prototype.authenticated = function (redirect) {
        var _this = this;
        if (redirect === void 0) { redirect = true; }
        return new Promise(function (resolve, reject) {
            _this.authService.check_login().then(function (status) {
                if (status) {
                    resolve(true);
                }
                else {
                    if (redirect) {
                        _this.Nav = _this.app.getRootNavById('n4');
                        _this.presentToast('Your session has been expired. Please log in to continue.', 'error');
                        _this.Nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */]);
                    }
                    reject(false);
                }
            });
        });
    };
    // Check User email exits or not 
    Helper.prototype.isEmailUnique = function (email_info) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.authService.isEmailRegisterd(email_info).then(function (result) {
                _this.data = result;
                if (_this.data.status == 'success') {
                    resolve(true);
                }
                else {
                    reject(false);
                }
            });
        });
    };
    // Check Username exits or not
    Helper.prototype.isUsernameUnique = function (user_info) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.authService.isUsernameRegisterd(user_info).then(function (result) {
                _this.data = result;
                if (_this.data.status == 'success') {
                    resolve(true);
                }
                else {
                    reject(false);
                }
            });
        });
    };
    // Comment logout function 
    Helper.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.authService.logout().then(function (result) {
                _this.data = result;
                if (_this.data.status == 'success') {
                    _this.Nav = _this.app.getRootNavById('n4');
                    _this.presentToast('You are logout successfully.', 'error');
                    _this.Nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */]);
                    // this.app.getRootNav().setRoot(HomePage);
                }
                else {
                    reject(false);
                }
            });
        });
    };
    Helper.prototype.get_course_setting = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.get('course_settings').then(function (course_settings) {
                resolve(course_settings);
                return true;
            });
        });
    };
    return Helper;
}());
Helper = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */]])
], Helper);

//# sourceMappingURL=helper.js.map

/***/ })

},[399]);
//# sourceMappingURL=main.js.map