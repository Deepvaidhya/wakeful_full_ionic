<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['mailtype'] = 'html';
$config['charset']  = 'utf-8';
$config['newline']  = "\r\n";
$config['wordwrap'] = TRUE;
$config['_bit_depths'] = array('7bit', '8bit', 'base64');
$config['_encoding'] = 'base64';
